include("../Sources/bmera_codes.jl")
using BMeraCodes
n_levels = 10

prob_of_flip = 0.1

cnot = elementary_gates["cnot"]
id = elementary_gates["id_1"]

function init_layer(level::Int, n_levels::Int)
    branch_length = 2^(n_levels-level+1)
    gates1 = [GatePosition(cnot,2*i-1,2*i) for i in 1:div(branch_length,2)]
    sl1 = SubLayer(branch_length, gates1)
    gates2 = vcat(GatePosition(id, 1), [GatePosition(cnot,2*i,2*i+1) for i in 1:(div(branch_length,2)-1)], GatePosition(id, branch_length))
    sl2 = SubLayer(branch_length, gates2)
    bl = BranchingLayer([sl2, sl1])
    Layer(bl, 2^(level-1))
end

noisy_zeros = ZeroProbState(prob_of_flip * ones(2^n_levels))
layers = [init_layer(i, n_levels) for i in 1:n_levels]
circ = Circuit(2, noisy_zeros, layers)
decode!(circ, 1)

println()
println("---- Start ----")
println()

tot = 0
for i in 1:10
    println(i)
    tot += @elapsed decode!(circ, 1)
end
println(tot/10)

