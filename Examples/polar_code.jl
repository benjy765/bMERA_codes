include("../Sources/bmera_codes.jl")
using BMeraCodes

n_levels = 8
prob_of_flip = 0.1
#=
cnot = elementary_gates["cnot"]

function init_layer(level::Int, n_levels::Int)
    branch_length = 2^(n_levels-level+1)
    gates = [GatePosition(cnot,2*i-1,2*i) for i in 1:div(branch_length,2)]
    sl = SubLayer(branch_length, gates)
    bl = BranchingLayer([sl])
    Layer(bl, 2^(level-1))
end

layers = [init_layer(i, n_levels) for i in 1:n_levels]
=#
circ = polar_codes(n_levels)

frozen_bits = frozen_from_erasure(circ, 2^(n_levels-1), 0.25)
println(frozen_bits)
using PyPlot;

h = plt[:hist](frozen_bits, 20)
show()
#=
noisy_zeros = ZeroProbState([rand() > prob_of_flip ? 1-prob_of_flip : prob_of_flip for bit in ones(2^n_levels)])
layers = [init_layer(i, n_levels) for i in 1:n_levels]
circ = Circuit(2, noisy_zeros, layers)
decode!(circ, 1)

println()
println("---- Start ----")
println()

tot = 0
for i in 1:10
    println(i)
    tot += @elapsed decode!(circ, 1)
end
println(tot/10)

=#
