include("../Sources/bmera_codes.jl")
using BMeraCodes

n_levels = 8
#prob_of_flip = 0.1
#=
cnot = elementary_gates["cnot"]

function init_layer(level::Int, n_levels::Int)
    branch_length = 2^(n_levels-level+1)
    gates = [GatePosition(cnot,2*i-1,2*i) for i in 1:div(branch_length,2)]
    sl = SubLayer(branch_length, gates)
    bl = BranchingLayer([sl])
    Layer(bl, 2^(level-1))
end
=#
l = LeftCorrelatedProbState(0.0, 0.5, 1., 0.)
r = RightCorrelatedProbState(0.0, 0.5, 1., 0.)
m = MidCorrelatedProbState(0.0, 0.5)
c = Correlator(0.97, 0.75)

corr_noise = CorrelatedNoise(2^n_levels, c, l, m, r)

#println("Noiseless CN = ", corr_noise.chain)
#println("")
noisy_corr_noise = flip_cps(corr_noise)

#println("Noisy CN = ", noisy_corr_noise.chain)
#println("")

#layers = [init_layer(i, n_levels) for i in 1:n_levels]

circ = polar_codes(n_levels)
circ.bottom_state = corr_noise

frozen_bits = frozen_from_correlation(circ, 2^(n_levels-1))

#println("Frozen_bits = ", frozen_bits)
using PyPlot;

h = plt[:hist](frozen_bits, 20)
show()
#circ = Circuit(2, noisy_corr_noise, layers)
#decode!(circ, 1)
#=
println()
println("---- Start ----")
println()
decode!(circ, 1)
println("Final State = ", get_state(circ))
=#
#=
tot = 0
for i in 1:10
    println(i)
    tot += @elapsed decode!(circ, 1)
end
println(tot/10)
=#

#=
function burst(n_bits::Int, pgb::Float64, pbg::Float64)
    output = zeros(Int, n_bits)
    state = "G" # We start in the good state
    for i in 1:n_bits-1
        if state == "B"
            # This is the "bad" state
            output[i] = rand() > 1/2 ? 0 : 1

            # pick a random number to know what to do next
            if rand() <= pbg
                state = "G"
            end
        else
            # State = "G"
            output[i] = 0
            # pick a random number to know what to do next
            if rand() <= pgb
                state = "B"
            end
        end
    end
    output[n_bits] = 0 # We finish in the Good state
    output
end

# Estimated logical prob of error
function p_error(n_rep::Int, n_bits::Int, p_gb::Float64, p_bg::Float64)
    r = 0
    for x in 1:n_rep
        out = burst(n_bits, p_gb, p_bg)
        r += sum(out)/n_bits
    end
    r/n_rep
end

# Assuming that good is identity and bad is BSC(0.5)
function flip_cps(cn::CorrelatedNoise)
    pgb, pbg = cn.corr.corr[2,1], cn.corr.corr[1,2]
    b = burst(cn.n_bits, pgb, pbg)
    println("burst = ", b)
    println()
    for i in 2:cn.n_bits-1
        if b[i] == 1
            cn[i].prob[1,1,:] = [0.0, 1.0]
        end
    end
    cn
end
=#