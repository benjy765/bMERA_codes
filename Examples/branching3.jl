include("../Sources/bmera_codes.jl")
using BMeraCodes

n_levels = 2
n_bits = 3^n_levels

## Gates

id = elementary_gates["id_1"]
cnot = elementary_gates["cnot"]

bottom_state = ZeroProbState(0.9 - 0.8 * (rand(3^n_levels) .> 0.5))

function init_layer(level::Int, n_levels::Int)
    gates1 = [[GatePosition(cnot,i,i+2),GatePosition(id, i+3)] for i in 1:3:n_bits]
    sl1 = SubLayer(2^level, gates1)
    gates2 = vcat(GatePosition(id, 1), [GatePosition(cnot, 2*i, 2*i + 1) for i in 1:(2^(level-1)-1)], GatePosition(id, 2^level))
    sl2 = SubLayer(2^level, gates2)
    bl = BranchingLayer([sl1, sl2])
    Layer(bl, 2^(n_levels - level))
end