# Home

This is a simulation tools for BMera codes. Once downloaded, it can be use by first including the source file :
```
include("Sources/bmera_codes.jl")
```
After, it is possible to use the `BMera_Codes` module :
```
using BMeraCodes
```

## Functionalities

- [bit.jl](@ref)
- [prob_states.jl](@ref)
- [transformations.jl](@ref)
- [contractions.jl](@ref)
- [causal_cones.jl](@ref)
- [gate.jl](@ref)
- [gate_position.jl](@ref)
- [sublayers.jl](@ref)
- [branching_layers.jl](@ref)
- [layers.jl](@ref)
- [circuit.jl](@ref)
- [frozen_bits.jl](@ref)

## Examples 

- [Polar codes](@ref)
- [Convolutional polar codes](@ref)