# Convolutional polar codes

Here is a code that create a four layers convolutional polar code with a 
probability of flip of 0.1.

```julia-repl
# Include the module
include("../Sources/bmera_codes.jl")
using BMeraCodes

# Parameters
n_layers = 4
prob_of_flip = 0.1

# Import gates
cnot = elementary_gates["cnot"]
id = elementary_gates["id_1"]

# Create every layers in the circuit
function init_layer(level::Int, n_levels::Int)
    branch_length = 2^(n_levels-level+1)
    gates1 = [GatePosition(cnot,2*i-1,2*i) for i in 1:div(branch_length,2)]
    sl1 = SubLayer(branch_length, gates1)
    gates2 = vcat(GatePosition(id, 1), [GatePosition(cnot,2*i,2*i+1) for i in 1:(div(branch_length,2)-1)], GatePosition(id, branch_length))
    sl2 = SubLayer(branch_length, gates2)
    bl = BranchingLayer([sl2, sl1])
    Layer(bl, 2^(level-1))
end

layers = [init_layer(i, n_layers) for i in 1:n_layers]

# For every bit, the probability of begin 0 is 1 - prob_of_flip
noisy_zeros = ZeroProbState((1 - prob_of_flip) * ones(2^n_layers))

# Create the circuit and decode.
circ = Circuit(2, noisy_zeros, layers)
decode!(circ, 1)
```

The ouput of that code must be 
```
16-element Array{BMeraCodes.Bit,1}:
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
```