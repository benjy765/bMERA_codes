# Polar codes

Here is a code that create a four layers polar code with a probability of flip of 0.1.

```julia-repl
# Include the module
include("../Sources/bmera_codes.jl")
using BMeraCodes

# Parameters
n_layers = 4
prob_of_flip = 0.1

# Import cnot gate
cnot = elementary_gates["cnot"]

# Create every layers in the circuit
function init_layer(layer::Int, n_layers::Int)
    branch_length = 2^(n_layers-layer+1)
    gates = [GatePosition(cnot,2*i-1,2*i) for i in 1:div(branch_length,2)]
    sl = SubLayer(branch_length, gates)
    bl = BranchingLayer([sl])
    Layer(bl, 2^(layer-1))
end

layers = [init_layer(i, n_layers) for i in 1:n_layers]

# For every bit, the probability of begin 0 is 1 - prob_of_flip
noisy_zeros = ZeroProbState((1 - prob_of_flip) * ones(2^n_layers))

# Create the circuit and decode.
circ = Circuit(2, noisy_zeros, layers)
decode!(circ, 1)
```

The ouput of that code must be 
```
16-element Array{BMeraCodes.Bit,1}:
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
 bit_0
```