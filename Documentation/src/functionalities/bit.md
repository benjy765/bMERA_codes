# bit.jl

## Bit

```@docs
Bit
```

## BitState

```@docs
BitState
```

## Functions

```@docs
permute(::BitState, ::Vector{Int})
n_open_legs
all_bit_states
```

