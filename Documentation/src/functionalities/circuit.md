# circuit.jl

```@docs
Circuit
```

## Functions and methods

```@docs
top_cone
get_state
change_state!(circ::Circuit, state::BitState, first_bit::Int, last_bit::Int)
decode!
```
