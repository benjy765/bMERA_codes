# layers.jl

# Layer
```@docs
Layer
```

## General methods

### Utilitary
```
get_state(layer::Layer)
length(layer::Layer)
```

### Set state
```@docs
 set_state!(layer::Layer, bs::BitState)
```

### Propagate
```@docs
propagate(layer::Layer, first_bit::Int, last_bit::Int)
propagate(layer::Layer, position::Int)
propagate(layer::Layer)
```