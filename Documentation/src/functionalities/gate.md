# gate.jl

```@docs
Gate
GateSet
```

## Elementary gates

```@docs
identity_gate
identity_gate!
not_gate
not_gate!
cnot_gate
cnot_gate!
swap_gate
```

#### Elementary gate set

We provide an elementary gate set with three gates : the not, the cnot and the identity on 1 bit.
It can serve as a basis to build more GateSet or to get access to a single get.

```@julia-repl
julia> elementary_gates
Dict{String,Gate} with 5 entries:
  "cnot_2" => 2 bit(s) cnot gate
  "cnot_1" => 1 bit(s) cnot gate
  "not"    => 1 bit(s) cnot gate
  "cnot"   => 2 bit(s) cnot gate
  "id_1"   => 1 bit(s) identity gate
julia> cnot = elementary_gates["cnot"]
2 bit(s) cnot gate
```

## Functions and methods

```@docs
kron(left_gate::Gate, right_gate::Gate)
*(down_gate::Gate, up_gate::Gate)
getindex(gate::Gate, bs::BitState)
length(gate::Gate)
==(g1::Gate, g2::Gate)
```


