# sublayers.jl

# Sublayer
```@docs
SubLayer
```

## Functions and methods

#### Propagate & simplification
```@docs
propagate(sl::SubLayer, input_bs::BitState, first_bit::Int, last_bit::Int)
simplify!(sl::SubLayer, input_bs::BitState, first_bit::Int, last_bit::Int)
```

#### Utilitary functions
```@docs
getindex(sl::SubLayer, ind::Int)
getindex(sl::SubLayer, inds...)
length(sl::SubLayer)
get_transform
```

#### Comparison methods
The following method is also implemented : 
```
==(sl1::SubLayer, sl2::SubLayer)
```



