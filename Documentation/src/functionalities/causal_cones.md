# causal_cones.jl

## Causal Cone

```@docs
CausalCone
```

## Alias
```
const ConeChilds = Union{Vector{CausalCone}, ProbState}
```

## Functions and methods

```@docs
length(cc::CausalCone)
depth
merge_childs
contract!
set_childs!
```

## Show
```
show(io::IO, cc::CausalCone, space = "")
```