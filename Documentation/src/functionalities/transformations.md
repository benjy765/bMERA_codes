# transformations.jl

```@docs
Transformation
```

## Specific types

```@docs
VectorTransformation
MatrixTransformation
```

## Functions and methods

```@docs
length(transform::Transformation)
inv(vTransform::VectorTransformation)
kron(vt1::VectorTransformation, vt2::VectorTransformation)
```

#### Comparison methods
The following methods are also implemented : 
```
==(transform1::Transformation, transform2::Transformation)
!=(transform1::Transformation, transform2::Transformation)
```

## Conversions

TO DO