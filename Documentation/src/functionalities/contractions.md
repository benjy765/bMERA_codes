# contractions.jl

```@docs
*(bs::BitState, zps::ZeroProbState)
*(transform::Transformation, ps::ProbState)
*(ps::ProbState, transform::Transformation)
*(low::Transformation, high::Transformation)
```

## Chained contractions

Since every contraction operators `*` return a object that it contractable, it is possible to chain
contractions.

#### Example
```julia-repl
julia> ps = BitStringProbState([0.1, 0.2, 0.3, 0.4]);
julia> transform = VectorTransformation([1, 2, 4, 3]);
julia> bs = BitState("_0");
julia> ps * transform * bs
BitStringProbState([0.2, 0.8], 1)
```

