# prob_states.jl

```@docs
ProbState
```

## Specific types

```@docs
ZeroProbState
BinProbState
TensorProbState
BitStringProbState
```

## General methods
```@docs
permute(ps::ProbState, permutation::Vector{Int})
getindex(zps::ZeroProbState, inds...)
length(ps::ProbState)
```

#### Comparison methods
The following methods are also implemented : 
```
==(ps1::ProbState, ps2::ProbState)
!=(ps1::ProbState, ps2::ProbState)
≈(ps1::ProbState, ps2::ProbState)
```

## Specific methods

#### Set index
```@docs
setindex!(zps::ZeroProbState, prob, inds...)
setindex!(bps::BinProbState, prob::Matrix{Float64}, inds...)
setindex!(bps::BinProbState, prob::Vector{Float64}, ind::Int)
```

#### Kronecker product
```@docs
kron(tps1::TensorProbState, tps2::TensorProbState)
kron(bsps1::BitStringProbState, bsps2::BitStringProbState)
```

#### Fix bits
```@docs
fix_bits(tps::TensorProbState, b::Bit, ind::Int)
fix_bits(bsps::BitStringProbState, b::Union{Bit, BitState}, inds...)
```

## Conversions
```@docs
BitState(zps::ZeroProbState)
ZeroProbState(bps::BinProbState)
BinProbState(zps::ZeroProbState)
TensorProbState(zps::ZeroProbState)
TensorProbState(bps::BinProbState)
BitStringProbState(zps::ZeroProbState)
BitStringProbState(bps::BinProbState)
BitStringProbState(tps::TensorProbState)
```


