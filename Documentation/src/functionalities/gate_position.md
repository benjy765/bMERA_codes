# gate_position.jl

# GatePosition
```@docs
GatePosition
```

## Functions and methods
```@docs
span
getindex(gp::GatePosition, bs::BitState)
```

### Comparison methods
The following methods are implemented : 
```
==(gp1::GatePosition, gp2::GatePosition)
isless(gp1::GatePosition, gp2::GatePosition)
hash(gp::GatePosition)
```