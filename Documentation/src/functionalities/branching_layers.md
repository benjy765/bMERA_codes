# branching_layers.jl

# BranchingLayer

```@docs
BranchingLayer
```

## General methods

### Set state and change state

```@docs
set_state!(bl::BranchingLayer, bs::BitState)
change_state!(bl::BranchingLayer, bs::BitState, first_bit::Int, last_bit::Int)
change_state!(bl::BranchingLayer, bs::BitState)
change_state!(bl::BranchingLayer, bs::BitState, position::Int)
```

### Propagate and simplify

```@docs
propagate(bl::BranchingLayer, first_bit::Int, last_bit::Int)
propagate(bl::BranchingLayer, position::Int)
propagate(bl::BranchingLayer)
simplify!(bl::BranchingLayer, first_bit::Int, last_bit::Int)
simplify!(bl::BranchingLayer, position::Int)
simplify!(bl::BranchingLayer)
simplify_and_propagate!(bl::BranchingLayer, first_bit::Int, last_bit::Int)
simplify_and_propagate!(bl::BranchingLayer, position::Int)
simplify_and_propagate!(bl::BranchingLayer)
```

### Utilitary
```
length(bl::BranchingLayer)
```