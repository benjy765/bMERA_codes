# frozen_bits.jl

# Algorithms
```@docs
frozen_from_bhatta
frozen_from_random_output
frozen_from_mean_output
```

# Utilitaries
```@docs
hamming_distance
prob_first_error
```