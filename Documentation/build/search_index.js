var documenterSearchIndex = {"docs": [

{
    "location": "index.html#",
    "page": "Home",
    "title": "Home",
    "category": "page",
    "text": ""
},

{
    "location": "index.html#Home-1",
    "page": "Home",
    "title": "Home",
    "category": "section",
    "text": "This is a simulation tools for BMera codes. Once downloaded, it can be use by first including the source file :include(\"Sources/bmera_codes.jl\")After, it is possible to use the BMera_Codes module :using BMeraCodes"
},

{
    "location": "index.html#Functionalities-1",
    "page": "Home",
    "title": "Functionalities",
    "category": "section",
    "text": "bit.jl\nprob_states.jl\ntransformations.jl\ncontractions.jl\ncausal_cones.jl\ngate.jl\ngate_position.jl\nsublayers.jl\nbranching_layers.jl\nlayers.jl\ncircuit.jl\nfrozen_bits.jl"
},

{
    "location": "index.html#Examples-1",
    "page": "Home",
    "title": "Examples",
    "category": "section",
    "text": "Polar codes\nConvolutional polar codes"
},

{
    "location": "functionalities/bit.html#",
    "page": "bit.jl",
    "title": "bit.jl",
    "category": "page",
    "text": ""
},

{
    "location": "functionalities/bit.html#bit.jl-1",
    "page": "bit.jl",
    "title": "bit.jl",
    "category": "section",
    "text": ""
},

{
    "location": "functionalities/bit.html#BMeraCodes.Bit",
    "page": "bit.jl",
    "title": "BMeraCodes.Bit",
    "category": "type",
    "text": "Enumeration representing bits. Possible values are\n\nbit_0 represent (1, 0),\nbit_1 represent (0, 1),\nbit_e represent (1, 1),\nbit__ represent an open leg.\n\nCan be instantiated via a char (\'0\', \'1\', \'e\', \'_\'), an integer (0, 1, 2, 3) or by their name :\n\nExamples\n\njulia> Bit(0)\nbit_0::Bit = 0\njulia> Bit(\'e\')\nbit_e::Bit = 2\njulia> bit__\nbit__::Bit = 3\n\n\n\n"
},

{
    "location": "functionalities/bit.html#Bit-1",
    "page": "bit.jl",
    "title": "Bit",
    "category": "section",
    "text": "Bit"
},

{
    "location": "functionalities/bit.html#BMeraCodes.BitState",
    "page": "bit.jl",
    "title": "BMeraCodes.BitState",
    "category": "type",
    "text": "An alias for any array of Bit. Can be instantiated via an array of Bit :\n\njulia> BitState([bit__, bit_1])\n2-element Array{Bit,1}:\n bit__\n bit_1\n\nFor a vector, can be instantiated via a string :\n\njulia> BitState(\"e_01\")\n4-element Array{Bit,1}:\n bit_e\n bit__\n bit_0\n bit_1\n\n\n\n"
},

{
    "location": "functionalities/bit.html#BitState-1",
    "page": "bit.jl",
    "title": "BitState",
    "category": "section",
    "text": "BitState"
},

{
    "location": "functionalities/bit.html#Base.SparseArrays.permute-Tuple{AbstractArray{BMeraCodes.Bit,N} where N,Array{Int64,1}}",
    "page": "bit.jl",
    "title": "Base.SparseArrays.permute",
    "category": "method",
    "text": "permute(bs::BitState, permutation::Vector{Int})\n\nIf permutation is a valid length(bs) permutation then return bs[permutation].\n\n\n\n"
},

{
    "location": "functionalities/bit.html#BMeraCodes.n_open_legs",
    "page": "bit.jl",
    "title": "BMeraCodes.n_open_legs",
    "category": "function",
    "text": "n_open_legs(bs::BitState)\n\nReturn the number of bit__ in bs.\n\n\n\n"
},

{
    "location": "functionalities/bit.html#BMeraCodes.all_bit_states",
    "page": "bit.jl",
    "title": "BMeraCodes.all_bit_states",
    "category": "function",
    "text": "all_bit_states(n_bits::Int)\n\nReturn an array of 4^n_bits elements containing every possible BitState of length n_bits. \n\n\n\n"
},

{
    "location": "functionalities/bit.html#Functions-1",
    "page": "bit.jl",
    "title": "Functions",
    "category": "section",
    "text": "permute(::BitState, ::Vector{Int})\nn_open_legs\nall_bit_states"
},

{
    "location": "functionalities/prob_states.html#",
    "page": "prob_states.jl",
    "title": "prob_states.jl",
    "category": "page",
    "text": ""
},

{
    "location": "functionalities/prob_states.html#BMeraCodes.ProbState",
    "page": "prob_states.jl",
    "title": "BMeraCodes.ProbState",
    "category": "type",
    "text": "ProbState <: Any\n\nAbstract type. \n\nIt is a way to represent a distribution of probabilities over the values of some bits.\n\nNeeded fields for a composite subtype are \n\nprob::AbstractArray{Float64}\nn_bits::Int\n\n\n\n"
},

{
    "location": "functionalities/prob_states.html#prob_states.jl-1",
    "page": "prob_states.jl",
    "title": "prob_states.jl",
    "category": "section",
    "text": "ProbState"
},

{
    "location": "functionalities/prob_states.html#BMeraCodes.ZeroProbState",
    "page": "prob_states.jl",
    "title": "BMeraCodes.ZeroProbState",
    "category": "type",
    "text": "ZeroProbState <: ProbState\n\nImmutable type.\n\nFields\n\nprob::Vector{Float64}\nn_bits::Int\n\nA vector of probabilities p such that 0 <= p <= 1 and where each element represents  the probability of a bit being 0. The lowest index represents the bit on the left.\n\nExamples\n\njulia> ZeroProbState([0.1, 0.3, 0.5, 0.7, 0.9])\nZeroProbState([0.1, 0.3, 0.5, 0.7, 0.9], 5)\n\n\n\n"
},

{
    "location": "functionalities/prob_states.html#BMeraCodes.BinProbState",
    "page": "prob_states.jl",
    "title": "BMeraCodes.BinProbState",
    "category": "type",
    "text": "BinProbState <: ProbState\n\nImmutable type.\n\nFields:\n\nprob::Matrix{Float64}\nn_bits::Int\n\nA matrix where the elements in the first column represent the probability  of a bit being bit_0 and the element in the second column represent the probability of being bit_1. Each rows must sum to one and each prob must be between 0 and 1. The lowest index represents  the bit on the left.\n\nExamples\n\njulia> BinProbState([ 0.1 0.9 ; 0.2 0.8; 0.7 0.3])\nBinProbState([0.1 0.9; 0.2 0.8; 0.7 0.3], 3)\n\n\n\n"
},

{
    "location": "functionalities/prob_states.html#BMeraCodes.TensorProbState",
    "page": "prob_states.jl",
    "title": "BMeraCodes.TensorProbState",
    "category": "type",
    "text": "TensorProbState <: ProbState\n\nImmutable type.\n\nFields:\n\nprob::Array{Float64}\nn_bits::Int\n\nA n-dimension array where n is the number of bits and each dimension as size 2. The first dimension represent the bit on the left. The two index of each dimension represent the two possible state for a bit (1 -> 0, 2 -> 1). Each element in the array represents the probability of the corresponding bitstring. All probabilities must be  between 0 and 1 and their sum must be 1.\n\nExamples\n\njulia> prob = zeros(2,2,2);\njulia> prob[:,:,1] = [0.10 0.20 ; 0.25 0.05];\njulia> prob[:,:,2] = [0.05 0.15 ; 0.10 0.10];\njulia> TensorProbState(prob)\nTensorProbState([0.1 0.2; 0.25 0.05]\n[0.05 0.15; 0.1 0.1], 3)\n\n\n\n"
},

{
    "location": "functionalities/prob_states.html#BMeraCodes.BitStringProbState",
    "page": "prob_states.jl",
    "title": "BMeraCodes.BitStringProbState",
    "category": "type",
    "text": "BitStringProbState <: ProbState\n\nComposite type.\n\nFields:\n\nprob::Vector{Float64}\nn_bits::Int\n\nA vector where the elements represent the probability of a bit string. Entry at index i represent the probability of the bitstring associated to the binary representation of i - 1. All probabilities must be between 0 and 1 and their sum must be 1. Length of the prob must  be a power of two.\n\nExamples\n\njulia> BitStringProbState([0.1, 0.05, 0.20, 0.15, 0.25, 0.10, 0.05, 0.10])\nBitStringProbState([0.1, 0.05, 0.2, 0.15, 0.25, 0.1, 0.05, 0.1], 3)\n\n\n\n"
},

{
    "location": "functionalities/prob_states.html#Specific-types-1",
    "page": "prob_states.jl",
    "title": "Specific types",
    "category": "section",
    "text": "ZeroProbState\nBinProbState\nTensorProbState\nBitStringProbState"
},

{
    "location": "functionalities/prob_states.html#Base.SparseArrays.permute-Tuple{BMeraCodes.ProbState,Array{Int64,1}}",
    "page": "prob_states.jl",
    "title": "Base.SparseArrays.permute",
    "category": "method",
    "text": "permute(ps::ProbState, permutation::Vector{Int})\n\nReturn a new ProbState the same type as ps where the bits order as been changed  according to permutation. permutation must be the same length as ps and have unique entries between 1 and length(ps).\n\n\n\n"
},

{
    "location": "functionalities/prob_states.html#Base.getindex-Tuple{BMeraCodes.ZeroProbState,Vararg{Any,N} where N}",
    "page": "prob_states.jl",
    "title": "Base.getindex",
    "category": "method",
    "text": "getindex(ps::ProbState, inds...)\n\nReturn the probabilities for the bits specified by inds... in the type of ps. If the probabilities are correlated, return the marginal distribution, else  return the original values at inds.... \n\n\n\n"
},

{
    "location": "functionalities/prob_states.html#Base.length-Tuple{BMeraCodes.ProbState}",
    "page": "prob_states.jl",
    "title": "Base.length",
    "category": "method",
    "text": "length(ps::ProbState)\n\nReturn the number of bits in ps.\n\n\n\n"
},

{
    "location": "functionalities/prob_states.html#General-methods-1",
    "page": "prob_states.jl",
    "title": "General methods",
    "category": "section",
    "text": "permute(ps::ProbState, permutation::Vector{Int})\ngetindex(zps::ZeroProbState, inds...)\nlength(ps::ProbState)"
},

{
    "location": "functionalities/prob_states.html#Comparison-methods-1",
    "page": "prob_states.jl",
    "title": "Comparison methods",
    "category": "section",
    "text": "The following methods are also implemented : ==(ps1::ProbState, ps2::ProbState)\n!=(ps1::ProbState, ps2::ProbState)\n≈(ps1::ProbState, ps2::ProbState)"
},

{
    "location": "functionalities/prob_states.html#Specific-methods-1",
    "page": "prob_states.jl",
    "title": "Specific methods",
    "category": "section",
    "text": ""
},

{
    "location": "functionalities/prob_states.html#Base.setindex!-Tuple{BMeraCodes.ZeroProbState,Any,Vararg{Any,N} where N}",
    "page": "prob_states.jl",
    "title": "Base.setindex!",
    "category": "method",
    "text": "setindex!(zps::ZeroProbState, prob, inds...)\n\nSet prob to bits indexed by inds.... All element of prob must be between 0 and 1. The length of prob and inds... must the same and lower than the length of zps.\n\n\n\n"
},

{
    "location": "functionalities/prob_states.html#Base.setindex!-Tuple{BMeraCodes.BinProbState,Array{Float64,2},Vararg{Any,N} where N}",
    "page": "prob_states.jl",
    "title": "Base.setindex!",
    "category": "method",
    "text": "setindex!(bps::BinProbState, prob::Matrix{Float64}, inds...)\n\nSet prob to bits indexed by inds.... All element of prob must be between 0 and 1. Each row of prob must sum to 1. The number of rows in prob must be the length of  inds... lower than the length of bps. The number of columns must be 2.\n\n\n\n"
},

{
    "location": "functionalities/prob_states.html#Base.setindex!-Tuple{BMeraCodes.BinProbState,Array{Float64,1},Int64}",
    "page": "prob_states.jl",
    "title": "Base.setindex!",
    "category": "method",
    "text": "setindex!(bps::BinProbState, prob::Vector{Float64}, ind::Int)\n\nSet prob to the bit indexed by ind. All element of prob must be between 0 and 1. prob must sum to 1. The length of prob must be 2.\n\n\n\n"
},

{
    "location": "functionalities/prob_states.html#Set-index-1",
    "page": "prob_states.jl",
    "title": "Set index",
    "category": "section",
    "text": "setindex!(zps::ZeroProbState, prob, inds...)\nsetindex!(bps::BinProbState, prob::Matrix{Float64}, inds...)\nsetindex!(bps::BinProbState, prob::Vector{Float64}, ind::Int)"
},

{
    "location": "functionalities/prob_states.html#Base.kron-Tuple{BMeraCodes.TensorProbState,BMeraCodes.TensorProbState}",
    "page": "prob_states.jl",
    "title": "Base.kron",
    "category": "method",
    "text": "kron(tps1::TensorProbState, tps2::TensorProbState)\n\nCompute the kronecker product between the corresponding BitStringProbState and return the result as a TensorProbState.\n\n\n\n"
},

{
    "location": "functionalities/prob_states.html#Base.kron-Tuple{BMeraCodes.BitStringProbState,BMeraCodes.BitStringProbState}",
    "page": "prob_states.jl",
    "title": "Base.kron",
    "category": "method",
    "text": "kron(bsps1::BitStringProbState, bsps2::BitStringProbState)\n\nCompute the kronecker product between the two BitStringProbState array of probabilities.\n\n\n\n"
},

{
    "location": "functionalities/prob_states.html#Kronecker-product-1",
    "page": "prob_states.jl",
    "title": "Kronecker product",
    "category": "section",
    "text": "kron(tps1::TensorProbState, tps2::TensorProbState)\nkron(bsps1::BitStringProbState, bsps2::BitStringProbState)"
},

{
    "location": "functionalities/prob_states.html#BMeraCodes.fix_bits-Tuple{BMeraCodes.TensorProbState,BMeraCodes.Bit,Int64}",
    "page": "prob_states.jl",
    "title": "BMeraCodes.fix_bits",
    "category": "method",
    "text": "fix_bits(tps::TensorProbState, b::Union{Bit, BitState}, inds...)\n\nReturn the probabilities distribution in the form of a TensorProbState with the bits given by inds... fixed to the values given by b. Equivalent to the  probabilities distribution conditional on the bits given by inds... begin b.\n\n\n\n"
},

{
    "location": "functionalities/prob_states.html#BMeraCodes.fix_bits-Tuple{BMeraCodes.BitStringProbState,Union{AbstractArray{BMeraCodes.Bit,N} where N, BMeraCodes.Bit},Vararg{Any,N} where N}",
    "page": "prob_states.jl",
    "title": "BMeraCodes.fix_bits",
    "category": "method",
    "text": "fix_bits(bsps::BitStringProbState, b::Union{Bit, BitState}, inds...)\n\nReturn the probabilities distribution in the form of a BitStringProbState with the bits given by inds... fixed to the values given by b. Equivalent to the  probabilities distribution conditional on the bits given by inds... begin b.\n\n\n\n"
},

{
    "location": "functionalities/prob_states.html#Fix-bits-1",
    "page": "prob_states.jl",
    "title": "Fix bits",
    "category": "section",
    "text": "fix_bits(tps::TensorProbState, b::Bit, ind::Int)\nfix_bits(bsps::BitStringProbState, b::Union{Bit, BitState}, inds...)"
},

{
    "location": "functionalities/prob_states.html#BMeraCodes.BitState-Tuple{BMeraCodes.ZeroProbState}",
    "page": "prob_states.jl",
    "title": "BMeraCodes.BitState",
    "category": "method",
    "text": "BitState(ps::ProbState)\n\nFind the bit string with maximum probability in ps and return the corresponding BitState.\n\n\n\n"
},

{
    "location": "functionalities/prob_states.html#BMeraCodes.ZeroProbState-Tuple{BMeraCodes.BinProbState}",
    "page": "prob_states.jl",
    "title": "BMeraCodes.ZeroProbState",
    "category": "method",
    "text": "ZeroProbState(bps::BinProbState)\n\nReturn a ZeroProbState from the first column of bps.\n\n\n\n"
},

{
    "location": "functionalities/prob_states.html#BMeraCodes.BinProbState-Tuple{BMeraCodes.ZeroProbState}",
    "page": "prob_states.jl",
    "title": "BMeraCodes.BinProbState",
    "category": "method",
    "text": "BinProbState(zps::ZeroProbState)\n\nReturn a BinProbState where the first column is zps and the second is 1 - zps. \n\n\n\n"
},

{
    "location": "functionalities/prob_states.html#BMeraCodes.TensorProbState-Tuple{BMeraCodes.ZeroProbState}",
    "page": "prob_states.jl",
    "title": "BMeraCodes.TensorProbState",
    "category": "method",
    "text": "TensorProbState(zps::ZeroProbState)\n\nConvert zps to a BitStringProbState and then to a TensorProbState.\n\n\n\n"
},

{
    "location": "functionalities/prob_states.html#BMeraCodes.TensorProbState-Tuple{BMeraCodes.BinProbState}",
    "page": "prob_states.jl",
    "title": "BMeraCodes.TensorProbState",
    "category": "method",
    "text": "TensorProbState(bps::BinProbState)\n\nConvert bps to a BitStringProbState and then to a TensorProbState.\n\n\n\n"
},

{
    "location": "functionalities/prob_states.html#BMeraCodes.BitStringProbState-Tuple{BMeraCodes.ZeroProbState}",
    "page": "prob_states.jl",
    "title": "BMeraCodes.BitStringProbState",
    "category": "method",
    "text": "BitStringProbState(zps::ZeroProbState)\n\nConvert zps to a BinProbState and then to a BitStringProbState.\n\n\n\n"
},

{
    "location": "functionalities/prob_states.html#BMeraCodes.BitStringProbState-Tuple{BMeraCodes.BinProbState}",
    "page": "prob_states.jl",
    "title": "BMeraCodes.BitStringProbState",
    "category": "method",
    "text": "BitStringProbState(bps::BinProbState)\n\nReturn the kronecker product between all rows of bps as a BitStringProbState.\n\n\n\n"
},

{
    "location": "functionalities/prob_states.html#BMeraCodes.BitStringProbState-Tuple{BMeraCodes.TensorProbState}",
    "page": "prob_states.jl",
    "title": "BMeraCodes.BitStringProbState",
    "category": "method",
    "text": "BitStringProbState(tps::TensorProbState)\n\nReshape tps to a BitStringProbState.\n\n\n\n"
},

{
    "location": "functionalities/prob_states.html#Conversions-1",
    "page": "prob_states.jl",
    "title": "Conversions",
    "category": "section",
    "text": "BitState(zps::ZeroProbState)\nZeroProbState(bps::BinProbState)\nBinProbState(zps::ZeroProbState)\nTensorProbState(zps::ZeroProbState)\nTensorProbState(bps::BinProbState)\nBitStringProbState(zps::ZeroProbState)\nBitStringProbState(bps::BinProbState)\nBitStringProbState(tps::TensorProbState)"
},

{
    "location": "functionalities/transformations.html#",
    "page": "transformations.jl",
    "title": "transformations.jl",
    "category": "page",
    "text": ""
},

{
    "location": "functionalities/transformations.html#BMeraCodes.Transformation",
    "page": "transformations.jl",
    "title": "BMeraCodes.Transformation",
    "category": "type",
    "text": "Transformation <: Any\n\nAbstract type.\n\nRepresent how to transform BitState and ProbState from top to bottom.\n\nNeeded fields for a composite subtype are \n\npermutation\nn_bits::Int\n\n\n\n"
},

{
    "location": "functionalities/transformations.html#transformations.jl-1",
    "page": "transformations.jl",
    "title": "transformations.jl",
    "category": "section",
    "text": "Transformation"
},

{
    "location": "functionalities/transformations.html#BMeraCodes.VectorTransformation",
    "page": "transformations.jl",
    "title": "BMeraCodes.VectorTransformation",
    "category": "type",
    "text": "VectorTransformation <: Transformation\n\nMutable type.\n\nFields:\n\npermutation::Vector{Int}\nn_bits::Int\n\nA permutation vector of 2^n_bits unique elements between 1 and 2^n_bits. It represent the permutation of a BitString.\n\nCan be instanciated via a Vector{Int} :\n\njulia> VectorTransformation([1,3,2,4])\nVectorTransformation([1, 3, 2, 4], 2)\n\na Matrix{Int} :\n\njulia> VectorTransformation([1 0 0 0 ; 0 1 0 0 ; 0 0 0 1 ; 0 0 1 0])\nVectorTransformation([1, 2, 4, 3], 2)\n\nor a MatrixTransformation :\n\njulia> mt = MatrixTransformation([1 0 0 0 ; 0 1 0 0 ; 0 0 0 1 ; 0 0 1 0]);\njulia> VectorTransformation(mt)\nVectorTransformation([1, 2, 4, 3], 2)\n\nSee contractions.jl for more informations and examples.\n\n\n\n"
},

{
    "location": "functionalities/transformations.html#BMeraCodes.MatrixTransformation",
    "page": "transformations.jl",
    "title": "BMeraCodes.MatrixTransformation",
    "category": "type",
    "text": "MatrixTransformation <: Transformation\n\nMutable type.\n\nFields:\n\npermutation::SparseMatrixCSC{Int,Int}\nn_bits::Int\n\nA sparse matrix with size 2^n_bits x 2^n_bits. Each entries are 0 except for one and only one entry by row and column. It represent the permutation of a BitString.\n\nCan be instanciated via a SparseMatrixCSC{Int,Int} :\n\njulia> smcsc = SparseMatrixCSC{Int,Int}([1 0 0 0 ; 0 1 0 0 ; 0 0 0 1; 0 0 1 0]);\njulia> MatrixTransformation(smcsc)\nMatrixTransformation(\n  [1, 1]  =  1\n  [2, 2]  =  1\n  [4, 3]  =  1\n  [3, 4]  =  1, 2)\n\na Matrix{Int} :\n\njulia> MatrixTransformation([1 0 0 0 ; 0 1 0 0 ; 0 0 0 1 ; 0 0 1 0])\nMatrixTransformation(\n  [1, 1]  =  1\n  [2, 2]  =  1\n  [4, 3]  =  1\n  [3, 4]  =  1, 2)\n\na Vector{Int} :\n\nMatrixTransformation([1,2,4,3])\nMatrixTransformation(\n  [1, 1]  =  1\n  [2, 2]  =  1\n  [4, 3]  =  1\n  [3, 4]  =  1, 2)\n\nor a VectorTransformation :\n\njulia> vt = VectorTransformation([1,2,4,3]);\njulia> MatrixTransformation(vt)\nMatrixTransformation(\n  [1, 1]  =  1\n  [2, 2]  =  1\n  [4, 3]  =  1\n  [3, 4]  =  1, 2)\n\nSee contractions.jl for more informations and examples.\n\n\n\n"
},

{
    "location": "functionalities/transformations.html#Specific-types-1",
    "page": "transformations.jl",
    "title": "Specific types",
    "category": "section",
    "text": "VectorTransformation\nMatrixTransformation"
},

{
    "location": "functionalities/transformations.html#Base.length-Tuple{BMeraCodes.Transformation}",
    "page": "transformations.jl",
    "title": "Base.length",
    "category": "method",
    "text": "length(transform::Transformation)\n\nReturn the number of bits that transform acts on.\n\n\n\n"
},

{
    "location": "functionalities/transformations.html#Base.inv-Tuple{BMeraCodes.VectorTransformation}",
    "page": "transformations.jl",
    "title": "Base.inv",
    "category": "method",
    "text": "inv(transform::transformation)\n\nReturn the inverse transformation of transform.\n\n\n\n"
},

{
    "location": "functionalities/transformations.html#Base.kron-Tuple{BMeraCodes.VectorTransformation,BMeraCodes.VectorTransformation}",
    "page": "transformations.jl",
    "title": "Base.kron",
    "category": "method",
    "text": "kron(transform1::Transformation, transform2::Transform)\n\nReturn the kronecker product between transform1 and transform2. The type  of the result is the same as transform1.\n\n\n\n"
},

{
    "location": "functionalities/transformations.html#Functions-and-methods-1",
    "page": "transformations.jl",
    "title": "Functions and methods",
    "category": "section",
    "text": "length(transform::Transformation)\ninv(vTransform::VectorTransformation)\nkron(vt1::VectorTransformation, vt2::VectorTransformation)"
},

{
    "location": "functionalities/transformations.html#Comparison-methods-1",
    "page": "transformations.jl",
    "title": "Comparison methods",
    "category": "section",
    "text": "The following methods are also implemented : ==(transform1::Transformation, transform2::Transformation)\n!=(transform1::Transformation, transform2::Transformation)"
},

{
    "location": "functionalities/transformations.html#Conversions-1",
    "page": "transformations.jl",
    "title": "Conversions",
    "category": "section",
    "text": "TO DO"
},

{
    "location": "functionalities/contractions.html#",
    "page": "contractions.jl",
    "title": "contractions.jl",
    "category": "page",
    "text": ""
},

{
    "location": "functionalities/contractions.html#Base.:*-Tuple{AbstractArray{BMeraCodes.Bit,N} where N,BMeraCodes.ZeroProbState}",
    "page": "contractions.jl",
    "title": "Base.:*",
    "category": "method",
    "text": "*(bs::BitState, ps::ProbState)\n*(ps::ProbState, bs::BitState)\n\nMultiplication operator realizing a tensor like contraction where bit__ are considered as open legs. This operation commute.\n\nExamples\n\njulia> BitState(\"e_01\")*ZeroProbState([0.1, 0.9, 0.3, 0.7])\nZeroProbState([0.9], 1)\njulia> BinProbState([0.1 0.9 ; 0.9 0.1 ; 0.3 0.7 ; 0.7 0.3]) * BitState(\"e_01\")\nBinProbState([0.9 0.1], 1)\njulia> BitStringProbState(fill(0.0625,16)) * BitState(\"e__1\")\nBitStringProbState([0.25, 0.25, 0.25, 0.25], 2)\n\n\n\n"
},

{
    "location": "functionalities/contractions.html#Base.:*-Tuple{BMeraCodes.Transformation,BMeraCodes.ProbState}",
    "page": "contractions.jl",
    "title": "Base.:*",
    "category": "method",
    "text": "*(transform::Transformation, ps::ProbState)\n\nMultiplication operator realizing a tensor like contraction. This operation doesn\'t commute. The element on the right is consider higher in the circuit. If the type of ps doesn\'t support correlation, a BitStringProbState is return\n\nExamples\n\njulia> VectorTransformation([4, 1, 2, 3]) * TensorProbState([0.40 0.15 ; 0.10 0.35])\nTensorProbState([0.35 0.4; 0.15 0.1], 2)\njulia> MatrixTransformation([0 0 0 1 ; 1 0 0 0 ; 0 1 0 0 ; 0 0 1 0]) * ZeroProbState([0.3, 0.8])\nBitStringProbState([0.14, 0.24, 0.06, 0.56], 2)\n\n\n\n"
},

{
    "location": "functionalities/contractions.html#Base.:*-Tuple{BMeraCodes.ProbState,BMeraCodes.Transformation}",
    "page": "contractions.jl",
    "title": "Base.:*",
    "category": "method",
    "text": "*(ps::ProbState, transform::Transformation)\n\nReturn inv(transform) * ps.\n\n\n\n"
},

{
    "location": "functionalities/contractions.html#Base.:*-Tuple{BMeraCodes.Transformation,BMeraCodes.Transformation}",
    "page": "contractions.jl",
    "title": "Base.:*",
    "category": "method",
    "text": "*(low::Transformation, high::Transformation)\n\nReturn the contraction of both Transformation where low come after high in the circuit. The return type is the same as low.\n\nExamples\n\njulia> VectorTransformation([1,2,4,3]) * VectorTransformation([2,3,1,4])\nVectorTransformation([2, 3, 4, 1], 2)\njulia> MatrixTransformation([1 0 0 0 ; 0 1 0 0 ; 0 0 0 1 ; 0 0 1 0]) * VectorTransformation([2,3,1,4])\nMatrixTransformation(\n  [4, 1]  =  1\n  [1, 2]  =  1\n  [2, 3]  =  1\n  [3, 4]  =  1, 2)\n\n\n\n"
},

{
    "location": "functionalities/contractions.html#contractions.jl-1",
    "page": "contractions.jl",
    "title": "contractions.jl",
    "category": "section",
    "text": "*(bs::BitState, zps::ZeroProbState)\n*(transform::Transformation, ps::ProbState)\n*(ps::ProbState, transform::Transformation)\n*(low::Transformation, high::Transformation)"
},

{
    "location": "functionalities/contractions.html#Chained-contractions-1",
    "page": "contractions.jl",
    "title": "Chained contractions",
    "category": "section",
    "text": "Since every contraction operators * return a object that it contractable, it is possible to chain contractions."
},

{
    "location": "functionalities/contractions.html#Example-1",
    "page": "contractions.jl",
    "title": "Example",
    "category": "section",
    "text": "julia> ps = BitStringProbState([0.1, 0.2, 0.3, 0.4]);\njulia> transform = VectorTransformation([1, 2, 4, 3]);\njulia> bs = BitState(\"_0\");\njulia> ps * transform * bs\nBitStringProbState([0.2, 0.8], 1)"
},

{
    "location": "functionalities/causal_cones.html#",
    "page": "causal_cones.jl",
    "title": "causal_cones.jl",
    "category": "page",
    "text": ""
},

{
    "location": "functionalities/causal_cones.html#causal_cones.jl-1",
    "page": "causal_cones.jl",
    "title": "causal_cones.jl",
    "category": "section",
    "text": ""
},

{
    "location": "functionalities/causal_cones.html#BMeraCodes.CausalCone",
    "page": "causal_cones.jl",
    "title": "BMeraCodes.CausalCone",
    "category": "type",
    "text": "CausalCone <: Any\n\nMutable type.\n\nFields\n\ntop_bit_state::BitState\nfirst_bit_position::Int\ntransform::Transformation\nchilds::Union{Vector{CausalCone}, ProbState}\nlast_contracted_bit_state::BitState\nlast_contracted_prob_state::ProbState\n\nCausal cone must have some open legs to proceed to the decoding. We must also ensure that the number of linked legs of childs equals the length of top_bit_state  so that contraction is possible. There are tree ways to init a CausalCone : \n\nCausalCone(top_bit_state::BitState, first_bit_position::Int, transform::Transformation, childs::Vector{CausalCone})\nCausalCone(top_bit_state::BitState, first_bit_position::Int, transform::Transformation, childs::ProbState)\nCausalCone(top_bit_state::BitState, first_bit_position::Int, transform::Transformation)\n\nThe CausalCone will be contracted at creation if childs is of type Probstate. This will define a value for last_contracted_bit_state and last_contracted_prob_state.\n\nExamples\n\njulia> transform = VectorTransformation([1,2,4,3]);\njulia> left_prob = ZeroProbState([0.1, 0.1]);\njulia> right_prob = ZeroProbState([0.9, 0.1]);\njulia> left_cone = CausalCone(BitState(\"e_\"), 1, transform, left_prob)\nTop bit state : \"e_\"\nFirst bit position : 1\nTransformation : [1, 2, 4, 3]\nZeroProbState :\n[0.1, 0.1]\n\njulia> left_cone.last_contracted_bit_state\n2-element Array{Bit,1}:\n bit_e\n bit__\n\njulia> left_cone.last_contracted_prob_state\nBitStringProbState([0.82, 0.18], 1)\n\njulia> right_cone = CausalCone(BitState(\"e_\"), 1, transform, right_prob);\njulia> cone = CausalCone(BitState(\"_0\"), 1, transform, [left_cone, right_cone])\nTop bit state : \"_0\"\nFirst bit position : 1\nTransformation : [1, 2, 4, 3]\nChilds :\n   Top bit state : \"e_\"\n   First bit position : 1\n   Transformation : [1, 2, 4, 3]\n   ZeroProbState :\n   [0.1, 0.1]\n\n   Top bit state : \"e_\"\n   First bit position : 1\n   Transformation : [1, 2, 4, 3]\n   ZeroProbState :\n   [0.9, 0.1]\n\njulia> cone.last_contracted_bit_state\nERROR: UndefRefError: access to undefined reference\n\njulia> no_child_cone = CausalCone(BitState(\"e_\"), 1, transform)\nTop bit state : \"e_\"\nFirst bit position : 1\nTransformation : [1, 2, 4, 3]\nNo child.\n\njulia> no_child_cone.last_contracted_bit_state\nERROR: UndefRefError: access to undefined reference\n\n\n\n"
},

{
    "location": "functionalities/causal_cones.html#Causal-Cone-1",
    "page": "causal_cones.jl",
    "title": "Causal Cone",
    "category": "section",
    "text": "CausalCone"
},

{
    "location": "functionalities/causal_cones.html#Alias-1",
    "page": "causal_cones.jl",
    "title": "Alias",
    "category": "section",
    "text": "const ConeChilds = Union{Vector{CausalCone}, ProbState}"
},

{
    "location": "functionalities/causal_cones.html#Base.length-Tuple{BMeraCodes.CausalCone}",
    "page": "causal_cones.jl",
    "title": "Base.length",
    "category": "method",
    "text": "length(cc::CausalCone)\n\nReturn the length of top_bit_state.    \n\n\n\n"
},

{
    "location": "functionalities/causal_cones.html#BMeraCodes.depth",
    "page": "causal_cones.jl",
    "title": "BMeraCodes.depth",
    "category": "function",
    "text": "depth(cc::CausalCone)\n\nReturn the depth of a causal cone, corresponding to the number of descendant of the causal cone. For example, a causal cone with childs and grandchilds is a causal cone of depth of 3.\n\n\n\n"
},

{
    "location": "functionalities/causal_cones.html#BMeraCodes.merge_childs",
    "page": "causal_cones.jl",
    "title": "BMeraCodes.merge_childs",
    "category": "function",
    "text": "merge_childs(cc::CausalCone)\n\nCompute the kronecker between all childs and permute the result according to the number of childs and cc.first_bit_position to fit cc bottom legs positions.\n\n\n\n"
},

{
    "location": "functionalities/causal_cones.html#BMeraCodes.contract!",
    "page": "causal_cones.jl",
    "title": "BMeraCodes.contract!",
    "category": "function",
    "text": "contract!(cc::CausalCone)\n\nPerform the contraction on a causal cone. It corresponds to an updated value of last_contracted_bit_state and last_contracted_prob_state.\n\n\n\n"
},

{
    "location": "functionalities/causal_cones.html#BMeraCodes.set_childs!",
    "page": "causal_cones.jl",
    "title": "BMeraCodes.set_childs!",
    "category": "function",
    "text": "set_childs!(cc::CausalCone, childs::Vector{CausalCone})\n\nSet the attribute childs for a causal cone with the childs corresponding to a vector of causal cone. The length of the causal cone must be equal to the number of open legs of the childs.\n\n\n\nset_childs!(cc::CausalCone, childs::ProbState)\n\nSet the childs when they are a probstate. The length of the probstate must be equal to the length of the causal cone. Will contract the cone.\n\n\n\n"
},

{
    "location": "functionalities/causal_cones.html#Functions-and-methods-1",
    "page": "causal_cones.jl",
    "title": "Functions and methods",
    "category": "section",
    "text": "length(cc::CausalCone)\ndepth\nmerge_childs\ncontract!\nset_childs!"
},

{
    "location": "functionalities/causal_cones.html#Show-1",
    "page": "causal_cones.jl",
    "title": "Show",
    "category": "section",
    "text": "show(io::IO, cc::CausalCone, space = \"\")"
},

{
    "location": "functionalities/gate.html#",
    "page": "gate.jl",
    "title": "gate.jl",
    "category": "page",
    "text": ""
},

{
    "location": "functionalities/gate.html#BMeraCodes.Gate",
    "page": "gate.jl",
    "title": "BMeraCodes.Gate",
    "category": "type",
    "text": "Gate <: Any\n\nMutable type.\n\nFields\n\nn_bits::Int\ntransform::Transformation\nsimplification::Function\nmemory::Dict{BitState, Tuple{BitState, Vector{Vector{Int}}, Vector{Gate}}}\n\nA Gate transform the ProbState via transform and use simplification to compute  the way a BitState interact with the gate. memory is use to keep track of already done simplification in order to reuse computation. To create a new gate, we only need to specify the transform and then manualy give the simplification. This procedure is mandatory to create Gate that can simplify to themself. \n\nExample\n\njulia> not = Gate(VectorTransformation([2,1]))\n1 bit(s) cnot gate\njulia> not.simplification = bs -> (Int(bs[1]) < 2 ? [Bit(1 - Int(bs[1]))] : copy(bs), [[1]], [not]);\njulia> not[bit_1]\n(Bit[bit_0], Array{Int64,1}[[1]], Gate[1 bit(s) cnot gate])\n\nSee Elementary gates for more details and examples.\n\n\n\n"
},

{
    "location": "functionalities/gate.html#BMeraCodes.GateSet",
    "page": "gate.jl",
    "title": "BMeraCodes.GateSet",
    "category": "type",
    "text": "A dictionnary of Gate that can be access by names. Can be use to reduce the number of needed  Gate. All Gate in a GateSet will simplify to Gate in that GateSet. So, it is a better  usage of the memory parameter of Gate.\n\nSee Elementary gates for more details and examples.\n\n\n\n"
},

{
    "location": "functionalities/gate.html#gate.jl-1",
    "page": "gate.jl",
    "title": "gate.jl",
    "category": "section",
    "text": "Gate\nGateSet"
},

{
    "location": "functionalities/gate.html#BMeraCodes.identity_gate",
    "page": "gate.jl",
    "title": "BMeraCodes.identity_gate",
    "category": "function",
    "text": "identity_gate(n_bits::Int)\n\nReturn a Gate object that act like the identity on n_bits bits. \n\nExample\n\njulia> id = identity_gate(2)\n2 bit(s) identity gate\njulia> id[BitState(\"0_\")]\n(Bit[bit_0, bit__], Array{Int64,1}[[1, 2]], Gate[2 bit(s) identity gate])\n\n\n\n"
},

{
    "location": "functionalities/gate.html#BMeraCodes.identity_gate!",
    "page": "gate.jl",
    "title": "BMeraCodes.identity_gate!",
    "category": "function",
    "text": "identity_gate!(gate_set::GateSet, n_bits::Int)\n\nReturn a reference to the n_bits identity gate in gate_set. If the gate didn\'t exist, it is created and added in gate_set with the name id_{n_bits}.\n\nExample\n\njulia> gs = GateSet()\nDict{String,Gate} with 0 entries\njulia> identity_gate!(gs,2);\njulia> gs\nDict{String,Gate} with 1 entry:\n  \"id_2\" => 2 bit(s) identity gate\n\n\n\n"
},

{
    "location": "functionalities/gate.html#BMeraCodes.not_gate",
    "page": "gate.jl",
    "title": "BMeraCodes.not_gate",
    "category": "function",
    "text": "not_gate()\n\nReturn a Gate object that act like the not gate. \n\nExample\n\njulia> not = not_gate()\n1 bit(s) cnot gate\njulia> not[bit_1]\n(Bit[bit_0], Array{Int64,1}[[1]], Gate[1 bit(s) cnot gate])\n\n\n\n"
},

{
    "location": "functionalities/gate.html#BMeraCodes.not_gate!",
    "page": "gate.jl",
    "title": "BMeraCodes.not_gate!",
    "category": "function",
    "text": "not_gate!(gate_set::GateSet)\n\nReturn a reference to the not gate in gate_set. If the gate didn\'t exist, it is created and added ingate_setwith the namesnotandcnot_1`. Note that  both names refer to the same gate.\n\nExample\n\njulia> gs = GateSet()\nDict{String,Gate} with 0 entries\njulia> not_gate!(gs);\njulia> gs\nDict{String,Gate} with 2 entries:\n  \"cnot_1\" => 1 bit(s) cnot gate\n  \"not\"    => 1 bit(s) cnot gate\n\n\n\n"
},

{
    "location": "functionalities/gate.html#BMeraCodes.cnot_gate",
    "page": "gate.jl",
    "title": "BMeraCodes.cnot_gate",
    "category": "function",
    "text": "cnot_gate(n_bits::Int = 2)\n\nReturn a Gate object that act like the cnot gate on n_bits bits. The last bit  is the target and all the others are control. If n_bits = 1, it became the not gate. \n\nExamples\n\njulia> cnot = cnot_gate()\n2 bit(s) cnot gate\njulia> cnot[BitState(\"10\")]\n(Bit[bit_1, bit_1], Array{Int64,1}[[1], [2]], Gate[1 bit(s) identity gate, 1 bit(s) cnot gate])\njulia> cnot[BitState(\"e_\")]\n(Bit[bit__, bit__], Array{Int64,1}[[1, 2]], Gate[2 bit(s) cnot gate])\n\n\n\n"
},

{
    "location": "functionalities/gate.html#BMeraCodes.cnot_gate!",
    "page": "gate.jl",
    "title": "BMeraCodes.cnot_gate!",
    "category": "function",
    "text": "cnot_gate!(gate_set::GateSet, n_bits::Int = 2)\n\nReturn a reference to the cnot gate in gate_set. If the gate didn\'t exist, it is created and added ingate_setwith the namecnot_{n_bits}`. \n\nExample\n\njulia> gs = GateSet()\nDict{String,Gate} with 0 entries\njulia> not_gate!(gs);\njulia> gs\nDict{String,Gate} with 2 entries:\n  \"cnot_1\" => 1 bit(s) cnot gate\n  \"not\"    => 1 bit(s) cnot gate\n\n\n\n"
},

{
    "location": "functionalities/gate.html#BMeraCodes.swap_gate",
    "page": "gate.jl",
    "title": "BMeraCodes.swap_gate",
    "category": "function",
    "text": "swap_gate()\n\nReturn a Gate object that act like the swap gate on 2 bits. \n\nExamples\n\njulia> swap = swap_gate()\n2 bit(s) gate : [1, 3, 2, 4]\njulia> swap[BitState(\"10\")]\n(Bit[bit_0, bit_1], Array{Int64,1}[[1, 2]], Gate[2 bit(s) arbitrary gate : [1, 3, 2, 4]])\n\n\n\n"
},

{
    "location": "functionalities/gate.html#Elementary-gates-1",
    "page": "gate.jl",
    "title": "Elementary gates",
    "category": "section",
    "text": "identity_gate\nidentity_gate!\nnot_gate\nnot_gate!\ncnot_gate\ncnot_gate!\nswap_gate"
},

{
    "location": "functionalities/gate.html#Elementary-gate-set-1",
    "page": "gate.jl",
    "title": "Elementary gate set",
    "category": "section",
    "text": "We provide an elementary gate set with three gates : the not, the cnot and the identity on 1 bit. It can serve as a basis to build more GateSet or to get access to a single get.julia> elementary_gates\nDict{String,Gate} with 5 entries:\n  \"cnot_2\" => 2 bit(s) cnot gate\n  \"cnot_1\" => 1 bit(s) cnot gate\n  \"not\"    => 1 bit(s) cnot gate\n  \"cnot\"   => 2 bit(s) cnot gate\n  \"id_1\"   => 1 bit(s) identity gate\njulia> cnot = elementary_gates[\"cnot\"]\n2 bit(s) cnot gate"
},

{
    "location": "functionalities/gate.html#Base.kron-Tuple{BMeraCodes.Gate,BMeraCodes.Gate}",
    "page": "gate.jl",
    "title": "Base.kron",
    "category": "method",
    "text": "kron(left_gate::Gate, right_gate::Gate)\n\nReturn a new Gate where transform is the kronecker product between  left_gate.transform and right_gate.transform and simplification is a  concatenation of left_gate.transform and right_gate.transform.\n\nExample\n\njulia> id = elementary_gates[\"id_1\"];\njulia> not = elementary_gates[\"not\"];\njulia> id_not = kron(id, not)\n2 bit(s) gate : [2, 1, 4, 3]\njulia> id_not[BitState(\"01\")]\n(Bit[bit_0, bit_0], Array{Int64,1}[[1], [2]], Gate[1 bit(s) identity gate, 1 bit(s) cnot gate])\n\n\n\n"
},

{
    "location": "functionalities/gate.html#Base.:*-Tuple{BMeraCodes.Gate,BMeraCodes.Gate}",
    "page": "gate.jl",
    "title": "Base.:*",
    "category": "method",
    "text": "*(down_gate::Gate, up_gate::Gate)\n\nReturn a new Gate where transform is given by down_gate.transform * up_gate.transform and simplification is obtain by parsing the result of up_gate.simplification to  down_gate.simplification. The resulting simplification may not be optimaly simplify.\n\nExample\n\njulia> not = elementary_gates[\"not\"];\njulia> not * not\n1 bit(s) identity gate\n\n\n\n"
},

{
    "location": "functionalities/gate.html#Base.getindex-Tuple{BMeraCodes.Gate,AbstractArray{BMeraCodes.Bit,N} where N}",
    "page": "gate.jl",
    "title": "Base.getindex",
    "category": "method",
    "text": "getindex(gate::Gate, b::Union{Bit, BitState})\n\nReturn a tuple of the simplified BitState, gate positions and gates. Use this function istead of gate.simplification to maximise reuse of computation. The type of b must be BitState if the length of gate is more than one.\n\nExamples\n\njulia> ccnot = cnot_gate(3);\njulia> ccnot[BitState(\"1_0\")]\n(Bit[bit_1, bit__, bit__], Array{Int64,1}[[1], [2, 3]], Gate[1 bit(s) identity gate, 2 bit(s) cnot gate])\njulia> ccnot[BitState(\"_10\")]\n(Bit[bit__, bit_1, bit__], Array{Int64,1}[[2], [1, 3]], Gate[1 bit(s) identity gate, 2 bit(s) cnot gate])\njulia> ccnot[BitState(\"_00\")]\n(Bit[bit__, bit_0, bit_0], Array{Int64,1}[[1], [2], [3]], Gate[1 bit(s) identity gate, 1 bit(s) identity gate, 1 bit(s) identity gate])\n\n\n\n"
},

{
    "location": "functionalities/gate.html#Base.length-Tuple{BMeraCodes.Gate}",
    "page": "gate.jl",
    "title": "Base.length",
    "category": "method",
    "text": "length(gate::Gate)\n\nReturn the number of bits that gate act on.\n\n\n\n"
},

{
    "location": "functionalities/gate.html#Base.:==-Tuple{BMeraCodes.Gate,BMeraCodes.Gate}",
    "page": "gate.jl",
    "title": "Base.:==",
    "category": "method",
    "text": "==(g1::Gate, g2::Gate)\n\nCheck if g1.transform == g2.transform.\n\n\n\n"
},

{
    "location": "functionalities/gate.html#Functions-and-methods-1",
    "page": "gate.jl",
    "title": "Functions and methods",
    "category": "section",
    "text": "kron(left_gate::Gate, right_gate::Gate)\n*(down_gate::Gate, up_gate::Gate)\ngetindex(gate::Gate, bs::BitState)\nlength(gate::Gate)\n==(g1::Gate, g2::Gate)"
},

{
    "location": "functionalities/gate_position.html#",
    "page": "gate_position.jl",
    "title": "gate_position.jl",
    "category": "page",
    "text": ""
},

{
    "location": "functionalities/gate_position.html#gate_position.jl-1",
    "page": "gate_position.jl",
    "title": "gate_position.jl",
    "category": "section",
    "text": ""
},

{
    "location": "functionalities/gate_position.html#BMeraCodes.GatePosition",
    "page": "gate_position.jl",
    "title": "BMeraCodes.GatePosition",
    "category": "type",
    "text": "GatePosition <: Any\n\nMutable type.\n\nFields\n\ngate::Gate\nfirst::Int\nlast::Int\n\nGate position define a gate with a relative position in the circuit starting to first up to last. Obsviously, a valid GatePosition must contains gate with length equals to  (last - first + 1). It is also possible to create empty gate position  (useful to preallocate memory). In that case, default values for first and last are set to 0.\n\nThe main advantage of GatePosition over adding a position parameter to the Gate type, is that by dissocing the position and the Gate, we can use the same Gate a lot of time only by  refering to it in different GatePosition. This will allows more reuse of computation.\n\nThere are three ways to initialize a GatePostion:\n\nGatePosition() # Empty\nGatePosition(gate::Gate, first::Int, last::Int)\nGatePosition(gate::Gate, pos::Int)\n\nExamples\n\n# Empty GatePosition\njulia> GatePosition()\nGatePosition(#undef, 0, 0)\n\njulia> cnot = elementary_gates[\"cnot\"];\n\njulia> gp = GatePosition(cnot, 7, 8)\nGatePosition(2 bit(s) cnot gate, 7, 8)\n\n# Wrong initialization since length(cnot) = 2.\n\njulia> gp = GatePosition(cnot, 7, 9)\nERROR: Gate length must fit with first and last bits.\n\n\n\n"
},

{
    "location": "functionalities/gate_position.html#GatePosition-1",
    "page": "gate_position.jl",
    "title": "GatePosition",
    "category": "section",
    "text": "GatePosition"
},

{
    "location": "functionalities/gate_position.html#BMeraCodes.span",
    "page": "gate_position.jl",
    "title": "BMeraCodes.span",
    "category": "function",
    "text": "span(gates::Vector{GatePosition})\n\nReturn the span of a set of GatePosition (first and last bit). If the gates are not continuous, an error is thrown.\n\njulia> id, cnot = elementary_gates[\"id_1\"], elementary_gates[\"cnot\"];\njulia> gates = [GatePosition(cnot, 1,2), GatePosition(cnot,4,5), GatePosition(id, 3)];\n\njulia> span(gates)\n2-element Array{Int64,1}:\n 1\n 5\n\n\n\n"
},

{
    "location": "functionalities/gate_position.html#Base.getindex-Tuple{BMeraCodes.GatePosition,AbstractArray{BMeraCodes.Bit,N} where N}",
    "page": "gate_position.jl",
    "title": "Base.getindex",
    "category": "method",
    "text": "getindex(gp::GatePosition, bs::BitState)\n\nReturn the simplify GatePosition for the input bitstate. \n\njulia> cnot = elementary_gates[\"cnot\"];\njulia> gp = GatePosition(cnot, 6, 7);\n\njulia> gp[BitState(\"10\")]\n(GatePosition[GatePosition(1 bit(s) identity gate, 6, 6), GatePosition(1 bit(s) cnot gate, 7, 7)], Bit[bit_1, bit_1])\n\n\n\n"
},

{
    "location": "functionalities/gate_position.html#Functions-and-methods-1",
    "page": "gate_position.jl",
    "title": "Functions and methods",
    "category": "section",
    "text": "span\ngetindex(gp::GatePosition, bs::BitState)"
},

{
    "location": "functionalities/gate_position.html#Comparison-methods-1",
    "page": "gate_position.jl",
    "title": "Comparison methods",
    "category": "section",
    "text": "The following methods are implemented : ==(gp1::GatePosition, gp2::GatePosition)\nisless(gp1::GatePosition, gp2::GatePosition)\nhash(gp::GatePosition)"
},

{
    "location": "functionalities/sublayers.html#",
    "page": "sublayers.jl",
    "title": "sublayers.jl",
    "category": "page",
    "text": ""
},

{
    "location": "functionalities/sublayers.html#sublayers.jl-1",
    "page": "sublayers.jl",
    "title": "sublayers.jl",
    "category": "section",
    "text": ""
},

{
    "location": "functionalities/sublayers.html#BMeraCodes.SubLayer",
    "page": "sublayers.jl",
    "title": "BMeraCodes.SubLayer",
    "category": "type",
    "text": "SubLayer <: Any\n\nMutable type.\n\nFields\n\nn_bits::Int\ngates::Vector{GatePosition}\n\nContain a Vector of GatePosition that must be ordered. Also, there must be one and only one gate in gates for the n_bits bits, but each gate can span more than one bit.\n\nExamples\n\n# Sublayer containing two bits cnot.\njulia> n_bits = 2;\njulia> cnot = elementary_gates[\"cnot\"];\njulia> gates = [GatePosition(cnot, 1, 2)];\njulia> sublayer = SubLayer(n_bits, gates)\nSubLayer(2, GatePosition[GatePosition(2 bit(s) cnot gate, 1, 2), GatePosition(2 bit(s) cnot gate, 1, 2)])\n\n\n\n"
},

{
    "location": "functionalities/sublayers.html#Sublayer-1",
    "page": "sublayers.jl",
    "title": "Sublayer",
    "category": "section",
    "text": "SubLayer"
},

{
    "location": "functionalities/sublayers.html#Functions-and-methods-1",
    "page": "sublayers.jl",
    "title": "Functions and methods",
    "category": "section",
    "text": ""
},

{
    "location": "functionalities/sublayers.html#BMeraCodes.propagate-Tuple{BMeraCodes.SubLayer,AbstractArray{BMeraCodes.Bit,N} where N,Int64,Int64}",
    "page": "sublayers.jl",
    "title": "BMeraCodes.propagate",
    "category": "method",
    "text": "propagate(sl::SubLayer, input_bs::BitState, first_bit::Int, last_bit::Int)\n\nPropagates input bitstate through a sublayer starting from first_bit to last_bit  and output the resulting bitstate with his span.\n\npropagate(sl::SubLayer, input_bs::BitState)\n\nPropagate for the whole sublayer.\n\npropagate(sl::SubLayer, input_bs::BitState, position::Int)\n\nPropagate for one bit at a given position.\n\nExamples\n\njulia> not = elementary_gates[\"not\"];\njulia> cnot = elementary_gates[\"cnot\"];\njulia> sl = SubLayer(5, [GatePosition(cnot, 1, 2), GatePosition(cnot, 3, 4), GatePosition(not, 5)]);\n\njulia> propagate(sl, BitState(\"e_010\"), 1, 2)\n(Bit[bit__, bit__], [1, 2])\n\njulia> propagate(sl, BitState(\"eee_0\"), 5)\n(Bit[bit_1], [5, 5])\n\njulia> propagate(sl, BitState(\"eee_0\"))\n(Bit[bit_e, bit_e, bit__, bit__, bit_1], [1, 5])\n\n# Error\njulia> propagate(sl, BitState(\"01\"), 1)\nERROR: Bit State must be the same length as sublayer.\n\n\n\n"
},

{
    "location": "functionalities/sublayers.html#BMeraCodes.simplify!-Tuple{BMeraCodes.SubLayer,AbstractArray{BMeraCodes.Bit,N} where N,Int64,Int64}",
    "page": "sublayers.jl",
    "title": "BMeraCodes.simplify!",
    "category": "method",
    "text": "simplify!(sl::SubLayer, input_bs::BitState, first_bit::Int, last_bit::Int)\n\nSimplify the gates in the sublayer starting for first_bit to last_bit and output  the simplify array of gate position. \n\nsimplify!(sl::SubLayer, input_bs::BitState)\n\nSimplify the whole sublayer.\n\nsimplify!(sl::SubLayer, input_bs::BitState, position::Int)\n\nSimplify one bit at the position given by `position.\n\nExample\n\njulia> not = elementary_gates[\"not\"];\njulia> cnot = elementary_gates[\"cnot\"];\njulia> sl = SubLayer(5, [GatePosition(cnot, 1, 2), GatePosition(cnot, 3, 4), GatePosition(not, 5)]);\n\njulia> simplify!(sl, BitState(\"eeee_\"))\njulia> sl.gates\n5-element Array{GatePosition,1}:\n GatePosition(1 bit(s) identity gate, 1, 1)\n GatePosition(1 bit(s) identity gate, 2, 2)\n GatePosition(1 bit(s) identity gate, 3, 3)\n GatePosition(1 bit(s) identity gate, 4, 4)\n GatePosition(1 bit(s) cnot gate, 5, 5)  \n\n\n\n"
},

{
    "location": "functionalities/sublayers.html#Propagate-and-simplification-1",
    "page": "sublayers.jl",
    "title": "Propagate & simplification",
    "category": "section",
    "text": "propagate(sl::SubLayer, input_bs::BitState, first_bit::Int, last_bit::Int)\nsimplify!(sl::SubLayer, input_bs::BitState, first_bit::Int, last_bit::Int)"
},

{
    "location": "functionalities/sublayers.html#Base.getindex-Tuple{BMeraCodes.SubLayer,Int64}",
    "page": "sublayers.jl",
    "title": "Base.getindex",
    "category": "method",
    "text": "getindex(sl::SubLayer, ind::Int)\n\nReturns the gate position corresponding to the position ind in the array.\n\n\n\n"
},

{
    "location": "functionalities/sublayers.html#Base.getindex-Tuple{BMeraCodes.SubLayer,Vararg{Any,N} where N}",
    "page": "sublayers.jl",
    "title": "Base.getindex",
    "category": "method",
    "text": "getindex(sl::SubLayer, inds...)\n\nReturns an array containing only the unique gates elements corresponding to the positions ind... in the array. \n\n\n\n"
},

{
    "location": "functionalities/sublayers.html#Base.length-Tuple{BMeraCodes.SubLayer}",
    "page": "sublayers.jl",
    "title": "Base.length",
    "category": "method",
    "text": "length(sl::SubLayer)\n\nReturns the number of bits for the sublayer.\n\n\n\n"
},

{
    "location": "functionalities/sublayers.html#BMeraCodes.get_transform",
    "page": "sublayers.jl",
    "title": "BMeraCodes.get_transform",
    "category": "function",
    "text": "get_transform(sl::SubLayer, first_bit::Int, last_bit::Int)\n\nCompute the resulting transformation as a VectorTransformation starting from first_bit to last_bit.\n\n\n\nget_transform(bl::BranchingLayer)\n\nReturn the global transformation of bl.\n\n\n\nget_transform(layer::Layer)\n\nReturn the global transformation of layer.\n\n\n\n"
},

{
    "location": "functionalities/sublayers.html#Utilitary-functions-1",
    "page": "sublayers.jl",
    "title": "Utilitary functions",
    "category": "section",
    "text": "getindex(sl::SubLayer, ind::Int)\ngetindex(sl::SubLayer, inds...)\nlength(sl::SubLayer)\nget_transform"
},

{
    "location": "functionalities/sublayers.html#Comparison-methods-1",
    "page": "sublayers.jl",
    "title": "Comparison methods",
    "category": "section",
    "text": "The following method is also implemented : ==(sl1::SubLayer, sl2::SubLayer)"
},

{
    "location": "functionalities/branching_layers.html#",
    "page": "branching_layers.jl",
    "title": "branching_layers.jl",
    "category": "page",
    "text": ""
},

{
    "location": "functionalities/branching_layers.html#branching_layers.jl-1",
    "page": "branching_layers.jl",
    "title": "branching_layers.jl",
    "category": "section",
    "text": ""
},

{
    "location": "functionalities/branching_layers.html#BMeraCodes.BranchingLayer",
    "page": "branching_layers.jl",
    "title": "BMeraCodes.BranchingLayer",
    "category": "type",
    "text": "BranchingLayer <: Any\n\nMutable type.\n\nFields\n\nn_bits::Int\nsublayers::Vector{SubLayer}\nstate::BitState\ncone::CausalCone\n\nA vector of sublayers for n_bits with possibly a causal cone and a state. There are two ways to initialize a BranchingLayer:\n\nBranchingLayer(first_open_leg::Int, sublayers::Vector{SubLayer})\nBranchingLayer(sublayers::Vector{SubLayer})\n\nWhen first_open_leg is specify, a CausalCone is initialize automatically.\n\nExamples\n\n# Initialization of the branching layer.\njulia> id, not, cnot = elementary_gates[\"id_1\"], elementary_gates[\"not\"], elementary_gates[\"cnot\"];\njulia> n_bits = 8;\njulia> sub1 = SubLayer(8, [GatePosition(cnot, 2*i - 1, 2*i) for i in 1:4]);\njulia> sub2 = SubLayer(8, vcat(GatePosition(id, 1), [GatePosition(cnot, 2*i, 2*i + 1) for i in 1:3], GatePosition(id, 8)));\njulia> sublayers = [sub1, sub2];\njulia> bl = BranchingLayer(n_bits, sublayers);\n\njulia> bl.state\n8-element Array{Bit,1}:\n bit_e\n bit_e\n bit_e\n bit_e\n bit_e\n bit_e\n bit_e\n bit__\n\njulia> bl.cone\nTop bit state : \"ee_\"\nFirst bit position : 6\nTransformation : [1, 2, 4, 3, 8, 7, 5, 6]\nNo child.\n\n\n\n"
},

{
    "location": "functionalities/branching_layers.html#BranchingLayer-1",
    "page": "branching_layers.jl",
    "title": "BranchingLayer",
    "category": "section",
    "text": "BranchingLayer"
},

{
    "location": "functionalities/branching_layers.html#General-methods-1",
    "page": "branching_layers.jl",
    "title": "General methods",
    "category": "section",
    "text": ""
},

{
    "location": "functionalities/branching_layers.html#BMeraCodes.set_state!-Tuple{BMeraCodes.BranchingLayer,AbstractArray{BMeraCodes.Bit,N} where N}",
    "page": "branching_layers.jl",
    "title": "BMeraCodes.set_state!",
    "category": "method",
    "text": "set_state!(bl::BranchingLayer, bs::BitState)\n\nSet a state for a branching layer and initialize the causal cone.\n\nExamples\n\njulia> cnot = elementary_gates[\"cnot\"];\njulia> sub = SubLayer(8, [GatePosition(cnot, 2*i - 1, 2*i) for i in 1:4]);\njulia> bl = BranchingLayer([sub]);\n\njulia> bl.state\nERROR: UndefRefError: access to undefined reference\n\njulia> bl.cone\nERROR: UndefRefError: access to undefined reference\n\njulia> set_state!(bl, BitState(\"eeeeeee_\"));\n\njulia> bl.state\n8-element Array{Bit,1}:\n bit_e\n bit_e\n bit_e\n bit_e\n bit_e\n bit_e\n bit_e\n bit__\n\njulia> bl.cone\nTop bit state : \"e_\"\nFirst bit position : 7\nTransformation : [1, 2, 4, 3]\nNo child.\n\n\n\n"
},

{
    "location": "functionalities/branching_layers.html#BMeraCodes.change_state!-Tuple{BMeraCodes.BranchingLayer,AbstractArray{BMeraCodes.Bit,N} where N,Int64,Int64}",
    "page": "branching_layers.jl",
    "title": "BMeraCodes.change_state!",
    "category": "method",
    "text": "change_state!(bl::BranchingLayer, bs::BitState, first_bit::Int, last_bit::Int)\n\nChange the bitstate at the top of the branching layer starting from first_bit up to last_bit and initialize the causal cone if needed.\n\n\n\n"
},

{
    "location": "functionalities/branching_layers.html#BMeraCodes.change_state!-Tuple{BMeraCodes.BranchingLayer,AbstractArray{BMeraCodes.Bit,N} where N}",
    "page": "branching_layers.jl",
    "title": "BMeraCodes.change_state!",
    "category": "method",
    "text": "change_state!(bl::BranchingLayer, bs::BitState)\n\nChange state for the whole branching layer.\n\n\n\n"
},

{
    "location": "functionalities/branching_layers.html#BMeraCodes.change_state!-Tuple{BMeraCodes.BranchingLayer,AbstractArray{BMeraCodes.Bit,N} where N,Int64}",
    "page": "branching_layers.jl",
    "title": "BMeraCodes.change_state!",
    "category": "method",
    "text": "change_state!(bl::BranchingLayer, bs::BitState, position::Int)\n\nChange state for one bit position.\n\nExamples\n\nConsider the same branching layer as in the examples for BranchingLayer.\n\njulia> change_state!(bl, BitState(\"_1\"), 7, 8);\n\njulia> bl.state\n8-element Array{Bit,1}:\n bit_e\n bit_e\n bit_e\n bit_e\n bit_e\n bit_e\n bit__\n bit_1\n\njulia> bl.cone\nTop bit state : \"e_1\"\nFirst bit position : 6\nTransformation : [1, 2, 4, 3, 8, 7, 5, 6]\nNo child.\n\n\n\n"
},

{
    "location": "functionalities/branching_layers.html#Set-state-and-change-state-1",
    "page": "branching_layers.jl",
    "title": "Set state and change state",
    "category": "section",
    "text": "set_state!(bl::BranchingLayer, bs::BitState)\nchange_state!(bl::BranchingLayer, bs::BitState, first_bit::Int, last_bit::Int)\nchange_state!(bl::BranchingLayer, bs::BitState)\nchange_state!(bl::BranchingLayer, bs::BitState, position::Int)"
},

{
    "location": "functionalities/branching_layers.html#BMeraCodes.propagate-Tuple{BMeraCodes.BranchingLayer,Int64,Int64}",
    "page": "branching_layers.jl",
    "title": "BMeraCodes.propagate",
    "category": "method",
    "text": "propagate(bl::BranchingLayer, first_bit::Int, last_bit::Int)\n\nCompute the output state at the bottom of the branching layer starting from first_bit up to last_bit. \n\n\n\n"
},

{
    "location": "functionalities/branching_layers.html#BMeraCodes.propagate-Tuple{BMeraCodes.BranchingLayer,Int64}",
    "page": "branching_layers.jl",
    "title": "BMeraCodes.propagate",
    "category": "method",
    "text": "propagate(bl::BranchingLayer, position::Int)\n\nPropagate for one bit.\n\n\n\n"
},

{
    "location": "functionalities/branching_layers.html#BMeraCodes.propagate-Tuple{BMeraCodes.BranchingLayer}",
    "page": "branching_layers.jl",
    "title": "BMeraCodes.propagate",
    "category": "method",
    "text": "propagate(bl::BranchingLayer)\n\nPropagate for the whole branching layer.\n\nExamples\n\nConsider the same branching layer as in the examples for BranchingLayer.\n\njulia> propagate(bl, 8)\n(Bit[bit__, bit__, bit__], [6, 8])\n\n\n\n"
},

{
    "location": "functionalities/branching_layers.html#BMeraCodes.simplify!-Tuple{BMeraCodes.BranchingLayer,Int64,Int64}",
    "page": "branching_layers.jl",
    "title": "BMeraCodes.simplify!",
    "category": "method",
    "text": "simplify!(bl::BranchingLayer, first_bit::Int, last_bit::Int)\n\nSimplify the branching layer starting from first_bit up to last_bit according to the input bitstate resulting in a change in the sublayers.\n\n\n\n"
},

{
    "location": "functionalities/branching_layers.html#BMeraCodes.simplify!-Tuple{BMeraCodes.BranchingLayer,Int64}",
    "page": "branching_layers.jl",
    "title": "BMeraCodes.simplify!",
    "category": "method",
    "text": "simplify!(bl::BranchingLayer, position::Int)\n\nSimplify for one bit.\n\n\n\n"
},

{
    "location": "functionalities/branching_layers.html#BMeraCodes.simplify!-Tuple{BMeraCodes.BranchingLayer}",
    "page": "branching_layers.jl",
    "title": "BMeraCodes.simplify!",
    "category": "method",
    "text": "simplify!(bl::BranchingLayer)\n\nSimplify for the whole branching layer.\n\nExamples\n\nConsider the same branching layer as in the examples for BranchingLayer.\n\njulia> id, not, cnot = elementary_gates[\"id_1\"], elementary_gates[\"not\"], elementary_gates[\"cnot\"];\njulia> n_bits = 4;\njulia> sub1 = SubLayer(4, [GatePosition(cnot, 1, 2), GatePosition(cnot, 3,4)]);\njulia> sub2 = SubLayer(4, [GatePosition(id, 1), GatePosition(cnot, 2,3), GatePosition(id, 4)]);\njulia> sublayers = [sub1, sub2];\njulia> bl = BranchingLayer(n_bits, sublayers);\n\njulia> simplify!(bl)\n\njulia> bl.sublayers[1]\nSubLayer(4, GatePosition[GatePosition(1 bit(s) identity gate, 1, 1), GatePosition(1 bit(s) identity gate, 2, 2), \nGatePosition(2 bit(s) cnot gate, 3, 4), GatePosition(2 bit(s) cnot gate, 3, 4)])\n\njulia> bl.sublayers[2]\nSubLayer(4, GatePosition[GatePosition(1 bit(s) identity gate, 1, 1), GatePosition(2 bit(s) cnot gate, 2, 3), \nGatePosition(2 bit(s) cnot gate, 2, 3), GatePosition(1 bit(s) identity gate, 4, 4)])\n\n\n\n"
},

{
    "location": "functionalities/branching_layers.html#BMeraCodes.simplify_and_propagate!-Tuple{BMeraCodes.BranchingLayer,Int64,Int64}",
    "page": "branching_layers.jl",
    "title": "BMeraCodes.simplify_and_propagate!",
    "category": "method",
    "text": "simplify_and_propagate!(bl::BranchingLayer, first_bit::Int, last_bit::Int)\n\nSimplify and propagate through the branching layer starting from first_bit up to last_bit.\n\n\n\n"
},

{
    "location": "functionalities/branching_layers.html#BMeraCodes.simplify_and_propagate!-Tuple{BMeraCodes.BranchingLayer,Int64}",
    "page": "branching_layers.jl",
    "title": "BMeraCodes.simplify_and_propagate!",
    "category": "method",
    "text": "simplify_and_propagate!(bl::BranchingLayer, position::Int)\n\nSimplify and propagate for one bit.\n\n\n\n"
},

{
    "location": "functionalities/branching_layers.html#BMeraCodes.simplify_and_propagate!-Tuple{BMeraCodes.BranchingLayer}",
    "page": "branching_layers.jl",
    "title": "BMeraCodes.simplify_and_propagate!",
    "category": "method",
    "text": "simplify_and_propagate!(bl::BranchingLayer)\n\nSimplify and propagate for the whole branching layer. \n\nExamples\n\nSee the examples for propagate and simplify!.\n\n\n\n"
},

{
    "location": "functionalities/branching_layers.html#Propagate-and-simplify-1",
    "page": "branching_layers.jl",
    "title": "Propagate and simplify",
    "category": "section",
    "text": "propagate(bl::BranchingLayer, first_bit::Int, last_bit::Int)\npropagate(bl::BranchingLayer, position::Int)\npropagate(bl::BranchingLayer)\nsimplify!(bl::BranchingLayer, first_bit::Int, last_bit::Int)\nsimplify!(bl::BranchingLayer, position::Int)\nsimplify!(bl::BranchingLayer)\nsimplify_and_propagate!(bl::BranchingLayer, first_bit::Int, last_bit::Int)\nsimplify_and_propagate!(bl::BranchingLayer, position::Int)\nsimplify_and_propagate!(bl::BranchingLayer)"
},

{
    "location": "functionalities/branching_layers.html#Utilitary-1",
    "page": "branching_layers.jl",
    "title": "Utilitary",
    "category": "section",
    "text": "length(bl::BranchingLayer)"
},

{
    "location": "functionalities/layers.html#",
    "page": "layers.jl",
    "title": "layers.jl",
    "category": "page",
    "text": ""
},

{
    "location": "functionalities/layers.html#layers.jl-1",
    "page": "layers.jl",
    "title": "layers.jl",
    "category": "section",
    "text": ""
},

{
    "location": "functionalities/layers.html#BMeraCodes.Layer",
    "page": "layers.jl",
    "title": "BMeraCodes.Layer",
    "category": "type",
    "text": "Layer <: Any\n\nMutable type.\n\nFields\n\nn_bits::Int\nbranches_length::Int\nbranching_layers::Vector{BranchingLayer}\n\nA vector of branching_layer with each branching_layer having a length branches_length.  A valid Layer must have all branching layers with the same length. There are two ways to initialize a layer:\n\nLayer(branching_layers::Vector{BranchingLayer})\nLayer(branching_layer::BranchingLayer, n_branching::Int)\n\nwhere n_branching is the number of BranchingLayer within branching_layers.\n\nExamples\n\njulia> id, cnot = elementary_gates[\"id_1\"], elementary_gates[\"cnot\"];\njulia> bl = BranchingLayer([SubLayer(8, [GatePosition(cnot, 2*j - 1, 2*j) for j in 1:4])]);\njulia> layer = Layer(bl, 2);\n\njulia> layer.n_bits\n16\n\njulia> layer.branches_length\n8\n\n\n\n"
},

{
    "location": "functionalities/layers.html#Layer-1",
    "page": "layers.jl",
    "title": "Layer",
    "category": "section",
    "text": "Layer"
},

{
    "location": "functionalities/layers.html#General-methods-1",
    "page": "layers.jl",
    "title": "General methods",
    "category": "section",
    "text": ""
},

{
    "location": "functionalities/layers.html#Utilitary-1",
    "page": "layers.jl",
    "title": "Utilitary",
    "category": "section",
    "text": "get_state(layer::Layer)\nlength(layer::Layer)"
},

{
    "location": "functionalities/layers.html#BMeraCodes.set_state!-Tuple{BMeraCodes.Layer,AbstractArray{BMeraCodes.Bit,N} where N}",
    "page": "layers.jl",
    "title": "BMeraCodes.set_state!",
    "category": "method",
    "text": "set_state!(layer::Layer, bs::BitState)\n\nSet a BitState on the top of a Layer. The effect of this function is to update the value of state for each BranchingLayer and to initialize a cone. Also, important to note that the simplification and propagation are not apply automatically.\n\n\n\n"
},

{
    "location": "functionalities/layers.html#Set-state-1",
    "page": "layers.jl",
    "title": "Set state",
    "category": "section",
    "text": " set_state!(layer::Layer, bs::BitState)"
},

{
    "location": "functionalities/layers.html#BMeraCodes.propagate-Tuple{BMeraCodes.Layer,Int64,Int64}",
    "page": "layers.jl",
    "title": "BMeraCodes.propagate",
    "category": "method",
    "text": "propagate(layer::Layer, first_bit::Int, last_bit::Int)\n\nPropagate the BitState of the layer starting from first_bit up to last_bit and return the output state  with the first bit and last bit positions.\n\n\n\n"
},

{
    "location": "functionalities/layers.html#BMeraCodes.propagate-Tuple{BMeraCodes.Layer,Int64}",
    "page": "layers.jl",
    "title": "BMeraCodes.propagate",
    "category": "method",
    "text": "propagate(layer::Layer, position::Int)\n\nPropagate for one bit position.\n\n\n\n"
},

{
    "location": "functionalities/layers.html#BMeraCodes.propagate-Tuple{BMeraCodes.Layer}",
    "page": "layers.jl",
    "title": "BMeraCodes.propagate",
    "category": "method",
    "text": "propagate(layer::Layer)\n\nPropagate for the whole layer.\n\nFor more information see propagate(bl::BranchingLayer, first_bit::Int, last_bit::Int) simply because the function propagate in the case of a layer acts in the same way as for a branching_layer.\n\n\n\n"
},

{
    "location": "functionalities/layers.html#Propagate-1",
    "page": "layers.jl",
    "title": "Propagate",
    "category": "section",
    "text": "propagate(layer::Layer, first_bit::Int, last_bit::Int)\npropagate(layer::Layer, position::Int)\npropagate(layer::Layer)"
},

{
    "location": "functionalities/circuit.html#",
    "page": "circuit.jl",
    "title": "circuit.jl",
    "category": "page",
    "text": ""
},

{
    "location": "functionalities/circuit.html#BMeraCodes.Circuit",
    "page": "circuit.jl",
    "title": "BMeraCodes.Circuit",
    "category": "type",
    "text": "Circuit <: Any\n\nMutable type.\n\nFields\n\nn_bits::Int\nn_layers::Int\nbranching::Int\nbottom_state::ProbState\nlayers::Vector{Layer}\n\nA structure that contain a Vector of Layer that represent the whole circuit. branching is needed to compute all permutation between layers. bottom_state is the ProbState to decode. The constructor is \n\nCircuit(branching::Int, bottom_state::ProbState, layers::Vector{Layer})\n\nExample\n\njulia> cnot = elementary_gates[\"cnot\"];\njulia> # Create the branching layers\njulia> bl1 = BranchingLayer([SubLayer(4, [GatePosition(cnot, 1, 2), GatePosition(cnot, 3, 4)])]);\njulia> bl2 = BranchingLayer([SubLayer(2, [GatePosition(cnot, 1, 2)])]);\njulia> # Init the circuit\njulia> Circuit(2, ZeroProbState([0.1, 0.1, 0.9, 0.1]), [Layer(bl1, 1), Layer(bl2, 2)])\nNumber of bits : 4\nNumber of layers : 2\nBranching : 2\nBottom state : BMeraCodes.ZeroProbState([0.1, 0.1, 0.9, 0.1], 4)\n\n\n\n"
},

{
    "location": "functionalities/circuit.html#circuit.jl-1",
    "page": "circuit.jl",
    "title": "circuit.jl",
    "category": "section",
    "text": "Circuit"
},

{
    "location": "functionalities/circuit.html#BMeraCodes.top_cone",
    "page": "circuit.jl",
    "title": "BMeraCodes.top_cone",
    "category": "function",
    "text": "top_cone(circ::Circuit)\n\nReturn the CausalCone of the first layer of circ if define.\n\n\n\n"
},

{
    "location": "functionalities/circuit.html#BMeraCodes.get_state",
    "page": "circuit.jl",
    "title": "BMeraCodes.get_state",
    "category": "function",
    "text": "get_state(circ::Circuit, layer::Int = 1)\n\nReturn the state of circ.layers[layer] if define.\n\n\n\n"
},

{
    "location": "functionalities/circuit.html#BMeraCodes.change_state!-Tuple{BMeraCodes.Circuit,AbstractArray{BMeraCodes.Bit,N} where N,Int64,Int64}",
    "page": "circuit.jl",
    "title": "BMeraCodes.change_state!",
    "category": "method",
    "text": "change_state!(circ::Circuit, state::BitState, first_bit::Int, last_bit::Int)\n\nChange the state of the first layer of circ between first_bit and last_bit to state. The overall state must still have at least one open leg and all open_legs must be adjacent.\n\n\n\n"
},

{
    "location": "functionalities/circuit.html#BMeraCodes.decode!",
    "page": "circuit.jl",
    "title": "BMeraCodes.decode!",
    "category": "function",
    "text": "decode!(circ::Circuit, width::Int = 1, frozen_bits::Vector{Int} = [])\n\nDecode circ with width width and a set of frozen_bits. Return the decoded state.\n\n\n\n"
},

{
    "location": "functionalities/circuit.html#Functions-and-methods-1",
    "page": "circuit.jl",
    "title": "Functions and methods",
    "category": "section",
    "text": "top_cone\nget_state\nchange_state!(circ::Circuit, state::BitState, first_bit::Int, last_bit::Int)\ndecode!"
},

{
    "location": "functionalities/frozen_bits.html#",
    "page": "frozen_bits.jl",
    "title": "frozen_bits.jl",
    "category": "page",
    "text": ""
},

{
    "location": "functionalities/frozen_bits.html#frozen_bits.jl-1",
    "page": "frozen_bits.jl",
    "title": "frozen_bits.jl",
    "category": "section",
    "text": ""
},

{
    "location": "functionalities/frozen_bits.html#BMeraCodes.frozen_from_bhatta",
    "page": "frozen_bits.jl",
    "title": "BMeraCodes.frozen_from_bhatta",
    "category": "function",
    "text": "frozen_from_bhatta(n_bits::Int, n_frozen::Int, prob::Float64)\n\nReturn a Vector{Int} containing all the frozen bits position using bhattacharyya bounds for the bit flip channel.\n\n\n\n"
},

{
    "location": "functionalities/frozen_bits.html#BMeraCodes.frozen_from_random_output",
    "page": "frozen_bits.jl",
    "title": "BMeraCodes.frozen_from_random_output",
    "category": "function",
    "text": "frozen_from_random_output(circ::Circuit, n_frozen::Int, prob::Float64, n_reps::Int)\n\nReturn a Vector{Int} containing all the frozen bits position using an average over randomly flip outputs.\n\n\n\n"
},

{
    "location": "functionalities/frozen_bits.html#BMeraCodes.frozen_from_mean_output",
    "page": "frozen_bits.jl",
    "title": "BMeraCodes.frozen_from_mean_output",
    "category": "function",
    "text": "frozen_from_mean_ouput(circ::Circuit, n_frozen::Int, prob::Float64)\n\nReturn a Vector{Int} containing all the frozen bits position where all  outputs are given by (1 - 2 * p * (1 - p), 2 * p * (1 - p)).\n\n\n\n"
},

{
    "location": "functionalities/frozen_bits.html#Algorithms-1",
    "page": "frozen_bits.jl",
    "title": "Algorithms",
    "category": "section",
    "text": "frozen_from_bhatta\nfrozen_from_random_output\nfrozen_from_mean_output"
},

{
    "location": "functionalities/frozen_bits.html#BMeraCodes.hamming_distance",
    "page": "frozen_bits.jl",
    "title": "BMeraCodes.hamming_distance",
    "category": "function",
    "text": "hamming_distance(n_bits::Int, frozen1::Vector{Int}, frozen2::Vector{Int})\n\nReturn the distance between two vectors of frozen bits position.\n\n\n\n"
},

{
    "location": "functionalities/frozen_bits.html#BMeraCodes.prob_first_error",
    "page": "frozen_bits.jl",
    "title": "BMeraCodes.prob_first_error",
    "category": "function",
    "text": "prob_first_error(circ::Circuit)\n\nReturn a Vector{Float64} where element i is the probability that the bit i  is the first to be decoded wrongly with a width of 1.\n\n\n\n"
},

{
    "location": "functionalities/frozen_bits.html#Utilitaries-1",
    "page": "frozen_bits.jl",
    "title": "Utilitaries",
    "category": "section",
    "text": "hamming_distance\nprob_first_error"
},

{
    "location": "Examples/polar_codes.html#",
    "page": "Polar codes",
    "title": "Polar codes",
    "category": "page",
    "text": ""
},

{
    "location": "Examples/polar_codes.html#Polar-codes-1",
    "page": "Polar codes",
    "title": "Polar codes",
    "category": "section",
    "text": "Here is a code that create a four layers polar code with a probability of flip of 0.1.# Include the module\ninclude(\"../Sources/bmera_codes.jl\")\nusing BMeraCodes\n\n# Parameters\nn_layers = 4\nprob_of_flip = 0.1\n\n# Import cnot gate\ncnot = elementary_gates[\"cnot\"]\n\n# Create every layers in the circuit\nfunction init_layer(layer::Int, n_layers::Int)\n    branch_length = 2^(n_layers-layer+1)\n    gates = [GatePosition(cnot,2*i-1,2*i) for i in 1:div(branch_length,2)]\n    sl = SubLayer(branch_length, gates)\n    bl = BranchingLayer([sl])\n    Layer(bl, 2^(layer-1))\nend\n\nlayers = [init_layer(i, n_layers) for i in 1:n_layers]\n\n# For every bit, the probability of begin 0 is 1 - prob_of_flip\nnoisy_zeros = ZeroProbState((1 - prob_of_flip) * ones(2^n_layers))\n\n# Create the circuit and decode.\ncirc = Circuit(2, noisy_zeros, layers)\ndecode!(circ, 1)The ouput of that code must be 16-element Array{BMeraCodes.Bit,1}:\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0"
},

{
    "location": "Examples/conv_polar_codes.html#",
    "page": "Convolutional polar codes",
    "title": "Convolutional polar codes",
    "category": "page",
    "text": ""
},

{
    "location": "Examples/conv_polar_codes.html#Convolutional-polar-codes-1",
    "page": "Convolutional polar codes",
    "title": "Convolutional polar codes",
    "category": "section",
    "text": "Here is a code that create a four layers convolutional polar code with a  probability of flip of 0.1.# Include the module\ninclude(\"../Sources/bmera_codes.jl\")\nusing BMeraCodes\n\n# Parameters\nn_layers = 4\nprob_of_flip = 0.1\n\n# Import gates\ncnot = elementary_gates[\"cnot\"]\nid = elementary_gates[\"id_1\"]\n\n# Create every layers in the circuit\nfunction init_layer(level::Int, n_levels::Int)\n    branch_length = 2^(n_levels-level+1)\n    gates1 = [GatePosition(cnot,2*i-1,2*i) for i in 1:div(branch_length,2)]\n    sl1 = SubLayer(branch_length, gates1)\n    gates2 = vcat(GatePosition(id, 1), [GatePosition(cnot,2*i,2*i+1) for i in 1:(div(branch_length,2)-1)], GatePosition(id, branch_length))\n    sl2 = SubLayer(branch_length, gates2)\n    bl = BranchingLayer([sl2, sl1])\n    Layer(bl, 2^(level-1))\nend\n\nlayers = [init_layer(i, n_layers) for i in 1:n_layers]\n\n# For every bit, the probability of begin 0 is 1 - prob_of_flip\nnoisy_zeros = ZeroProbState((1 - prob_of_flip) * ones(2^n_layers))\n\n# Create the circuit and decode.\ncirc = Circuit(2, noisy_zeros, layers)\ndecode!(circ, 1)The ouput of that code must be 16-element Array{BMeraCodes.Bit,1}:\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0\n bit_0"
},

]}
