using Documenter
include("../Sources/bmera_codes.jl")
using BMeraCodes

makedocs(
    format = :html,
    sitename = "BMeraCodes.jl",
    pages = [
        "index.md",
        "Functionalities" => Any[
            "functionalities/bit.md",
            "functionalities/prob_states.md",
            "functionalities/transformations.md",
            "functionalities/contractions.md",
            "functionalities/causal_cones.md",
            "functionalities/gate.md",
            "functionalities/gate_position.md",
            "functionalities/sublayers.md",
            "functionalities/branching_layers.md",
            "functionalities/layers.md",
            "functionalities/circuit.md",
            "functionalities/frozen_bits.md"
        ],
        "Examples" => Any[
            "Examples/polar_codes.md",
            "Examples/conv_polar_codes.md"
        ]
    ]
)

deploydocs(
    repo   = "gitlab.com/benjy765/bMERA_codes.git",
    target = "build",
    deps   = nothing,
    make   = nothing
)