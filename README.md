# BMera codes
<img src="Documentation/logo.png"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 10px;"
     height="150" 
     width="150"/>             
     
[![pipeline status](https://gitlab.com/benjy765/bMERA_codes/badges/master/pipeline.svg)](https://gitlab.com/benjy765/bMERA_codes/commits/master)


This is a simulation tools for [BMera codes](https://arxiv.org/pdf/1312.4575.pdf) a code family that generalize the well known [polar codes](https://arxiv.org/pdf/0807.3917.pdf).

## Getting Started

To start using the code, you must clone this project on your machine. Then, you need to include the bmera_codes module by running:
```
include("PATH_TO_BMERA/Sources/bmera_codes.jl")
```
After, you can use the module with:
```
using BmeraCodes
```

## Prerequisites

You need to have the 0.6.x version of Julia installed in order to use this module. Otherwise, some modification are needed.

## Running the tests

We provide some tests in the `Tests` directory. You can run all the tests by running the command
```
bash run_all_tests.sh
```
within the terminal. If you want to run only one test, you can use de command:
```
julia test_file.jl
```

## Documentation

We provide a complete documentation useful for the user and the developper. To build the documentation, you must go to the `Documentation` directory and run:
```
julia make.jl
```
This command will create a `build` directory with an `index.html` file that will provide the documentation. For function definition and code examples we refer the this documentation.

## Authors

* **Benjamin Bourassa** 
* **Maxime Tremblay**

## Publications

Using this code we made two publications.
* [Depth versus Breadth in Convolutional Polar Codes](https://arxiv.org/pdf/1805.09306.pdf)
* [Convolutional Polar Codes on Channels with Memory using Tensor Networks](https://arxiv.org/pdf/1805.09378.pdf)


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
