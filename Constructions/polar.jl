include("../Sources/bmera_codes.jl")
using BMeraCodes

cnot = VectorTransformation([1,2,4,3])
noisy_state = ZeroProbState([0.1, 0.9, 0.1, 0.1, 0.9, 0.9, 0.9, 0.1])

mutable struct PolarCircuit
    n_bits::Int
    n_layers::Int
    branching::Int

    gate::Transformation
    noisy_state::ProbState

    cone_positions::Vector{Int}

    state::Vector{Vector{BitState}}
    contractions::Vector{Vector{ProbState}}

    function PolarCircuit(n_layers::Int, branching::Int, gate::Transformation, noisy_state::ProbState)
        n_bits = branching^n_layers
        if length(noisy_state) != n_bits 
            error("Number of bits in noisy state must be branching^n_layers.")
        end
        cone_positions = [branching^l for l in n_layers:-1:1]
        circ = new(n_bits, n_layers, branching, gate, noisy_state, cone_positions)
        init_circuit_state!(circ)
        init_contraction!(circ)
        return circ
    end
end

# Init PolarCircuit

function init_layer_state(layer::Int, n_layers::Int, branching::Int)
    state = fill(bit_e, branching^(n_layers - layer + 1))
    state[end] = bit__
    return fill(state, branching^(layer - 1))
end

function init_circuit_state!(circ::PolarCircuit)
    circ.state = [init_layer_state(l, circ.n_layers, circ.branching) for l in 1:circ.n_layers]
end

function init_contraction!(circ::PolarCircuit)
    circ.contractions = [Vector{ProbState}(circ.branching^l) for l in 0:(circ.n_layers-1)]
    
    span(i::Int) = (circ.branching * (i - 1) + 1):(circ.branching * i)

    for (i, state) in enumerate(circ.state[end])
        circ.contractions[end][i] = circ.noisy_state[span(i)] * circ.gate * state
    end

    for (i, state) in enumerate(circ.state[end-1:-1:1])
        contractions = Vector{ProbState}(length(state))
        for (j, s) in enumerate(state)
            bottom_state = kron(circ.contractions[end-i+1][span(j)]...)
            contractions[j] = bottom_state * circ.gate * s[end-circ.branching+1:end]
        end
        circ.contractions[end-i] = contractions
    end
end




