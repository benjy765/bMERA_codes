include("../Sources/bmera_codes.jl")
using BMeraCodes
using Base.Test

@testset "SubLayers .." begin
    id = elementary_gates["id_1"]
    not = elementary_gates["not"]
    cnot = elementary_gates["cnot"]

    @testset ".. constructor" begin
        gate = GatePosition(cnot, 1, 2)
        @test SubLayer(2, [gate]) == SubLayer(2, [gate, gate])
        @test_throws ErrorException SubLayer(2, [gate, GatePosition(id, 1)]) # Gate overlap.
        @test_throws ErrorException SubLayer(3, [gate]) # Bit 3 as no gate.
        @test_throws ErrorException SubLayer(3, [GatePosition(cnot, 1, 3), GatePosition(id, 2)])
    end

    @testset ".. getindex" begin
        sl = SubLayer(2, [GatePosition(cnot, 1, 2)])
        @test sl[1] == GatePosition(cnot, 1, 2)
        @test sl[2] == GatePosition(cnot, 1, 2)        
    end

    @testset ".. propagate" begin
        sl = SubLayer(5, [GatePosition(cnot, 1, 2), GatePosition(cnot, 3, 4), GatePosition(not, 5)])
        
        @test propagate(sl, BitState("eee__"), 4) == (BitState("__"), [3,4])
        @test propagate(sl, BitState("e_011"), 4) == (BitState("01"), [3,4])
        @test propagate(sl, BitState("e_110"), 4) == (BitState("10"), [3,4])
        @test propagate(sl, BitState("e_010"), 1, 2) == (BitState("__"), [1,2])
        @test propagate(sl, BitState("eee_0"), 5) == (BitState("1"), [5,5])
        @test propagate(sl, BitState("eee_0")) == (BitState("ee__1"), [1,5])

        @test_throws ErrorException propagate(sl, BitState("01"), 1)     # Dimension incompatible
        @test_throws ErrorException propagate(sl, BitState("e_01"), 2, 5) # Index out of range
        @test_throws ErrorException propagate(sl, BitState("e_01"), -1, 3) # Negative index
    end

    @testset ".. simplify!" begin
        sl = SubLayer(5, [GatePosition(cnot, 1, 2), GatePosition(cnot, 3, 4), GatePosition(not, 5)])
        
        simplify!(sl, BitState("eeee_"), 5)
        @test  sl.gates == [GatePosition(cnot, 1, 2), GatePosition(cnot, 1, 2), GatePosition(cnot, 3, 4), GatePosition(cnot, 3, 4), GatePosition(not, 5)]

        simplify!(sl, BitState("eee_0"), 4)
        @test  sl.gates == [GatePosition(cnot, 1, 2), GatePosition(cnot, 1, 2), GatePosition(cnot, 3, 4), GatePosition(cnot, 3, 4), GatePosition(not, 5)]

        simplify!(sl, BitState("e_100"), 3)
        @test  sl.gates == [GatePosition(cnot, 1, 2), GatePosition(cnot, 1, 2), GatePosition(id, 3), GatePosition(not, 4), GatePosition(not, 5)]

        # Reset
        sl = SubLayer(5, [GatePosition(cnot, 1, 2), GatePosition(cnot, 3, 4), GatePosition(not, 5)])
        
        simplify!(sl, BitState("e_100"))
        @test  sl.gates == [GatePosition(cnot, 1, 2), GatePosition(cnot, 1, 2), GatePosition(id, 3), GatePosition(not, 4), GatePosition(not, 5)]

        @test_throws ErrorException simplify!(sl, BitState("01"), 1) # Dimension incompatible
        @test_throws ErrorException simplify!(sl, BitState("e_010"), 2, 6) # Index out of range
        @test_throws ErrorException simplify!(sl, BitState("ee_01"), -1, 3) # Negative index
    end

    @testset ".. get_transform" begin
        sl = SubLayer(5, [GatePosition(cnot, 1, 2), GatePosition(cnot, 3, 4), GatePosition(not, 5)])

        @test get_transform(sl, 1, 2) == (cnot.transform, [1, 2])
        @test get_transform(sl, 1, 3) == (kron(cnot.transform, cnot.transform), [1, 4])
        @test get_transform(sl, 2, 3) == (kron(cnot.transform, cnot.transform), [1, 4])
        @test get_transform(sl, 1, 1) == (cnot.transform, [1, 2])
        @test get_transform(sl, 5, 5) == (not.transform, [5, 5])

        @test_throws ErrorException get_transform(sl, 2, 1) # Wrong indices order 
    end

    @testset ".. +" begin
        sl1 = SubLayer(3, [GatePosition(cnot, 1, 2), GatePosition(id, 3)])
        sl2 = SubLayer(2, [GatePosition(cnot, 1, 2)])
        @test sl1 + sl2 == SubLayer(5, [GatePosition(cnot, 1, 2), GatePosition(id, 3), GatePosition(cnot, 4, 5)])
        @test sum([sl2, sl2, sl2]) == SubLayer(6, [GatePosition(cnot, 1, 2), GatePosition(cnot, 3, 4), GatePosition(cnot, 5, 6)])
    end
end