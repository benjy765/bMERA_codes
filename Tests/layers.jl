include("../Sources/bmera_codes.jl")
using BMeraCodes
using Base.Test

@testset "Layer" begin
    id = elementary_gates["id_1"]
    cnot = elementary_gates["cnot"]
    bl = BranchingLayer([SubLayer(8, [GatePosition(cnot, 2*j - 1, 2*j) for j in 1:4])])

    @testset ".. constructor" begin
        bl2 = BranchingLayer([SubLayer(10, [GatePosition(cnot, 2*j - 1, 2*j) for j in 1:5])])

        @test_throws ErrorException Layer([bl, bl2])    # Branching layers must have the same length

        layer = Layer(bl, 2)

        @test layer.n_bits == 16
        @test layer.branches_length == 8
    end

    @testset ".. set state" begin
        layer = Layer(bl, 1)

        @test_throws ErrorException set_state!(layer, BitState("eee_"))     # Number of bits not equal to dim of layer
        @test_throws ErrorException set_state!(layer, BitState("eeeeeeee")) # Nothing to decode
        @test_throws ErrorException set_state!(layer, BitState("eee_000_")) # bit_ not continuous

        set_state!(layer, BitState("eeeeeee_"))
        branchlayer = layer.branching_layers[1]
        @test branchlayer.state == BitState("eeeeeee_")

        # Case where we have multiple branching layers
        layer2 = Layer(bl, 2)

        @test_throws ErrorException set_state!(layer2, vcat(fill(bit_e, 15), bit__)) # One of the branching layer don't have a bit to decode

        set_state!(layer2, vcat(fill(bit_e, 7), bit__, fill(bit_e, 7), bit__))        
        bl1 = layer2.branching_layers[1]
        bl2 = layer2.branching_layers[2]

        @test bl1.state == vcat(fill(bit_e, 7), bit__)
        @test bl2.state == vcat(fill(bit_e, 7), bit__)

        layer3 = Layer(bl, 2)

        set_state!(layer3, vcat(fill(bit_e, 6), bit__, bit_1, fill(bit_e, 6), bit__, bit_0))
        bl1 = layer3.branching_layers[1]
        bl2 = layer3.branching_layers[2]

        @test bl1.state == vcat(fill(bit_e, 6), bit__, bit_1)
        @test bl2.state == vcat(fill(bit_e, 6), bit__, bit_0)        
    end

    @testset ".. propagate" begin
        layer = Layer(bl, 2)
        set_state!(layer::Layer, vcat(fill(BitState("eeee_110"), 2)...))
        
        @test propagate(layer) == (vcat(fill(BitState("eeee__11"), 2)...), [1,16])
        @test propagate(layer, 5) == (BitState("__"), [5, 6])
        @test propagate(layer, 5,7) == (BitState("__11"), [5, 8])
        @test propagate(layer, 5,13) == (BitState("__11eeee__"), [5, 14])

        bl = BranchingLayer([SubLayer(2,[GatePosition(cnot, 1, 2)])])
        layer = Layer(bl, 8)
        set_state!(layer::Layer, vcat([BitState("e_") for i in 1:8]...))

        @test propagate(layer) == (fill(bit__, 16), [1,16])
        @test propagate(layer, 3) == (BitState("__"), [3,4])
        @test propagate(layer, 5,6) == (BitState("__"), [5,6])
        @test propagate(layer, 3,5) == (BitState("____"), [3,6])
        @test propagate(layer, 3,12) == (fill(bit__,10), [3,12])
    end

    @testset ".. get_transform" begin
        sub1 = SubLayer(4, [GatePosition(id, 1), GatePosition(cnot, 2, 3), GatePosition(id, 4)])
        sub2 = SubLayer(4, [GatePosition(cnot, 1, 2), GatePosition(cnot, 3, 4)])
        bl_1 = BranchingLayer(4, [sub1, sub2])
        bl_2 = BranchingLayer(4, [sub2, sub1])
        layer = Layer([bl_1, bl_2])

        @test get_transform(layer) == kron(get_transform(bl_1), get_transform(bl_2))
    end

    @testset ".. depth" begin
        layer = Layer(bl, 2)
        @test depth(layer) == 1

        sub1 = SubLayer(4, [GatePosition(id, 1), GatePosition(cnot, 2, 3), GatePosition(id, 4)])
        sub2 = SubLayer(4, [GatePosition(cnot, 1, 2), GatePosition(cnot, 3, 4)])
        bl_1 = BranchingLayer(4, [sub1, sub2])
        bl_2 = BranchingLayer(4, [sub1])
        layer = Layer([bl_1, bl_2])
        @test depth(layer) == 2
    end
end
