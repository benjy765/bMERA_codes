include("../Sources/bmera_codes.jl")
using BMeraCodes
using Base.Test
#=
@testset "Frozen bits" begin
    @testset "Frozen from erasure" begin

        pc = polar_code(4, elementary_gates["cnot"])
        @test frozen_from_erasure(pc, 6, 0.1) == [16,15,14,12,8,13]

        srand(42)
        @test frozen_from_erasure(pc, 6, 0.1, 1) == [16,15,14,12,8,13]

        srand(42)
        @test frozen_from_erasure(pc, 6, 0.1, 2) == [16,15,14,12,8,13]
    end
end
=#