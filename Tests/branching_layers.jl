include("../Sources/bmera_codes.jl")
using BMeraCodes
using Base.Test

id = elementary_gates["id_1"]
not = elementary_gates["not"]
cnot = elementary_gates["cnot"]

sub1 = SubLayer(8, [GatePosition(cnot, 2*i - 1, 2*i) for i in 1:4])
sub2 = SubLayer(8, vcat(GatePosition(id, 1), [GatePosition(cnot, 2*i, 2*i + 1) for i in 1:3], GatePosition(id, 8)))

@testset "Branching Layer" begin
    @testset ".. constructor" begin
        bl = BranchingLayer(8, [sub1, sub2])

        @test bl.n_bits == 8
        @test bl.state == BitState("eeeeeee_")
        @test bl.cone.top_bit_state == BitState("ee_")
        @test bl.cone.transform == VectorTransformation([1, 2, 4, 3, 8, 7, 5, 6])

        bl = BranchingLayer(6, [sub1, sub2])

        @test bl.n_bits == 8
        @test bl.state == BitState("eeeee___")
        @test bl.cone.top_bit_state == BitState("ee___")
        @test bl.cone.transform == kron(cnot.transform, cnot.transform, id.transform) * kron(id.transform, cnot.transform, cnot.transform)
    
        sub3 = SubLayer(1, [GatePosition(id, 1)])

        @test_throws ErrorException BranchingLayer(8, [sub1, sub3])
        @test_throws ErrorException BranchingLayer(12, [sub1, sub2])
    end

    @testset ".. Set State" begin
        bl = BranchingLayer([sub1, sub2])
        @test_throws ErrorException set_state!(bl, BitState("ee__")) # Wrong state length.
        @test_throws ErrorException set_state!(bl, BitState("eeee0101")) # No open leg.
        @test_throws ErrorException set_state!(bl, BitState("ee_e_101")) # Non continuous open leg.

        set_state!(bl, BitState("eeee__01"))
        @test bl.n_bits == 8
        @test bl.state == BitState("eeee__01")
        @test bl.cone.top_bit_state == BitState("e__0")
        @test bl.cone.transform == VectorTransformation([1, 2, 4, 3, 7, 8, 6, 5, 15, 16, 14, 13, 9, 10, 12, 11])
    end

    @testset ".. change state" begin
        bl = BranchingLayer([sub1, sub2])
        set_state!(bl, BitState("eeee__01"))

        @test_throws ErrorException change_state!(bl, BitState("10"), 2, 1) # first and last bit invalid
        @test_throws ErrorException change_state!(bl, BitState("10"), 1, 3) # first and last bit invalid
        @test_throws ErrorException change_state!(bl, BitState("ee"), 5, 6) # There must be at least 1 bit_
        @test_throws ErrorException change_state!(bl, BitState("_"), 8)     # Open legs must be adjacent

        change_state!(bl, BitState("e__1"), 4, 7)
        @test bl.n_bits == 8
        @test bl.state == BitState("eeee__11")
        @test bl.cone.top_bit_state == BitState("e__1")
        @test bl.cone.transform == VectorTransformation([1, 2, 4, 3, 7, 8, 6, 5, 15, 16, 14, 13, 9, 10, 12, 11])            

        change_state!(bl, BitState("eee_"), 5, 8)
        @test bl.n_bits == 8
        @test bl.state == BitState("eeeeeee_")
        @test bl.cone.top_bit_state == BitState("ee_")
        @test bl.cone.transform == VectorTransformation([1, 2, 4, 3, 8, 7, 5, 6])            
    end

    @testset ".. propagate" begin
        bl = BranchingLayer(8, [sub1, sub2])

        @test propagate(bl, 8) == (BitState("___"), [6, 8])

        bl = BranchingLayer(6, [sub1, sub2])

        @test propagate(bl, 6, 8) == (BitState("_____"), [4, 8])
        @test propagate(bl) == (BitState("eee_____"), [1, 8]) 
    end

    @testset ".. simplify" begin
        bl = BranchingLayer(8, [sub1, sub2])

        simplify!(bl, 1, 8)
        @test bl.sublayers[1] == SubLayer(8, vcat([GatePosition(id, i) for i in 1:6], GatePosition(cnot, 7, 8)))
        @test bl.sublayers[2] == SubLayer(8, vcat([GatePosition(id, i) for i in 1:5], GatePosition(cnot, 6, 7), GatePosition(id, 8)))

        # RESET

        sub1 = SubLayer(8, [GatePosition(cnot, 2*i - 1, 2*i) for i in 1:4])
        sub2 = SubLayer(8, vcat(GatePosition(id, 1), [GatePosition(cnot, 2*i, 2*i + 1) for i in 1:3], GatePosition(id, 8)))
        bl = BranchingLayer(6, [sub1, sub2])

        simplify!(bl, 1, 8)
        @test bl.sublayers[1] == SubLayer(8, vcat([GatePosition(id, i) for i in 1:4], [GatePosition(cnot, 2*i-1, 2*i) for i in 3:4]))
        @test bl.sublayers[2] == SubLayer(8, vcat([GatePosition(id, i) for i in 1:3], [GatePosition(cnot, 2*i, 2*i+1) for i in 2:3], GatePosition(id, 8))) 

        # RESET

        sub1 = SubLayer(8, [GatePosition(cnot, 2*i - 1, 2*i) for i in 1:4])
        sub2 = SubLayer(8, vcat(GatePosition(id, 1), [GatePosition(cnot, 2*i, 2*i + 1) for i in 1:3], GatePosition(id, 8)))
        bl = BranchingLayer(8, [sub1, sub2])

        simplify!(bl, 8)
        @test bl.sublayers[1] == SubLayer(8, [GatePosition(cnot, 2*i - 1, 2*i) for i in 1:4])
        @test bl.sublayers[2] == SubLayer(8, vcat(GatePosition(id, 1), [GatePosition(cnot, 2*i, 2*i + 1) for i in 1:3], GatePosition(id, 8)))

    end

    @testset ".. simplify and propagate" begin
        sub1 = SubLayer(8, [GatePosition(cnot, 2*i - 1, 2*i) for i in 1:4])
        sub2 = SubLayer(8, vcat(GatePosition(id, 1), [GatePosition(cnot, 2*i, 2*i + 1) for i in 1:3], GatePosition(id, 8)))
        bl = BranchingLayer(8, [sub1, sub2])

        @test simplify_and_propagate!(bl, 8) == (BitState("___"), [6, 8])
        @test bl.sublayers[1] == SubLayer(8, [GatePosition(cnot, 2*i - 1, 2*i) for i in 1:4])
        @test bl.sublayers[2] == SubLayer(8, vcat(GatePosition(id, 1), [GatePosition(cnot, 2*i, 2*i + 1) for i in 1:3], GatePosition(id, 8)))
    end

    @testset ".. get_transform" begin
        sub1 = SubLayer(4, [GatePosition(id, 1), GatePosition(cnot, 2, 3), GatePosition(id, 4)])
        sub2 = SubLayer(4, [GatePosition(cnot, 1, 2), GatePosition(cnot, 3, 4)])
        bl = BranchingLayer(4, [sub1, sub2])

        cn_t = cnot.transform
        id_t = id.transform
        @test get_transform(bl) == kron(cn_t, cn_t) * kron(id_t, cn_t, id_t)
    end

end
