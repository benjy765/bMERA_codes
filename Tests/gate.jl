include("../Sources/bmera_codes.jl")
using BMeraCodes
using Base.Test

@testset "Gate" begin
    # With gate set
    ID_1 = elementary_gates["id_1"]
    ID_2 = identity_gate!(elementary_gates, 2)
    ID_3 = identity_gate!(elementary_gates, 3)
    NOT = elementary_gates["not"]
    CNOT = elementary_gates["cnot"]
    TOFFOLI = cnot_gate!(elementary_gates, 3)
    CNOT_1_1 = multi_cnot_gate!(elementary_gates, 1, 1)
    CNOT_3_2 = multi_cnot_gate!(elementary_gates, 3, 2)

    # Without gate set
    id_1 = identity_gate(1)
    id_2 = identity_gate(2)
    id_3 = identity_gate(3)
    not = not_gate()
    cnot = cnot_gate()
    toffoli = cnot_gate(3)
    cnot_1_1 = multi_cnot_gate(1, 1)
    cnot_3_2 = multi_cnot_gate(3, 2)
    swap = swap_gate()

    @testset ".. identity" begin        
        # Test with gate set
        for bs in all_bit_states(1)
            @test ID_1[bs] == (bs, [[1]], [ID_1])
        end

        # Test without gate set
        for bs in all_bit_states(2)
            @test id_2[bs] == (bs, [[1,2]], [id_2])
        end
    end

    @testset ".. not" begin
        # Test with gate set
        @test NOT[BitState("0")] == (BitState("1"), [[1]], [NOT])
        @test NOT[BitState("1")] == (BitState("0"), [[1]], [NOT])
        @test NOT[BitState("e")] == (BitState("e"), [[1]], [NOT])
        @test NOT[BitState("_")] == (BitState("_"), [[1]], [NOT])
       
        # Test for references in gate set
        @test NOT[BitState("0")][3][1] === NOT
        
        # Test without gate set
        @test not[BitState("0")] == (BitState("1"), [[1]], [not])
        @test not[BitState("1")] == (BitState("0"), [[1]], [not])
        @test not[BitState("e")] == (BitState("e"), [[1]], [not])
        @test not[BitState("_")] == (BitState("_"), [[1]], [not])

    end

    @testset ".. 2 bit cnot" begin
        # Test with gate set
        @test CNOT[BitState("00")] == (BitState("00"), [[1], [2]], [ID_1, ID_1])
        @test CNOT[BitState("01")] == (BitState("01"), [[1], [2]], [ID_1, ID_1])
        @test CNOT[BitState("0e")] == (BitState("0e"), [[1], [2]], [ID_1, ID_1])
        @test CNOT[BitState("0_")] == (BitState("0_"), [[1], [2]], [ID_1, ID_1])
        @test CNOT[BitState("10")] == (BitState("11"), [[1], [2]], [ID_1, NOT])
        @test CNOT[BitState("11")] == (BitState("10"), [[1], [2]], [ID_1, NOT])
        @test CNOT[BitState("1e")] == (BitState("1e"), [[1], [2]], [ID_1, ID_1])
        @test CNOT[BitState("1_")] == (BitState("1_"), [[1], [2]], [ID_1, NOT])
        @test CNOT[BitState("ee")] == (BitState("ee"), [[1], [2]], [ID_1, ID_1])
        @test CNOT[BitState("e_")] == (BitState("__"), [[1, 2]], [CNOT])
        @test CNOT[BitState("_0")] == (BitState("__"), [[1, 2]], [CNOT])
        @test CNOT[BitState("_1")] == (BitState("__"), [[1, 2]], [CNOT])
        @test CNOT[BitState("_e")] == (BitState("_e"), [[1], [2]], [ID_1, ID_1])
        @test CNOT[BitState("__")] == (BitState("__"), [[1, 2]], [CNOT])

        @test_throws ErrorException CNOT[BitState("e0")]
        @test_throws ErrorException CNOT[BitState("e1")]

        # Test for references in gate set
        @test CNOT[BitState("00")][3][1] === ID_1
        @test CNOT[BitState("00")][3][2] === ID_1
        @test CNOT[BitState("10")][3][1] === ID_1
        @test CNOT[BitState("10")][3][2] === NOT
        @test CNOT[BitState("e_")][3][1] === CNOT

        # Test without gate set
        @test cnot[BitState("00")] == (BitState("00"), [[1], [2]], [id_1, id_1])
        @test cnot[BitState("01")] == (BitState("01"), [[1], [2]], [id_1, id_1])
        @test cnot[BitState("0e")] == (BitState("0e"), [[1], [2]], [id_1, id_1])
        @test cnot[BitState("0_")] == (BitState("0_"), [[1], [2]], [id_1, id_1])
        @test cnot[BitState("10")] == (BitState("11"), [[1], [2]], [id_1, not])
        @test cnot[BitState("11")] == (BitState("10"), [[1], [2]], [id_1, not])
        @test cnot[BitState("1e")] == (BitState("1e"), [[1], [2]], [id_1, id_1])
        @test cnot[BitState("1_")] == (BitState("1_"), [[1], [2]], [id_1, not])
        @test cnot[BitState("ee")] == (BitState("ee"), [[1], [2]], [id_1, id_1])
        @test cnot[BitState("e_")] == (BitState("__"), [[1, 2]], [cnot])
        @test cnot[BitState("_0")] == (BitState("__"), [[1, 2]], [cnot])
        @test cnot[BitState("_1")] == (BitState("__"), [[1, 2]], [cnot])
        @test cnot[BitState("_e")] == (BitState("_e"), [[1], [2]], [id_1, id_1])
        @test cnot[BitState("__")] == (BitState("__"), [[1, 2]], [cnot])

        @test_throws ErrorException cnot[BitState("e0")]
        @test_throws ErrorException cnot[BitState("e1")]
    end

    @testset ".. 3 bit cnot" begin
        ## Test with gate set

        # Check for 0 in control.
        @test TOFFOLI[BitState("000")] == (BitState("000"), [[1], [2], [3]], [ID_1, ID_1, ID_1])
        @test TOFFOLI[BitState("0_1")] == (BitState("0_1"), [[1], [2], [3]], [ID_1, ID_1, ID_1])
        @test TOFFOLI[BitState("e0_")] == (BitState("e0_"), [[1], [2], [3]], [ID_1, ID_1, ID_1])

        # Check for e in target.
        @test TOFFOLI[BitState("__e")] == (BitState("__e"), [[1], [2], [3]], [ID_1, ID_1, ID_1])
        @test TOFFOLI[BitState("eee")] == (BitState("eee"), [[1], [2], [3]], [ID_1, ID_1, ID_1])
        @test TOFFOLI[BitState("e1e")] == (BitState("e1e"), [[1], [2], [3]], [ID_1, ID_1, ID_1])

        # Check for 1 removal.
        @test TOFFOLI[BitState("_10")] == (BitState("_1_"), [[2], [1, 3]], [ID_1, CNOT])
        @test TOFFOLI[BitState("110")] == (BitState("111"), [[1], [2], [3]], [ID_1, ID_1, NOT])
        @test TOFFOLI[BitState("_11")] == (BitState("_1_"), [[2],[1, 3]], [ID_1, CNOT])

        # Check for open legs.
        @test TOFFOLI[BitState("ee_")] == (BitState("___"), [[1, 2, 3]], [TOFFOLI])
        @test TOFFOLI[BitState("e_0")] == (BitState("e__"), [[1, 2, 3]], [TOFFOLI])
        @test TOFFOLI[BitState("_e1")] == (BitState("_e_"), [[1, 2, 3]], [TOFFOLI])

        # Check for state without simplification.
        @test_throws ErrorException TOFFOLI[BitState("e11")]
        @test_throws ErrorException TOFFOLI[BitState("ee0")]

        ## Test for references in gate set.

        @test TOFFOLI[BitState("000")][3][1] === ID_1
        @test TOFFOLI[BitState("000")][3][2] === ID_1
        @test TOFFOLI[BitState("000")][3][3] === ID_1
        @test TOFFOLI[BitState("_10")][3][1] === ID_1
        @test TOFFOLI[BitState("_10")][3][2] === CNOT
        @test TOFFOLI[BitState("110")][3][1] === ID_1
        @test TOFFOLI[BitState("110")][3][2] === ID_1
        @test TOFFOLI[BitState("110")][3][3] === NOT
        @test TOFFOLI[BitState("ee_")][3][1] === TOFFOLI

        ## Test without gate set.

        # Check for 0 in control.
        @test toffoli[BitState("000")] == (BitState("000"), [[1], [2], [3]], [id_1, id_1, id_1])
        @test toffoli[BitState("0_1")] == (BitState("0_1"), [[1], [2], [3]], [id_1, id_1, id_1])
        @test toffoli[BitState("e0_")] == (BitState("e0_"), [[1], [2], [3]], [id_1, id_1, id_1])

        # Check for e in target.
        @test toffoli[BitState("__e")] == (BitState("__e"), [[1], [2], [3]], [id_1, id_1, id_1])
        @test toffoli[BitState("eee")] == (BitState("eee"), [[1], [2], [3]], [id_1, id_1, id_1])
        @test toffoli[BitState("e1e")] == (BitState("e1e"), [[1], [2], [3]], [id_1, id_1, id_1])

        # Check for 1 removal.
        @test toffoli[BitState("_10")] == (BitState("_1_"), [[2], [1, 3]], [id_1, cnot])
        @test toffoli[BitState("110")] == (BitState("111"), [[1], [2], [3]], [id_1, id_1, not])
        @test toffoli[BitState("_11")] == (BitState("_1_"), [[2],[1, 3]], [id_1, cnot])

        # Check for open legs.
        @test toffoli[BitState("ee_")] == (BitState("___"), [[1, 2, 3]], [toffoli])
        @test toffoli[BitState("e_0")] == (BitState("e__"), [[1, 2, 3]], [toffoli])
        @test toffoli[BitState("_e1")] == (BitState("_e_"), [[1, 2, 3]], [toffoli])

        # Check for state without simplification.
        @test_throws ErrorException toffoli[BitState("e11")]
        @test_throws ErrorException toffoli[BitState("ee0")]
    end

    @testset ".. multi cnot 1 1" begin
        @test CNOT_1_1.transform == VectorTransformation([1,2,4,3])
        @test cnot_1_1.transform == VectorTransformation([1,2,4,3])

        # Test without gate set
        @test CNOT_1_1[BitState("00")] == (BitState("00"), [[1], [2]], [ID_1, ID_1])
        @test CNOT_1_1[BitState("01")] == (BitState("01"), [[1], [2]], [ID_1, ID_1])
        @test CNOT_1_1[BitState("0e")] == (BitState("0e"), [[1], [2]], [ID_1, ID_1])
        @test CNOT_1_1[BitState("0_")] == (BitState("__"), [[1, 2]], [CNOT_1_1])
        @test CNOT_1_1[BitState("10")] == (BitState("11"), [[1], [2]], [ID_1, NOT])
        @test CNOT_1_1[BitState("11")] == (BitState("10"), [[1], [2]], [ID_1, NOT])
        @test CNOT_1_1[BitState("1e")] == (BitState("1e"), [[1], [2]], [ID_1, ID_1])
        @test CNOT_1_1[BitState("1_")] == (BitState("__"), [[1, 2]], [CNOT_1_1])
        @test CNOT_1_1[BitState("ee")] == (BitState("ee"), [[1], [2]], [ID_1, ID_1])
        @test CNOT_1_1[BitState("e_")] == (BitState("__"), [[1, 2]], [CNOT_1_1])
        @test CNOT_1_1[BitState("_0")] == (BitState("__"), [[1, 2]], [CNOT_1_1])
        @test CNOT_1_1[BitState("_1")] == (BitState("__"), [[1, 2]], [CNOT_1_1])
        @test CNOT_1_1[BitState("_e")] == (BitState("__"), [[1, 2]], [CNOT_1_1])
        @test CNOT_1_1[BitState("__")] == (BitState("__"), [[1, 2]], [CNOT_1_1])

        @test_throws ErrorException CNOT_1_1[BitState("e0")]
        @test_throws ErrorException CNOT_1_1[BitState("e1")]

        # Test without gate set
        @test cnot_1_1[BitState("00")] == (BitState("00"), [[1], [2]], [id_1, id_1])
        @test cnot_1_1[BitState("01")] == (BitState("01"), [[1], [2]], [id_1, id_1])
        @test cnot_1_1[BitState("0e")] == (BitState("0e"), [[1], [2]], [id_1, id_1])
        @test cnot_1_1[BitState("0_")] == (BitState("__"), [[1, 2]], [cnot_1_1])
        @test cnot_1_1[BitState("10")] == (BitState("11"), [[1], [2]], [id_1, not])
        @test cnot_1_1[BitState("11")] == (BitState("10"), [[1], [2]], [id_1, not])
        @test cnot_1_1[BitState("1e")] == (BitState("1e"), [[1], [2]], [id_1, id_1])
        @test cnot_1_1[BitState("1_")] == (BitState("__"), [[1, 2]], [cnot_1_1])
        @test cnot_1_1[BitState("ee")] == (BitState("ee"), [[1], [2]], [id_1, id_1])
        @test cnot_1_1[BitState("e_")] == (BitState("__"), [[1, 2]], [cnot_1_1])
        @test cnot_1_1[BitState("_0")] == (BitState("__"), [[1, 2]], [cnot_1_1])
        @test cnot_1_1[BitState("_1")] == (BitState("__"), [[1, 2]], [cnot_1_1])
        @test cnot_1_1[BitState("_e")] == (BitState("__"), [[1, 2]], [cnot_1_1])
        @test cnot_1_1[BitState("__")] == (BitState("__"), [[1, 2]], [cnot_1_1])

        @test_throws ErrorException cnot_1_1[BitState("e0")]
        @test_throws ErrorException cnot_1_1[BitState("e1")]
    end

    @testset ".. multi cnot 3 2" begin
        @test CNOT_3_2.transform == VectorTransformation(vcat(collect(1:28), [32,31,30,29]))
        @test cnot_3_2.transform == VectorTransformation(vcat(collect(1:28), [32,31,30,29]))

        ## Test with gate set
        # Check for 0 in control.
        @test CNOT_3_2[zeros(Bit,5)] == (zeros(Bit,5), [[i] for i in 1:5], [id_1 for i in 1:5])
        @test CNOT_3_2[BitState("e_011")] == (BitState("_____"), [[1, 2, 3, 4, 5]], [CNOT_3_2])
       
        # Check for only e in target.
        @test CNOT_3_2[BitState("eeeee")] == (BitState("eeeee"), [[i] for i in 1:5], [ID_1 for i in 1:5])
        @test CNOT_3_2[BitState("__eee")] == (BitState("_____"), [[1, 2, 3, 4, 5]], [CNOT_3_2])
        @test CNOT_3_2[BitState("1_1ee")] == (BitState("_____"), [[1, 2, 3, 4, 5]], [CNOT_3_2])

        # Check for 1 removal.
        @test CNOT_3_2[BitState("11100")] == (BitState("11111"), [[i] for i in 1:5], [ID_1, ID_1, ID_1, NOT, NOT])
        @test CNOT_3_2[BitState("11111")] == (BitState("11100"), [[i] for i in 1:5], [ID_1, ID_1, ID_1, NOT, NOT])

        # Check for e removal
        @test CNOT_3_2[BitState("__e1e")] == (BitState("_____"), [[1, 2, 3, 4, 5]], [CNOT_3_2])

        # Check for open legs.
        @test CNOT_3_2[BitState("ee_01")] == (BitState("_____"), [[1, 2, 3, 4, 5]], [CNOT_3_2])
        @test CNOT_3_2[BitState("__101")] == (BitState("_____"), [[1, 2, 3, 4, 5]], [CNOT_3_2])
        @test CNOT_3_2[BitState("eee_1")] == (BitState("_____"), [[1, 2, 3, 4, 5]], [CNOT_3_2])
        @test CNOT_3_2[BitState("11_11")] == (BitState("_____"), [[1, 2, 3, 4, 5]], [CNOT_3_2])
        
        ## Test without gate set
        # Check for 0 in control.
        @test cnot_3_2[zeros(Bit,5)] == (zeros(Bit,5), [[i] for i in 1:5], [id_1 for i in 1:5])
        @test cnot_3_2[BitState("e_011")] == (BitState("_____"), [[1, 2, 3, 4, 5]], [cnot_3_2])

        # Check for only e in target.
        @test cnot_3_2[BitState("eeeee")] == (BitState("eeeee"), [[i] for i in 1:5], [id_1 for i in 1:5])
        @test cnot_3_2[BitState("__eee")] == (BitState("_____"), [[1, 2, 3, 4, 5]], [cnot_3_2])
        @test cnot_3_2[BitState("1_1ee")] == (BitState("_____"), [[1, 2, 3, 4, 5]], [cnot_3_2])

        # Check for 1 removal.
        @test cnot_3_2[BitState("11100")] == (BitState("11111"), [[i] for i in 1:5], [id_1, id_1, id_1, not, not])
        @test cnot_3_2[BitState("11111")] == (BitState("11100"), [[i] for i in 1:5], [id_1, id_1, id_1, not, not])

        # Check for e removal
        @test cnot_3_2[BitState("__e1e")] == (BitState("_____"), [[1, 2, 3, 4, 5]], [cnot_3_2])

        # Check for open legs.
        @test cnot_3_2[BitState("ee_01")] == (BitState("_____"), [[1, 2, 3, 4, 5]], [cnot_3_2])
        @test cnot_3_2[BitState("__101")] == (BitState("_____"), [[1, 2, 3, 4, 5]], [cnot_3_2])
        @test cnot_3_2[BitState("eee_1")] == (BitState("_____"), [[1, 2, 3, 4, 5]], [cnot_3_2])
        @test cnot_3_2[BitState("11_11")] == (BitState("_____"), [[1, 2, 3, 4, 5]], [cnot_3_2])

    end

    @testset ".. swap" begin

        @test swap[BitState("00")] == (BitState("00"), [[1], [2]], [id_1, id_1])
        @test swap[BitState("01")] == (BitState("10"), [[1, 2]], [swap])
        @test swap[BitState("ee")] == (BitState("ee"), [[1], [2]], [id_1, id_1])
        @test swap[BitState("e_")] == (BitState("_e"), [[1, 2]], [swap])
        @test swap[BitState("__")] == (BitState("__"), [[1, 2]], [swap])        
    end

    @testset ".. kronecker product" begin

        # Test with the combination of id_1 and not gates
        id_1_not = kron(id_1, not)
        
        @test id_1_not[BitState("00")] == (BitState("01"), [[1], [2]], [id_1, not])
        @test id_1_not[BitState("10")] == (BitState("11"), [[1], [2]], [id_1, not])
        @test id_1_not[BitState("11")] == (BitState("10"), [[1], [2]], [id_1, not])
        @test id_1_not[BitState("e0")] == (BitState("e1"), [[1], [2]], [id_1, not])
        @test id_1_not[BitState("0_")] == (BitState("0_"), [[1], [2]], [id_1, not])
        @test id_1_not[BitState("_0")] == (BitState("_1"), [[1], [2]], [id_1, not])
        @test id_1_not[BitState("e_")] == (BitState("e_"), [[1], [2]], [id_1, not])
    end

    @testset ".. gate multiplication" begin
        id_2 = identity_gate(2)

        @test id_1 * id_1 == id_1
        @test not * not == id_1
        @test id_1 * not == not
        @test cnot * cnot == id_2
        @test id_2 * cnot == cnot

        @test (cnot * swap).transform == VectorTransformation([1,3,4,2])
        @test (swap * cnot).transform == VectorTransformation([1,4,2,3])        
    end 

    @testset ".. random gate" begin
        function is_linear(gate::Gate)
            n_bits = gate.n_bits
            basis = [zeros(Bit, n_bits) for i in 1:n_bits]
            for i in 1:n_bits
                basis[i][i] = bit_1
            end
            for i in 1:n_bits
                for j in (i+1):n_bits
                    lhs = gate.transform * (basis[i] + basis[j])
                    rhs = gate.transform * basis[i] + gate.transform * basis[j]
                    if lhs != rhs
                        return false
                    end
                end
            end
            return true
        end

        for i in 1:10
            gate = random_linear_reversible_gate(3)        
            @test is_linear(gate)
            @test gate[BitState("000")] == (BitState("000"), [[1,2,3]], [gate])
            @test gate[BitState("eee")] == (BitState("eee"), [[1,2,3]], [identity_gate(3)])
            @test gate[BitState("_01")] == (BitState("___"), [[1,2,3]], [gate])

        end

        for i in 1:10
            gate = random_linear_reversible_gate(5)        
            @test is_linear(gate)
            @test gate[BitState("00000")] == (BitState("00000"), [[1,2,3,4,5]], [gate])
            @test gate[BitState("eeeee")] == (BitState("eeeee"), [[1,2,3,4,5]], [identity_gate(5)])
            @test gate[BitState("ee_01")] == (BitState("_____"), [[1,2,3,4,5]], [gate])
        end
    end
end


