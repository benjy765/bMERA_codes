include("../Sources/bmera_codes.jl")
using BMeraCodes
using BMeraCodes: generate_sublayers, generate_permutations
using Base.Test

cnot = elementary_gates["cnot"]
@testset "EncodingCircuit" begin
    @testset "Constructor" begin
        decoder = polar_code(3, cnot)
        encoder = EncodingCircuit(decoder)

        @test encoder.branching == 2
        @test encoder.n_layers == 3
        @test encoder.n_bits == 8

        for i in 1:3
            encoder.sublayers[i] == [SubLayer(8, [GatePosition(cnot,i,i+1) for i in 1:2:7])]
        end

        encoder.perms[1] = [1,3,5,7,2,4,6,8]
        encoder.perms[2] = [1,3,2,4,5,7,6,8]
        encoder.perms[3] = [1,2,3,4,5,6,7,8]
    end    

    @testset "generate sublayers" begin
        pc = polar_code(3, cnot)

        for i in 1:3
            gates = generate_sublayers(pc.layers[i])
            @test gates == [SubLayer(8, [GatePosition(cnot,i,i+1) for i in 1:2:7])]
        end
    end

    @testset "generate permutation" begin        
        # n_bits = 16, branching = 2, n_layers = 4
        @test generate_permutations(2, 1, 4) == vcat(collect(1:2:15), collect(2:2:16))
        @test generate_permutations(2, 2, 4) == vcat([[1,3,5,7,2,4,6,8] .+ x for x in [0,8]]...)
        @test generate_permutations(2, 3, 4) == vcat([[1,3,2,4] .+ x for x in [0,4,8,12]]...)
        @test generate_permutations(2, 4, 4) == collect(1:16)

        # n_bits = 27, branching = 3, n_layers = 3
        @test generate_permutations(3, 1, 3) == vcat([collect(1:3:25) .+ x for x in 0:2]...)
        @test generate_permutations(3, 2, 3) == vcat([[1,4,7,2,5,8,3,6,9] .+ x for x in [0,9,18]]...)
        @test generate_permutations(3, 3, 3) == collect(1:27)
    end

    @testset "encode" begin
        decoder = polar_code(3, cnot)
        encoder = EncodingCircuit(decoder)

        state = BitState("00000000")
        @test encode(state, encoder) == BitState("00000000")
        @test state == BitState("00000000")

        state = BitState("00101100")
        @test encode(state, encoder) == BitState("01100011")
        @test state == BitState("00101100")
    end
end