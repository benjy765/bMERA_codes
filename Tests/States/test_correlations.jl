include("../../Sources/States/states.jl")
#=
l = LeftCorrelatedProbState(0.1, 0.3)
r = RightCorrelatedProbState(0.3, 0.6)
m = MidCorrelatedProbState(0.2, 0.4) 
m1 = MidCorrelatedProbState(0.3,0.5)
c = Correlator([0.1 0.4; 0.9 0.6]) 

# Start with a mid mid cnot: 
tps = contract_cps(c, l, contract_cps(c, m, contract_cps(c, m1, r)))

mmc = contract_cnot(contract_cps(c, m, m1))

tps_cnot = contract_cps(c, l, contract_cps(c, mmc, r))

println("1. contraction mid-mid with cnot")
#println("bsps = ", BitStringProbState(tps).prob)
#println("bsps_cnot = ", BitStringProbState(tps_cnot).prob)
result = VectorTransformation([1,2,3,4,7,8,5,6,9,10,11,12,15,16,13,14])*tps
#println("Answer should be : ", BitStringProbState(result).prob)
println("Result = ", result.prob ≈ tps_cnot.prob)  
println("")


# Left-Mid cnot: 
#tps = contract_cps(c, l, contract_cps(c,m,r))
tps = contract_cps(c, contract_cps(c, l, m), r)
lmc = contract_cnot(contract_cps(c,l,m))

tps_cnot = contract_cps(c, lmc, r)

println("2. contraction left-mid with cnot")
#println("bsps = ", BitStringProbState(tps).prob)
#println("bsps_cnot = ", BitStringProbState(tps_cnot).prob)
result = VectorTransformation([1,2,3,4,7,8,5,6])*tps
#println("Answer should be : ", BitStringProbState(result).prob)
println("Result = ", result.prob ≈ tps_cnot.prob)
println("")

# Mid-Right cnot: 
tps = contract_cps(c, l, contract_cps(c,m,r))

mrc = contract_cnot(contract_cps(c,m,r))

tps_cnot = contract_cps(c, l, mrc)

println("3. contraction mid-right with cnot")
#println("bsps = ", BitStringProbState(tps).prob)
#println("bsps_cnot = ", BitStringProbState(tps_cnot).prob)
result = VectorTransformation([1,2,4,3,5,6,8,7])*tps
#println("Answer should be : ", BitStringProbState(result).prob)
println("Result = ", result.prob ≈ tps_cnot.prob)
println("")


# Left and Right with 2 MidCorrelatedProbState
# Left - Mid 4 bits
tps = contract_cps(c, l, contract_cps(c, m, contract_cps(c, m1, r)))

lmc = contract_cnot(contract_cps(c, l, m))

tps_cnot = contract_cps(c, lmc, contract_cps(c, m1, r))

println("4. contraction left-mid 4 bits with cnot")
#println("bsps = ", BitStringProbState(tps).prob)
#println("bsps_cnot = ", BitStringProbState(tps_cnot).prob)
result = VectorTransformation([1,2,3,4,5,6,7,8,13,14,15,16,9,10,11,12])*tps
#println("Answer should be : ", BitStringProbState(result).prob)
println("Result = ", result.prob ≈ tps_cnot.prob)
println("")

# Mid - Right 4 bits
tps = contract_cps(c, l, contract_cps(c, m, contract_cps(c, m1, r)))

mrc = contract_cnot(contract_cps(c, m1, r))

tps_cnot = contract_cps(c, l, contract_cps(c,m, mrc))

println("5. contraction mid-right 4 bits with cnot")
#println("bsps = ", BitStringProbState(tps).prob)
#println("bsps_cnot = ", BitStringProbState(tps_cnot).prob)
result = VectorTransformation([1,2,4,3,5,6,8,7,9,10,12,11,13,14,16,15])*tps
#println("Answer should be : ", BitStringProbState(result).prob)
println("Result = ", result.prob ≈ tps_cnot.prob)
println("")


# With 3 MidCorrelatedProbState
m2 = MidCorrelatedProbState(0.6,0.4)
tps = contract_cps(c, l, contract_cps(c, m, contract_cps(c,m1, contract_cps(c,m2, r))))

# CNOT Mid m-m1
mm1 = contract_cnot(contract_cps(c, m, m1))

tps_cnot = contract_cps(c, l, contract_cps(c, mm1, contract_cps(c, m2, r)))

println("6. contraction m-m1 with cnot")
#println("bsps = ", BitStringProbState(tps).prob)
#println("bsps_cnot = ", BitStringProbState(tps_cnot).prob)
result = kron(VectorTransformation([1,2]), kron(VectorTransformation([1,2,4,3]), VectorTransformation([1,2,3,4])))*tps
#println("Answer should be : ", BitStringProbState(result).prob)
println("Result = ", result.prob ≈ tps_cnot.prob)
println("")

# CNOT Mid m1-m2
m1m2 = contract_cnot(contract_cps(c, m1, m2))

tps_cnot = contract_cps(c, l, contract_cps(c, m, contract_cps(c, m1m2, r)))

println("7. contraction m1-m2 with cnot")
#println("bsps = ", BitStringProbState(tps).prob)
#println("bsps_cnot = ", BitStringProbState(tps_cnot).prob)
result = kron(VectorTransformation([1,2,3,4]), kron(VectorTransformation([1,2,4,3]), VectorTransformation([1,2])))*tps
#println("Answer should be : ", BitStringProbState(result).prob)
println("Result = ", result.prob ≈ tps_cnot.prob)
println("")

# CNOT l-m and m2-r
lm = contract_cnot(contract_cps(c,l,m))
m2r = contract_cnot(contract_cps(c,m2,r))

tps_cnot = contract_cps(c, lm, contract_cps(c, m1, m2r))

result = kron(VectorTransformation([1,2,4,3]), kron(VectorTransformation([1,2]), VectorTransformation([1,2,4,3])))*tps 
println("8. cnot lm and cnot m2r")
#println("bsps = ", BitStringProbState(tps).prob)
#println("bsps_cnot = ", BitStringProbState(tps_cnot).prob)
#println("Answer should be : ", BitStringProbState(result).prob)
println("Result = ", result.prob ≈ tps_cnot.prob)
println("")

# CNOT l-m

tps_cnot = contract_cps(c, lm, contract_cps(c, m1, contract_cps(c, m2, r)))
result = kron(VectorTransformation([1,2,4,3]), VectorTransformation([1,2,3,4,5,6,7,8]))*tps 
println("9. cnot lm")
#println("bsps = ", BitStringProbState(tps).prob)
#println("bsps_cnot = ", BitStringProbState(tps_cnot).prob)
#println("Answer should be : ", BitStringProbState(result).prob)
println("Result = ", result.prob ≈ tps_cnot.prob)
println("")
# CNOT m2-r

tps_cnot = contract_cps(c, l, contract_cps(c, m, contract_cps(c, m1, m2r)))
result = kron(VectorTransformation([1,2,3,4,5,6,7,8]), VectorTransformation([1,2,4,3]))*tps 
println("10. cnot m2r")
#println("bsps = ", BitStringProbState(tps).prob)
#println("bsps_cnot = ", BitStringProbState(tps_cnot).prob)
#println("Answer should be : ", BitStringProbState(result).prob)
println("Result = ", result.prob ≈ tps_cnot.prob)
println("")





# ------------------------- Example -----------------------------------
# Pg = 0.0 Pb = 1.0
# Corr = [1 0; 0 1]
# Start and finish in good

c = Correlator(1.,1.)
l = LeftCorrelatedProbState(0.0, 1.0, 1., 0.)
m = MidCorrelatedProbState(0.0, 1.0)
r = RightCorrelatedProbState(0.0, 1.0, 1., 0.)

# Test 1 : contract_cps c l,r

tps = contract_cps(c,l,r)
println("Test 1. ")
#println("bsps = ", BitStringProbState(tps).prob)
println("Result = ", tps.prob[1,1] == 1.0)

# Test 2 : contract_cps l,m,r

tps = contract_cps(c, l, contract_cps(c, m, r))
println("Test 2. ")
#println("bsps = ", BitStringProbState(tps).prob)
println("Result = ", tps.prob[1,1,1] == 1.0)

# Contract with a cnot lm.

lm = contract_cnot(contract_cps(c,l,m))

tps = contract_cps(c, lm, r)
println("Test 3. ")
#println("bsps = ", BitStringProbState(tps).prob)
println("Result = ", tps.prob[1,1,1] == 1.0)

# Contract with a cnot mr.

mr = contract_cnot(contract_cps(c,m,r))

tps = contract_cps(c, l, mr)
println("Test 4. ")
#println("bsps = ", BitStringProbState(tps).prob)
println("Result = ", tps.prob[1,1,1] == 1.0)
println("")

println("----------------------------------- New Set of test -----------------------------------")
# State 010 with prob = 1
c = Correlator(1.,1.)
l = LeftCorrelatedProbState(0., 1.0, 1., 0.)
m = MidCorrelatedProbState(1., 1.0)
r = RightCorrelatedProbState(0., 1.0, 1., 0.)

# Test 1 : contract_cps c l,m,r

tps = contract_cps(c,l,contract_cps(c,m,r))
println("Test 1. ")
#println("bsps = ", BitStringProbState(tps).prob)
println("Result = ", tps.prob[1,2,1] == 1)

# Test 2 : CNOT left-mid should do nothing.
lm = contract_cnot(contract_cps(c, l, m))

tps = contract_cps(c, lm, r)
println("Test 2.")
#println("bsps = ", BitStringProbState(tps).prob)
println("Result = ", tps.prob[1,2,1] == 1)

# Test 3 : CNOT mid-right should change to 011.
mr = contract_cnot(contract_cps(c, m,r))

tps = contract_cps(c, l, mr)
println("Test 3.")
#println("bsps = ", BitStringProbState(tps).prob)
println("Result = ", tps.prob[1,2,2] == 1)
println("")

println("----------------------------------- New Set of test -----------------------------------")
# Trying to get the state 0010 with prob = 1
m1 = MidCorrelatedProbState(0., 0.)
# Test 1 : contraction l, m1, m, r
tps = contract_cps(c, l, contract_cps(c, m1, contract_cps(c, m, r)))
println("Test 1. ")
#println("bsps = ", BitStringProbState(tps).prob)
println("Result = ", tps.prob[1,1,2,1] == 1) #Instead we have the state 0100


lm1 = contract_cps(c, l, m1)
lm1m = contract_cps(c,lm1,m)
tps = contract_cps(c, lm1m, r)
println("Test 2. ")
#println("bsps = ", BitStringProbState(tps).prob)
println("Result = ", tps.prob[1,1,2,1] == 1) #Instead we have the state 0100

println("")
println("------------------------- Test with BitState ---------------------------")
println("")
=#
# Def:
c = Correlator(0.1, 0.5)
l = LeftCorrelatedProbState(0.1, 0.2)
m1 = MidCorrelatedProbState(0.4, 0.2)
m2 = MidCorrelatedProbState(0.3, 0.5)
r = RightCorrelatedProbState(0.1, 0.4)

bs1 = BitState("__")
bs2 = BitState("e_")
bs3 = BitState("_0")
bs4 = BitState("_1")

# Start with mid1-mid2
m1m2 = contract_cps(c,m1,m2)
#=
println("m1m2 = ", m1m2.prob)
println("Result 1 : ", ((bs1*m1m2).prob ≈ m1m2.prob))
println("Result 2 : ", (bs2*m1m2).prob)
println("Result 3 : ", (bs3*m1m2).prob)
println("Result 4 : ", (bs4*m1m2).prob)
=#