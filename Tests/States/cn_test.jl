include("../../Sources/States/states.jl")

l = LeftCorrelatedProbState(0.0, 0.5, 1., 0.)
r = RightCorrelatedProbState(0.0, 0.5, 1., 0.)
m = MidCorrelatedProbState(0.0, 0.5)
corr = Correlator(0.97, 0.75)
n_bits = 2^8

# Correlated Noise
cn = CorrelatedNoise(n_bits, corr, l, m, r)

# First contraction 
chain = [cn[i, i+1] for i in 1:2:cn.n_bits]

cn1 = CorrelatedNoise(cn.corr, chain)
