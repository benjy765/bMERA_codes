include("../../Sources/States/states.jl")

using Base.Test

@testset "Bit and prob state" begin
    @testset "BitState" begin
        @testset ".. constructor" begin
            bs = BitState("ee___01")
            @test bs == [bit_e, bit_e, bit__, bit__, bit__, bit_0, bit_1] 
            @test_throws ErrorException BitState("ea") # a is not a valid bit
        end

        @testset ".. permute" begin
            bs = BitState("ee__01")
            @test permute(bs, [4,3,1,2,6,5]) == BitState("__ee10")
        end

    end

    @testset "ZeroProbState" begin
        @testset ".. constructor" begin
            @test_throws ErrorException ZeroProbState([0.1,0.3,1.2]) # 0 <= p <= 1
            @test ZeroProbState(0.4).prob == Vector{Float64}([0.4]) # Check float constructor
        end

        zps = ZeroProbState([0.2, 0.4, 0.6, 0.9])

        @testset ".. getindex " begin
            @test zps[2] == ZeroProbState(0.4)
            @test zps[2:3] == ZeroProbState([0.4, 0.6])   
            @test zps[[1,3]] == ZeroProbState([0.2, 0.6])             
        end

        @testset " .. setindex" begin
            zps[4] = 0.7 
            @test zps == ZeroProbState([0.2, 0.4, 0.6, 0.7])
            
            zps[1:3] = [0.4, 0.2, 0.5]
            @test zps == ZeroProbState([0.4, 0.2, 0.5, 0.7])
        end

        zps = ZeroProbState([0.2, 0.4, 0.6, 0.9])
        @testset " .. permute" begin
            @test permute(zps, [2, 3, 1, 4]) == ZeroProbState([0.4, 0.6, 0.2, 0.9])
        end
    end

    @testset "BinProbState" begin
        @testset ".. constructor" begin
            @test_throws ErrorException BinProbState([0.2 0.4 0.4 ; 0.3 0.3 0.4 ; 0.8 0.1 0.1]) # must be a 2 x n array
            @test_throws ErrorException BinProbState([0.2 0.8 ; 1.2 -0.2 ; 0.5 0.5]) # 0 <= p <= 1 
            @test_throws ErrorException BinProbState([0.2 0.8 ; 0.6 0.4 ; 0.5 0.7]) # Sum (bit) == 1
        end

        bps = BinProbState([0.2 0.8 ; 0.4 0.6 ; 0.3 0.7])

        @testset ".. getindex" begin
            @test bps[2] == BinProbState([0.4  0.6])
            @test bps[2:3] == BinProbState([0.4 0.6 ; 0.3 0.7])
            @test bps[[1,3]] == BinProbState([0.2 0.8 ; 0.3 0.7])    
        end

        @testset ".. setindex" begin
            bps[2] = [0.1  0.9]
            @test bps == BinProbState([0.2 0.8 ; 0.1 0.9 ; 0.3 0.7])
            
            bps[1:2] = [0.1 0.9 ; 0.6 0.4]
            @test bps == BinProbState([0.1 0.9 ; 0.6 0.4 ; 0.3 0.7])

            @test_throws ErrorException bps[1] = [-0.3  1.3] # 0 <= p <= 1
            @test_throws ErrorException bps[1:2] = [0.1 0.6 ; 0.3 0.4] # Sum (bit) == 1
        end

        bps = BinProbState([0.2 0.8 ; 0.4 0.6 ; 0.3 0.7])
        @testset " .. permute" begin
            @test permute(bps, [2, 3, 1]) == BinProbState([0.4 0.6 ; 0.3 0.7 ; 0.2 0.8])
        end
    end

    @testset "TensorProbState" begin
        @testset ".. TensorProbState exception" begin
            @test_throws ErrorException TensorProbState([0.2 0.3; 0.1  0.3 ; 0.7 0.4]) # size == (2,2,2,...)
            @test_throws ErrorException TensorProbState([0.2 -0.5; 1.2 0.1]) # 0 <= p <= 1 
            @test_throws ErrorException TensorProbState([0.2 0.2; 0.1 0.3]) # Sum == 1
        end

        prob = zeros(2,2,2,2)
        prob[:,:,1,1] = [0.075 0.025 ; 0.1 0.1]
        prob[:,:,1,2] = [0.075 0.025 ; 0.1 0.1]
        prob[:,:,2,1] = [0.05 0.065 ; 0.035 0.05]
        prob[:,:,2,2] = [0.05 0.065 ; 0.035 0.05]
        tps = TensorProbState(prob)

        @testset ".. getindex" begin
            @test tps[2] ≈ TensorProbState([0.52, 0.48])
            @test tps[2:3] ≈ TensorProbState([0.35 0.17 ; 0.25 0.23])
            @test tps[[1,3]] ≈ TensorProbState([0.2 0.23 ; 0.4 0.17])
        end

        @testset ".. fix_bit" begin
            new_prob = prob[:,:,2,:] / sum(prob[:,:,2,:])
            @test fix_bits(tps, bit_1, 3) ≈ TensorProbState(new_prob)
            
            new_prob = prob[:,:,2,1] / sum(prob[:,:,2,1])
            @test fix_bits(tps, [bit_1, bit_0], 3:4) ≈ TensorProbState(new_prob)

            @test_throws ErrorException fix_bits(tps, bit_e, 3)
            @test_throws ErrorException fix_bits(tps, [bit__, bit_1], 2:3)
            @test_throws ErrorException fix_bits(tps, [bit_1], 2:3)
            @test_throws ErrorException fix_bits(tps, [bit_1, bit_0, bit_1], 2:3)
        end

        @testset " .. permute" begin
            new_prob = zeros(2,2,2,2)
            new_prob[:,:,1,1] = [0.075 0.05 ; 0.025 0.065]
            new_prob[:,:,1,2] = [0.075 0.05 ; 0.025 0.065]
            new_prob[:,:,2,1] = [0.1 0.035 ; 0.1 0.05]
            new_prob[:,:,2,2] = [0.1 0.035 ; 0.1 0.05]
            new_tps = TensorProbState(new_prob)
            @test permute(tps, [2, 3, 1, 4]) == new_tps
        end
    end

    @testset "BitStringProbState" begin
        @testset ".. constructor" begin
            @test_throws ErrorException BitStringProbState([0.2, 0.5, 0.3]) # Length == 2^n
            @test_throws ErrorException BitStringProbState([0.2, -0.5, 1.1, 0.2]) # 0 <= p <= 1 
            @test_throws ErrorException BitStringProbState([0.2, 0.4, 0.1, 0.2]) # Sum == 1
            @test_throws ErrorException BitStringProbState(0.8) # Sum == 1 (Float constructor)
            @test BitStringProbState(1.0).prob == Vector{Float64}([1.0]) # Check float constructor
        end

        bsps = BitStringProbState([0.075, 0.075, 0.05, 0.05, 0.025, 0.025, 0.065, 0.065, 0.1, 0.1, 0.035, 0.035, 0.1, 0.1, 0.05, 0.05])
        @testset ".. getindex" begin
            @test bsps[2] ≈ BitStringProbState([0.52, 0.48])
            @test bsps[2:3] ≈ BitStringProbState([0.35, 0.17, 0.25, 0.23])
            @test bsps[[1,3]] ≈ BitStringProbState([0.2, 0.23, 0.4, 0.17])
        end

        @testset ".. fix_bit" begin
            new_prob = [0.05, 0.05, 0.065, 0.065, 0.035, 0.035, 0.05, 0.05]
            @test fix_bits(bsps, bit_1, 3) ≈ BitStringProbState(new_prob / sum(new_prob))
            
            new_prob = [0.05, 0.065, 0.035, 0.05]
            @test fix_bits(bsps, [bit_1, bit_0], 3:4) ≈ BitStringProbState(new_prob / sum(new_prob))

            @test_throws ErrorException fix_bits(bsps, bit_e, 3)
            @test_throws ErrorException fix_bits(bsps, [bit__, bit_1], 2:3)
            @test_throws ErrorException fix_bits(bsps, [bit_1], 2:3)
            @test_throws ErrorException fix_bits(bsps, [bit_1, bit_0, bit_1], 2:3)
        end

        @testset " .. permute" begin
            @test permute(bsps, [2, 3, 1, 4]) == BitStringProbState([0.075, 0.075, 0.1, 0.1, 0.05, 0.05, 0.035, 0.035, 0.025, 0.025, 0.1, 0.1 ,0.065, 0.065, 0.05, 0.05])
        end
    end

    @testset "Conversion" begin 
        z_prob = [0.2, 0.6, 0.7]
        bin_prob = [0.2 0.8 ; 0.6 0.4 ; 0.7 0.3]
        bs_prob = [0.084, 0.036, 0.056, 0.024, 0.336, 0.144, 0.224, 0.096]
        t_prob = zeros(2,2,2)
        t_prob[:,:,1] = [0.084 0.056; 0.336 0.224]
        t_prob[:,:,2] = [0.036 0.024; 0.144 0.096]
        
        bits = Vector{Bit}([1, 0, 0])
        
        @testset ".. BinProbState to ZeroProbState" begin
            bps = BinProbState(bin_prob)
            zps = ZeroProbState(bps)
            @test zps.prob ≈ z_prob
        end

        @testset ".. ZeroProbState to BinProbState" begin
            zps = ZeroProbState(z_prob)
            bps = BinProbState(zps)  
            @test bps.prob ≈ bin_prob
        end

        @testset ".. to TensorProbState" begin
            @testset ".. .. from ZeroProbState" begin
                zps = ZeroProbState(z_prob)
                tps = TensorProbState(zps)
                @test tps.prob ≈ t_prob
            end

            @testset ".. .. from BinProbState" begin
                bps = BinProbState(bin_prob)
                tps = TensorProbState(bps)
                @test tps.prob ≈ t_prob
            end

            @testset ".. .. from bsps" begin
                bsps = BitStringProbState(bs_prob)
                tps = TensorProbState(bsps)
                @test tps.prob ≈ t_prob
            end
        end

        @testset ".. to BitStringProbState" begin
            @testset ".. .. from ZeroProbState" begin
                zps = ZeroProbState(z_prob)
                bsps = BitStringProbState(zps)
                @test bsps.prob ≈ bs_prob
            end

            @testset ".. .. from BinProbState" begin
                bps = BinProbState(bin_prob)
                bsps = BitStringProbState(bps)
                @test bsps.prob ≈ bs_prob
            end

            @testset ".. .. from TensorProbState" begin
                tps = TensorProbState(t_prob)
                bsps = BitStringProbState(tps)
                @test bsps.prob ≈ bs_prob
            end
        end

        @testset ".. to BitState" begin   
            @testset ".. .. from ZeroProbState" begin    
                zps = ZeroProbState(z_prob)
                bs = BitState(zps)
                @test bs == bits
            end
            
            @testset ".. .. from BinProbState" begin
                bps = BinProbState(bin_prob)
                bs = BitState(bps)
                @test bs == bits
            end

            @testset ".. .. from BitStringProbState" begin
                bsps = BitStringProbState(bs_prob)
                bs = BitState(bsps)
                @test bs == bits
            end

            @testset ".. .. from TensorProbState" begin
                tps = TensorProbState(t_prob)
                bs = BitState(tps)
                @test bs == bits
            end
        end
    end

    @testset "Error rate" begin
        @test bit_error_rate(BitState("0101"), BitState("0101"), 3) == 0.0
        @test bit_error_rate(BitState("0110"), BitState("0100"), 2) == 0.50
        @test bit_error_rate(BitState("1111"), BitState("0000"), 4) == 1.0
        @test_throws ErrorException bit_error_rate(BitState("0"), BitState("00"),4)

        @test frame_error_rate(BitState("0101"), BitState("0101")) == 0
        @test frame_error_rate(BitState("0110"), BitState("0100")) == 1
        @test frame_error_rate(BitState("1111"), BitState("0000")) == 1
        @test_throws ErrorException frame_error_rate(BitState("0"), BitState("00"))
    end
end

@testset "Correlated states" begin
    @testset "CorrelatedProbState" begin
        @testset ".. MidCorrelatedProbState" begin
            @testset ".. .. Constructor" begin
                @test_throws ErrorException MidCorrelatedProbState(0.2, 1.1) 
                cps = MidCorrelatedProbState(0.1, 0.3)
                @test cps.n_bits == 1

                @test_throws ErrorException MidCorrelatedProbState([0.5, 0.5]) # dim must be > 2

                dummy = zeros(2,2,3)
                dummy[1,1,:] = [0.1, 0.4, 0.5]
                dummy[1,2,:] = [0.1, 0.4, 0.5]
                dummy[2,1,:] = [0.1, 0.4, 0.5]
                dummy[2,2,:] = [0.1, 0.4, 0.5]
                
                @test_throws ErrorException MidCorrelatedProbState(dummy) # bonds dim must = 2

                prob = zeros(2,2,2)
                prob[1,1,:] = [0.1, 0.9]
                prob[1,2,:] = [0.5, 0.5]
                prob[2,1,:] = [1.1, 0.5]
                prob[2,2,:] = [0.3, 0.7] 

                #@test_throws ErrorException MidCorrelatedProbState(prob) # All prob must be 0 < prob < 1

                #prob[2,1,:] = [0.6, 0.5]

                #@test_throws ErrorException MidCorrelatedProbState(prob) # prob must sum to 1

                prob[2,1,:] = [0.5, 0.5]

                @test MidCorrelatedProbState(prob).n_bits == 1
            end
        end 

        @testset ".. Left/RightCorrelatedProbState" begin
            @testset ".. .. Constructor" begin
                @test_throws ErrorException LeftCorrelatedProbState(0.2, 1.1) 
                cps = LeftCorrelatedProbState(0.1, 0.3)
                @test cps.n_bits == 1

                @test_throws ErrorException LeftCorrelatedProbState([0.5, 0.5]) # dim must be > 2

                dummy = zeros(2,3)
                dummy[1,:] = [0.1, 0.4, 0.5]
                dummy[2,:] = [0.1, 0.4, 0.5]
                
                @test_throws ErrorException LeftCorrelatedProbState(dummy) # bonds dim must = 2

                prob = zeros(2,2)
                prob[1,:] = [0.1, 0.9]
                prob[2,:] = [1.1, 0.5]

                #@test_throws ErrorException LeftCorrelatedProbState(prob) # All prob must be 0 < prob < 1

                #prob[2,:] = [0.6, 0.5]

                #@test_throws ErrorException LeftCorrelatedProbState(prob) # prob must sum to 1

                prob[2,:] = [0.5, 0.5]

                @test LeftCorrelatedProbState(prob).n_bits == 1
                
                # The same goes for RightCorrelatedProbState ...
            end             
        end      
    end

    @testset "Correlator" begin
        @testset ".. .. Constructor" begin
            m = [0.1 0.3; 0.4 0.5; 0.6 0.7]
            @test_throws ErrorException Correlator(m) # Must be size = (2,2)

            m = [1.1 0.1; 2 0.4]
            #@test_throws ErrorException Correlator(m) # All prob must be in [0,1]

            m = [0.1 0.2; 0.3 0.5]
            #@test_throws ErrorException Correlator(m) # All column must sum to 1

            m = [0.4 0.3; 0.6 0.7]
            @test Correlator(m).corr == m 

            @test_throws ErrorException Correlator(1.1, 0.3) # All prob must be in [0,1]

            @test Correlator(0.4, 0.7).corr ≈ m
        end
    end

    @testset "CorrelatedNoise" begin
        @testset ".. Constructor" begin
            c = Correlator(0.1, 0.4)
            l = LeftCorrelatedProbState(0.1, 0.3)

            @test_throws ErrorException CorrelatedNoise(c, [l])

            m1 = MidCorrelatedProbState(0.2, 0.4)
            m2 = MidCorrelatedProbState(0.5, 0.5)
            r = RightCorrelatedProbState(0.2,0.5)

            @test CorrelatedNoise(c, [l,m1,m2,r]).n_bits == 4

            @test CorrelatedNoise(8, c, l, m1, r).chain == Vector{CorrelatedProbState}([l, fill(m1, 6)..., r])

            m1m2 = contract_cps(c,m1,m2)
            @test CorrelatedNoise(c, [l, m1, m2, m1m2, r]).n_bits == 6

        end        
    end
end

