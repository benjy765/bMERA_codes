include("../../Sources/States/states.jl")

using Base.Test

@testset "Contraction" begin

    @testset "ZeroProbState and BitState" begin
        zps = ZeroProbState([0.2, 0.4, 0.7, 0.9, 0.3])

        bs = BitState("ee1")
        @test_throws ErrorException bs * zps

        bs = BitState("ee101")
        contracted = bs * zps
        @test contracted == 1

        bs = BitState("ee_01")
        contracted = bs * zps
        @test contracted ≈ ZeroProbState([0.7])

        bs = BitState("e__01")
        contracted = bs * zps
        @test contracted ≈ ZeroProbState([0.4, 0.7])
    end

    @testset "BinProbState and BitState" begin
        bps = BinProbState([0.2 0.8 ; 0.4 0.6 ; 0.7 0.3 ; 0.9 0.1 ; 0.3 0.7])

        bs = BitState("ee1")
        @test_throws ErrorException bs * bps

        bs = BitState("ee101")
        contracted = bs * bps
        @test contracted == 1

        bs = BitState("ee_01")
        contracted = bs * bps
        @test contracted ≈ BinProbState([0.7 ; 0.3])

        bs = BitState("e__01")
        contracted = bs * bps
        @test contracted ≈ BinProbState([0.4 0.6 ; 0.7 0.3])
    end

    @testset "TensorProbState and BitState" begin
        tps = TensorProbState(ZeroProbState([0.2, 0.4, 0.7, 0.9, 0.3]))

        bs = BitState("ee1")
        @test_throws ErrorException bs * tps

        bs = BitState("ee101")
        contracted = bs * tps
        @test contracted == 1

        bs = BitState("ee_01")
        contracted = bs * tps
        @test contracted ≈ TensorProbState([0.7, 0.3])

        bs = BitState("e__01")
        contracted = bs * tps
        @test contracted ≈ TensorProbState([0.28 0.12; 0.42 0.18])
    end

    @testset "BitStringProbState and BitState" begin
        bsps = BitStringProbState(ZeroProbState([0.2, 0.4, 0.7, 0.9, 0.3]))

        bs = BitState("ee1")
        @test_throws ErrorException bs * bsps

        bs = BitState("ee101")
        contracted = bs * bsps
        @test contracted == 1

        bs = BitState("ee_01")
        contracted = bs * bsps
        @test contracted ≈ BitStringProbState([0.7, 0.3])

        bs = BitState("e__01")
        contracted = bs * bsps
        @test contracted ≈ BitStringProbState([0.28, 0.12, 0.42, 0.18])
    end

    right_tps = TensorProbState([0.40 0.15 ; 0.10 0.35])
    left_tps = TensorProbState([0.40 0.15 ; 0.35 0.10])

    right_bsps = BitStringProbState([0.40, 0.15, 0.10, 0.35])
    left_bsps = BitStringProbState([0.40, 0.15, 0.35, 0.10])

    @testset "vTransform" begin
        vTransform = VectorTransformation([1, 2, 4, 3]) # CNOT        
        
        @test vTransform * right_tps == left_tps
        @test left_tps * vTransform == right_tps
        
        @test vTransform * right_bsps == left_bsps
        @test left_bsps * vTransform == right_bsps
    end

    @testset "mTransform" begin
        mTransform = MatrixTransformation([1 0 0 0 ; 0 1 0 0 ; 0 0 0 1 ; 0 0 1 0])
    
        @test mTransform * right_tps == left_tps
        @test left_tps * mTransform == right_tps
    
        @test mTransform * right_bsps == left_bsps
        @test left_bsps * mTransform == right_bsps
    end

    @testset "vTransform and vTransform" begin
        vTransform1 = VectorTransformation([1, 2, 4, 3]) # CNOT
        vTransform2 = VectorTransformation([1, 4, 3, 2]) # reverse CNOT
        bsps = BitStringProbState([0.1, 0.2, 0.3, 0.4])
        vTransform3 = vTransform2 * vTransform1

        @test vTransform3 == VectorTransformation([1, 3, 4, 2])
        @test vTransform3 * bsps == vTransform2 * (vTransform1 * bsps)
    end

    @testset "mTransform and mTransform" begin
        mTransform1 = MatrixTransformation([1, 2, 4, 3])
        mTransform2 = MatrixTransformation([1, 4, 3, 2])
        bsps = BitStringProbState([0.1, 0.2, 0.3, 0.4])
        mTransform3 = mTransform2 * mTransform1

        @test mTransform3 == MatrixTransformation([1, 3, 4, 2])
        @test mTransform3 * bsps == mTransform2 * (mTransform1 * bsps)
    end

    @testset "BitState and Transformation" begin
        transform = VectorTransformation([1, 3, 4, 2])
        
        @test transform * BitState("00") == BitState("00")
        @test transform * BitState("01") == BitState("11")
        @test transform * BitState("10") == BitState("01")
        @test transform * BitState("11") == BitState("10")
        
        @test BitState("00") * transform == BitState("00")
        @test BitState("01") * transform == BitState("10")
        @test BitState("10") * transform == BitState("11")
        @test BitState("11") * transform == BitState("01")

        @test_throws ErrorException transform * BitState("_1")
    end
end

# Test for correlated states

@testset "Contraction for correlated states" begin
    @testset "contraction CPS" begin
        corr = Correlator(0.3, 0.6)
        @testset "MidCPS with MidCPS" begin
            cps1 = MidCorrelatedProbState(0.1, 0.3)
            cps2 = MidCorrelatedProbState(0.4, 0.5)

            cps3 = contract_cps(corr, cps1, cps2)

            @test cps3.n_bits == 2
            @test typeof(cps3) == MidCorrelatedProbState
            #@test cps3.prob[1,1,:,:] ≈ [0.162 0.018; 0.108 0.012]
            #@test cps3.prob[1,2,:,:] ≈ [0.18 0.02; 0.18 0.02]
            #@test cps3.prob[2,1,:,:] ≈ [0.294 0.126; 0.196 0.084]
            #@test cps3.prob[2,2,:,:] ≈ [0.21 0.09; 0.21 0.09]

            cps4 = contract_cnot(cps3)

            #@test cps4.prob[1,1,:,:] ≈ [0.162 0.012; 0.108 0.018]
            #@test cps4.prob[1,2,:,:] ≈ [0.18 0.02; 0.18 0.02]
            #@test cps4.prob[2,1,:,:] ≈ [0.294 0.084; 0.196 0.126]
            #@test cps4.prob[2,2,:,:] ≈ [0.21 0.09; 0.21 0.09]
        end

        @testset "LeftCPS with MidCPS" begin
            cps1 = LeftCorrelatedProbState(0.1, 0.3)
            cps2 = MidCorrelatedProbState(0.4, 0.5)

            cps3 = contract_cps(corr, cps1, cps2)

            @test cps3.n_bits == 2
            @test typeof(cps3) == LeftCorrelatedProbState
            #@test cps3.prob[1,:,:] ≈ 1/sqrt(2)*[0.456 0.304; 0.144 0.096]
            #@test cps3.prob[2,:,:] ≈ 1/sqrt(2)*[0.39 0.39; 0.11 0.11]

            cps4 = contract_cnot(cps3)

            #@test csp4.prob[1,:,:] ≈ [;]
            #@test csp4.prob[2,:,:] ≈ [;]
        end

        @testset "MidCPS with RightCPS" begin
            cps1 = MidCorrelatedProbState(0.4, 0.5)
            cps2 = RightCorrelatedProbState(0.1, 0.3)

            cps3 = contract_cps(corr, cps1, cps2)

            @test cps3.n_bits == 2
            @test typeof(cps3) == RightCorrelatedProbState
        end

        @testset "LeftCPS with RightCPS" begin
            cps1 = LeftCorrelatedProbState(0.0, 0.5)
            cps2 = RightCorrelatedProbState(0.1, 0.5)

            cps3 = contract_cps(corr, cps1, cps2)

            @test cps3.n_bits == 2
            @test typeof(cps3) == TensorProbState

            cps4 = MidCorrelatedProbState(0.4, 0.5)

            # 3 bits contraction

            cps5 = contract_cps(corr, cps1, contract_cps(corr, cps4, cps2))

            @test cps5.n_bits == 3
            @test typeof(cps5) == TensorProbState
        end
    end

    @testset "contraction CNOT" begin
        corr = Correlator(0.3, 0.6)
        @testset "MidCorrelatedProbState" begin
            cps1 = MidCorrelatedProbState(0.1, 0.3)
            cps2 = MidCorrelatedProbState(0.4, 0.5)
            cps3 = contract_cps(corr, cps1, cps2)

            @test_throws ErrorException contract_cnot(cps1)

            cps4 = contract_cnot(cps3)

            @test cps4.n_bits == 2
            @test typeof(cps4) == MidCorrelatedProbState
        end

        @testset "LeftCorrelatedProbState" begin
            cps1 = LeftCorrelatedProbState(0.1, 0.3)
            cps2 = MidCorrelatedProbState(0.4, 0.5)
            cps3 = contract_cps(corr, cps1, cps2)
            
            @test_throws ErrorException contract_cnot(cps1)

            cps4 = contract_cnot(cps3)

            @test cps4.n_bits == 2
            @test typeof(cps4) == LeftCorrelatedProbState        
            
        end

        @testset "RightCorrelatedProbState" begin
            cps1 = MidCorrelatedProbState(0.4, 0.5)
            cps2 = RightCorrelatedProbState(0.1, 0.3)
            cps3 = contract_cps(corr, cps1, cps2)

            @test_throws ErrorException contract_cnot(cps2)

            cps4 = contract_cnot(cps3)

            @test cps4.n_bits == 2
            @test typeof(cps4) == RightCorrelatedProbState
        end
    end

    @testset "Correlated Noise" begin
        # Contract from right to left
        # Channel are Identity and BSC(0.5)
        left = LeftCorrelatedProbState(0.0, 0.5, 1.0, 0.0)      # We start in Good State
        right = RightCorrelatedProbState(0.0, 0.5, 1.0, 0.0)    # We finish in Good State
        mid = MidCorrelatedProbState(0.0, 0.5)                  
        corr = Correlator([1.0 0.0; 0.0 1.0])                   # We will stay in Good State
        c_noise = CorrelatedNoise(8, corr, left, mid, right)

        @testset "Contract from Right to Left" begin
            # We expect to have the chain 000 with prob = 1
            tps = contractRtoL(c_noise)
            @test BitStringProbState(tps).prob[1] == 1.0  
        end

        @testset "Contract from Left to Right" begin
            # We expect to have the chain 000 with prob = 1
            tps = contractLtoR(c_noise)
            @test BitStringProbState(tps).prob[1] == 1.0  
        end

        @testset "Contract starting with the middle" begin
            tps = contractMid(c_noise)
            @test BitStringProbState(tps).prob[1] == 1.0
        end

        # Start in Bad with Bad is a flip with prob 1
        left = LeftCorrelatedProbState(0.0, 1.0, 0.0, 1.0) # We start in Bad State
        right = RightCorrelatedProbState(0.0, 1.0, 0.0, 1.0)    # We finish in Bad State
        mid = MidCorrelatedProbState(0.0, 1.0) 
        corr = Correlator([1.0 0.0; 0.0 1.0])                   # We will stay in Good State
        c_noise = CorrelatedNoise(8, corr, left, mid, right)

        @testset "Contract from Right to Left" begin
            # We expect to have the chain 111 with prob = 1
            tps = contractRtoL(c_noise)
            @test BitStringProbState(tps).prob[end] == 1.0  
        end

        @testset "Contract from Left to Right" begin
            # We expect to have the chain 111 with prob = 1
            tps = contractLtoR(c_noise)
            @test BitStringProbState(tps).prob[end] == 1.0  
        end

        @testset "Contract starting with the middle" begin
            tps = contractMid(c_noise)
            @test BitStringProbState(tps).prob[end] == 1.0
        end

        # Start in Bad with Bad is a flip with prob 1
        left = LeftCorrelatedProbState(0.5, 0.5) # We start in Bad State
        right = RightCorrelatedProbState(0.5, 0.5)    # We finish in Bad State
        mid = MidCorrelatedProbState(0.5, 0.5) 
        corr = Correlator([0.5 0.5; 0.5 0.5])                   # We will stay in Good State
        c_noise = CorrelatedNoise(8, corr, left, mid, right)

        @testset "Contract from Right to Left" begin
            # We expect to have the chain 111 with prob = 1
            bsps = BitStringProbState(contractRtoL(c_noise))
            @test all(x->x == bsps.prob[1], bsps.prob)  
        end

        @testset "Contract from Left to Right" begin
            # We expect to have the chain 111 with prob = 1
            bsps = BitStringProbState(contractLtoR(c_noise))
            @test all(x->x == bsps.prob[1], bsps.prob)  
        end

        @testset "Contract starting with the middle" begin
            bsps = BitStringProbState(contractMid(c_noise))
            @test all(x->x == bsps.prob[1], bsps.prob) 
        end

        #left = LeftCorrelatedProbState(0.1, 0.5) # We start in Bad State
        #right = RightCorrelatedProbState(0.2, 0.5)    # We finish in Bad State
        #mid = MidCorrelatedProbState(0.4, 0.5) 
        #corr = Correlator([0.1 0.4; 0.9 0.6])                   # We will stay in Good State
        #c_noise = CorrelatedNoise(8, corr, [left, mid, right])

        @testset "contractCorrNoise" begin
            c_noise = CorrelatedNoise(corr, [left, mid, mid, right])
            tps = contractCorrNoise(c_noise)
            @test tps.n_bits == 4
        end


        left = LeftCorrelatedProbState(0.1, 0.5) # We start in Bad State
        right = RightCorrelatedProbState(0.2, 0.5)    # We finish in Bad State
        mid = MidCorrelatedProbState(0.4, 0.5) 
        corr = Correlator([0.1 0.4; 0.9 0.6])                   # We will stay in Good State
        #c_noise = CorrelatedNoise(8, corr, [left, mid, right])

        @testset "Contract with CNOT left" begin
            lcps = contract_cps(corr, left, mid)
            newlcps = contract_cnot(lcps)
            c_noise = CorrelatedNoise(corr, [newlcps, mid, right])
            tps = contractCorrNoise(c_noise)
            @test tps.n_bits == 4

            tps2 = contractCorrNoise(CorrelatedNoise(corr, [left, mid, mid, right]))
            #println("No CNOT : ", BitStringProbState(tps2).prob)
            #println("")
            #println("1. CNOT Left : ", BitStringProbState(tps).prob)
            #println("")
        end

        @testset "Contract with CNOT mid" begin
            mcps = contract_cps(corr, mid, mid)
            newmcps = contract_cnot(mcps)
            c_noise = CorrelatedNoise(corr, [left, newmcps, right])
            tps = contractCorrNoise(c_noise)
            @test tps.n_bits == 4
            #println("2. CNOT mid : ", BitStringProbState(tps).prob)
            #println("")
        end

        @testset "Contract with CNOT right" begin
            rcps = contract_cps(corr, mid, right)
            newrcps = contract_cnot(rcps)
            c_noise = CorrelatedNoise(corr, [left, mid, newrcps])
            tps = contractCorrNoise(c_noise)
            @test tps.n_bits == 4
            #println("3. CNOT right : ", BitStringProbState(tps).prob)
            #println("")
        end

        @testset "Getindex" begin
            c = Correlator(0.1, 0.5)
            l = LeftCorrelatedProbState(0.2, 0.4)
            m = MidCorrelatedProbState(0.3, 0.5)
            r = RightCorrelatedProbState(0.1, 0.3)
            cn = CorrelatedNoise(8, c, l, m, r)

            @test cn[1] == l
            @test cn[6] == m

            lm = contract_cps(c,l,m)
            @test cn[1,2] ≈ lm
            lmmm = contract_cps(c, lm, contract_cps(c, m, m))
            @test cn[1,4] ≈ lmmm
            mmm = contract_cps(c, m, contract_cps(c, m, m))
            @test cn[3,5] ≈ mmm
            mmr = contract_cps(c, m, contract_cps(c, m, r))
            @test cn[6,8] ≈ mmr
            @test cn[6:8] ≈ mmr
            whole_chain = contract_cps(c, lm, contract_cps(c, mmm, mmr))
            @test cn[1, 8] ≈ whole_chain
            @test cn[1:8] ≈ whole_chain
            # Second test
            m1 = MidCorrelatedProbState(0.1, 0.3)
            m2 = MidCorrelatedProbState(0.3, 0.2)
            m3 = MidCorrelatedProbState(0.5, 0.6)
            cn = CorrelatedNoise(c, [l, m, m1, m2, m3, r])

            @test cn[3] == m1
            @test cn[5] == m3

            lm = contract_cps(c,l,m)
            @test cn[1,2] ≈ lm
            m1m2 = contract_cps(c, m1, m2)
            @test cn[3,4] ≈ m1m2
            m3r = contract_cps(c, m3, r)
            m2m3r = contract_cps(c, m2, m3r)
            @test cn[4,6] ≈ m2m3r
            @test cn[4:6] ≈ m2m3r
            whole_chain = contract_cps(c, lm, contract_cps(c, m1m2, m3r))
            @test cn[1,6] ≈ whole_chain
            @test cn[1:6] ≈ whole_chain
        end
    end

    @testset "CPS and BitState" begin
        cps = MidCorrelatedProbState(0.1, 0.9)

        @test_throws ErrorException BitState("e")*cps      # No open legs

        @test (BitState("_")*cps).prob ≈ cps.prob/sum(cps.prob)

        cpsmid = contract_cps(Correlator(0.3, 0.6), MidCorrelatedProbState(0.1, 0.3), MidCorrelatedProbState(0.4, 0.5))

        @test (BitState("__")*cpsmid).prob ≈ cpsmid.prob/sum(cpsmid.prob)

        cpsleft = contract_cps(Correlator(0.3, 0.6), LeftCorrelatedProbState(0.1, 0.3), MidCorrelatedProbState(0.4, 0.5))

        @test (BitState("__")*cpsleft).prob ≈ cpsleft.prob/sum(cpsleft.prob)

        cpsright = contract_cps(Correlator(0.3, 0.6), MidCorrelatedProbState(0.4, 0.5), RightCorrelatedProbState(0.1, 0.3))

        @test (BitState("__")*cpsright).prob ≈ cpsright.prob/sum(cpsright.prob)

        c = Correlator(0.1, 0.5)
        l = LeftCorrelatedProbState(0.1, 0.2)
        m1 = MidCorrelatedProbState(0.4, 0.2)
        m2 = MidCorrelatedProbState(0.3, 0.5)
        r = RightCorrelatedProbState(0.1, 0.4)

        tps = contract_cps(c, l, contract_cps(c, m1, contract_cps(c, m2, r)))

        # Apply e___ on tps
        result1 = BitState("e___")*tps
        lm1 = contract_cps(c, l, m1)
        blm1 = BitState("e_")*lm1
        @test contract_cps(c, blm1, contract_cps(c, m2, r)).prob ≈ result1.prob

        # Apply _0__ on tps
        result2 = BitState("_1__")*tps
        lm1 = contract_cps(c, l, m1)
        #println("lm1 = ", lm1.prob)
        blm1 = BitState("_1")*lm1
        #println(blm1)
        #println("blm1 = ", blm1.prob)
        @test contract_cps(c, blm1, contract_cps(c, m2, r)).prob ≈ result2.prob

        # Apply _0_0 ont tps
        result3 = BitState("_0_0")*tps
        #println(result3)
        lm1 = contract_cps(c,l,m1)
        blm1 = BitState("_0")*lm1
        m2r = contract_cps(c, m2, r)
        bm2r = BitState("_0")*m2r
        @test contract_cps(c, blm1, bm2r).prob ≈ result3.prob

        # Apply _0_1 ont tps
        result4 = BitState("_0_1")*tps
        #println(result4)
        lm1 = contract_cps(c,l,m1)
        blm1 = BitState("_0")*lm1
        m2r = contract_cps(c, m2, r)
        bm2r = BitState("_1")*m2r
        @test contract_cps(c, blm1, bm2r).prob ≈ result4.prob

        # Apply _1_0 ont tps
        result5 = BitState("_1_0")*tps
        #println(result5)
        lm1 = contract_cps(c,l,m1)
        blm1 = BitState("_1")*lm1
        m2r = contract_cps(c, m2, r)
        bm2r = BitState("_0")*m2r
        @test contract_cps(c, blm1, bm2r).prob ≈ result5.prob

        # Apply _1_1 ont tps
        result6 = BitState("_1_1")*tps
        #println(result6)
        lm1 = contract_cps(c,l,m1)
        blm1 = BitState("_1")*lm1
        m2r = contract_cps(c, m2, r)
        bm2r = BitState("_1")*m2r
        @test contract_cps(c, blm1, bm2r).prob ≈ result6.prob

        # Apply _0_ on tps
        tps = contract_cps(c, l, contract_cps(c, m1, r))
        result7 = BitState("_0_")*tps
        lm1 = contract_cps(c, l, m1)
        blm1 = BitState("_0")*lm1
        @test contract_cps(c, blm1, r).prob ≈ result7.prob
    end
end