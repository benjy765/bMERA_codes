include("../../Sources/States/states.jl")

using Base.Test

@testset "Transformation" begin
    @testset "Inner constructors" begin
            @testset ".. VectorTransformation" begin
                vperm = VectorTransformation([1,2,4,3])
                @test vperm.permutation == [1,2,4,3]    
                @test_throws ErrorException VectorTransformation([1,2,3,1])
                @test_throws ErrorException VectorTransformation([1,3,2])    
            end
            
            @testset ".. MatrixTransformation" begin
                mperm = MatrixTransformation(sparse([1 0 0 0; 0 1 0 0; 0 0 0 1; 0 0 1 0]))
                @test mperm.permutation == sparse([1 0 0 0; 0 1 0 0; 0 0 0 1; 0 0 1 0])
                @test_throws ErrorException MatrixTransformation(sparse([1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 1 0]))
                @test_throws ErrorException MatrixTransformation(sparse([1 0 0 ; 0 0 1 ; 0 1 0])) 
                @test_throws ErrorException MatrixTransformation(sparse([1 0 0 0 ; 0 1 0 0 ; 0 0 0 0 ; 0 0 0 0]))
                @test_throws ErrorException MatrixTransformation(sparse([1 0 0 ; 0 1 0 ; 0 0 1 ; 0 0 0]))
            end 
    end

    @testset "Inverse" begin
        @test inv(VectorTransformation([1,3,4,2])) == VectorTransformation([1,4,2,3])
        @test inv(MatrixTransformation([1 0 0 0; 0 0 1 0; 0 0 0 1; 0 1 0 0])) == MatrixTransformation([1 0 0 0; 0 0 0 1; 0 1 0 0; 0 0 1 0])
    end

    @testset "Outer constructors" begin
        @testset ".. VectorTransformation" begin
            smperm = MatrixTransformation(sparse([1 0 0 0; 0 1 0 0; 0 0 0 1; 0 0 1 0]))
            mperm = [1 0 0 0; 0 1 0 0; 0 0 0 1; 0 0 1 0]
            mperm2 = [0 1 0 0; 0 0 1 0; 1 0 0 0; 0 0 0 1]
            @test VectorTransformation(smperm).permutation == [1,2,4,3]
            @test VectorTransformation(mperm).permutation == [1,2,4,3]
            @test VectorTransformation(mperm2).permutation == [2,3,1,4]
         end 
         
         @testset ".. MatrixTransformation" begin
              vperm = VectorTransformation([1,2,4,3])
              mperm = [1 0 0 0; 0 1 0 0; 0 0 0 1; 0 0 1 0]
              vperm2 = VectorTransformation([2,3,1,4])
              perm = [2,3,1,4]
              @test MatrixTransformation(vperm).permutation == sparse([1 0 0 0; 0 1 0 0; 0 0 0 1; 0 0 1 0])
              @test MatrixTransformation(mperm).permutation == sparse([1 0 0 0; 0 1 0 0; 0 0 0 1; 0 0 1 0])
              @test MatrixTransformation(vperm2).permutation == sparse([0 1 0 0; 0 0 1 0; 1 0 0 0; 0 0 0 1])
              @test MatrixTransformation(perm).permutation == MatrixTransformation(vperm2).permutation
         end
    end
end