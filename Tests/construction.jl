include("../Sources/bmera_codes.jl")
using BMeraCodes
using Base.Test

id = elementary_gates["id_1"]
cnot = elementary_gates["cnot"]
ccnot = multi_cnot_gate!(elementary_gates, 2, 1)

@testset "Construction .." begin
    @testset ".. Polar codes" begin
        
        # 3 layers, 1 control, 1 target
        circ = polar_code(3, cnot)
        @test circ.n_bits == 8
        @test circ.n_layers == 3
        @test circ.branching == 2
        
        sl_1 = circ.layers[1].branching_layers[1].sublayers[1]
        sl_2_1 = circ.layers[2].branching_layers[1].sublayers[1]
        sl_2_2 = circ.layers[2].branching_layers[2].sublayers[1]
        sl_3_1 = circ.layers[3].branching_layers[1].sublayers[1]
        sl_3_2 = circ.layers[3].branching_layers[2].sublayers[1]
        sl_3_3 = circ.layers[3].branching_layers[3].sublayers[1]
        sl_3_4 = circ.layers[3].branching_layers[4].sublayers[1]

        @test sl_1 == SubLayer(8, [GatePosition(cnot,1,2), GatePosition(cnot,3,4), GatePosition(cnot,5,6), GatePosition(cnot,7,8)])
        @test sl_2_1 == SubLayer(4, [GatePosition(cnot,1,2), GatePosition(cnot,3,4)])
        @test sl_2_2 == SubLayer(4, [GatePosition(cnot,1,2), GatePosition(cnot,3,4)])
        @test sl_3_1 == SubLayer(2, [GatePosition(cnot,1,2)])
        @test sl_3_2 == SubLayer(2, [GatePosition(cnot,1,2)])
        @test sl_3_3 == SubLayer(2, [GatePosition(cnot,1,2)])
        @test sl_3_4 == SubLayer(2, [GatePosition(cnot,1,2)])

        # 2 layers, 2 control, 1 target
        circ = polar_code(2, ccnot)
        @test circ.n_bits == 9
        @test circ.n_layers == 2
        @test circ.branching == 3
        
        sl_1 = circ.layers[1].branching_layers[1].sublayers[1]
        sl_2_1 = circ.layers[2].branching_layers[1].sublayers[1]
        sl_2_2 = circ.layers[2].branching_layers[2].sublayers[1]
        sl_2_3 = circ.layers[2].branching_layers[3].sublayers[1]

        @test sl_1 == SubLayer(9, [GatePosition(ccnot,1,3), GatePosition(ccnot,4,6), GatePosition(ccnot,7,9)])
        @test sl_2_1 == SubLayer(3, [GatePosition(ccnot,1,3)])
        @test sl_2_2 == SubLayer(3, [GatePosition(ccnot,1,3)])
        @test sl_2_3 == SubLayer(3, [GatePosition(ccnot,1,3)])
    end

    @testset "Conv polar codes" begin
        # 3 layers, (2, cnot), (1, cnot), (2, cnot)
        circ = conv_polar_code(3, [cnot, cnot, cnot], [2, 1, 2])
        @test circ.n_bits == 8
        @test circ.n_layers == 3
        @test circ.branching == 2

        sl(i::Int,j::Int,k::Int) = circ.layers[i].branching_layers[j].sublayers[k]

        @test sl(1,1,1) == SubLayer(8, [GatePosition(id, 1), GatePosition(cnot, 2,3), GatePosition(cnot, 4,5), GatePosition(cnot, 6,7), GatePosition(id, 8)])
        @test sl(1,1,2) == SubLayer(8, [GatePosition(cnot, 1,2), GatePosition(cnot, 3,4), GatePosition(cnot, 5,6), GatePosition(cnot, 7,8)])
        @test sl(1,1,3) == SubLayer(8, [GatePosition(id, 1), GatePosition(cnot, 2,3), GatePosition(cnot, 4,5), GatePosition(cnot, 6,7), GatePosition(id,8)])
        
        @test sl(2,1,1) == SubLayer(4, [GatePosition(id, 1), GatePosition(cnot, 2,3), GatePosition(id, 4)])
        @test sl(2,1,2) == SubLayer(4, [GatePosition(cnot, 1,2), GatePosition(cnot, 3,4)])
        @test sl(2,1,3) == SubLayer(4, [GatePosition(id, 1), GatePosition(cnot, 2,3), GatePosition(id, 4)])
        
        @test sl(2,2,1) == SubLayer(4, [GatePosition(id, 1), GatePosition(cnot, 2,3), GatePosition(id, 4)])
        @test sl(2,2,2) == SubLayer(4, [GatePosition(cnot, 1,2), GatePosition(cnot, 3,4)])
        @test sl(2,2,3) == SubLayer(4, [GatePosition(id, 1), GatePosition(cnot, 2,3), GatePosition(id, 4)])

        @test sl(3,1,1) == SubLayer(2, [GatePosition(cnot, 1,2)])
        @test sl(3,2,1) == SubLayer(2, [GatePosition(cnot, 1,2)])
        @test sl(3,3,1) == SubLayer(2, [GatePosition(cnot, 1,2)])
        @test sl(3,4,1) == SubLayer(2, [GatePosition(cnot, 1,2)])

        # 2 layers, (1, ccnot), (3, cnot)
        circ = conv_polar_code(2, [ccnot, cnot], [1, 3])
        @test circ.n_bits == 9
        @test circ.n_layers == 2
        @test circ.branching == 3

        sl(i::Int,j::Int,k::Int) = circ.layers[i].branching_layers[j].sublayers[k]

        @test sl(1,1,1) == SubLayer(9, [GatePosition(ccnot, 1,3), GatePosition(ccnot, 4,6), GatePosition(ccnot, 7,9)])
        @test sl(1,1,2) == SubLayer(9, [GatePosition(id,1), GatePosition(id,2), GatePosition(cnot,3,4), GatePosition(id,5), GatePosition(cnot,6,7), GatePosition(id,8), GatePosition(id,9)])
        
        @test sl(2,1,1) == SubLayer(3, [GatePosition(ccnot, 1,3)])
        @test sl(2,2,1) == SubLayer(3, [GatePosition(ccnot, 1,3)])
        @test sl(2,3,1) == SubLayer(3, [GatePosition(ccnot, 1,3)]) 
    end

    @testset "Fix conv polar codes" begin
        # 3 layers, (1, cnot), (2, cnot), (1, cnot)
        circ = fix_conv_polar_code(3, [cnot, cnot, cnot], [1, 2, 1], 2)
        @test circ.n_bits == 8
        @test circ.n_layers == 3
        @test circ.branching == 2

        sl(i::Int,j::Int,k::Int) = circ.layers[i].branching_layers[j].sublayers[k]

        @test sl(1,1,1) == SubLayer(8, [GatePosition(cnot, 1,2), GatePosition(cnot, 3,4), GatePosition(cnot, 5,6), GatePosition(cnot, 7,8)])
        @test sl(1,1,2) == SubLayer(8, [GatePosition(id, 1), GatePosition(cnot, 2,3), GatePosition(cnot, 4,5), GatePosition(cnot, 6,7), GatePosition(id,8)])
        @test sl(1,1,3) == SubLayer(8, [GatePosition(cnot, 1,2), GatePosition(cnot, 3,4), GatePosition(cnot, 5,6), GatePosition(cnot, 7,8)])
                
        @test sl(2,1,1) == SubLayer(4, [GatePosition(cnot, 1,2), GatePosition(cnot, 3,4)])
        @test sl(2,1,2) == SubLayer(4, [GatePosition(id, 1), GatePosition(cnot, 2,3), GatePosition(id, 4)])
        @test sl(2,1,3) == SubLayer(4, [GatePosition(cnot, 1,2), GatePosition(cnot, 3,4)])
        
        @test sl(2,2,1) == SubLayer(4, [GatePosition(cnot, 1,2), GatePosition(cnot, 3,4)])
        @test sl(2,2,2) == SubLayer(4, [GatePosition(id, 1), GatePosition(cnot, 2,3), GatePosition(id, 4)])
        @test sl(2,2,3) == SubLayer(4, [GatePosition(cnot, 1,2), GatePosition(cnot, 3,4)])

        for i in 1:4
            @test sl(3,i,1) == SubLayer(2, [GatePosition(cnot, 1,2)])
            @test length(circ.layers[3].branching_layers[i]) == 2
        end

    end

end