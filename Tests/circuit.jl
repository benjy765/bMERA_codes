include("../Sources/bmera_codes.jl")
using BMeraCodes
using BMeraCodes: upward_permutation, downward_permutation, init_circuit_state!, link_causal_cone!, update_state!
using Base.Test

cnot = elementary_gates["cnot"]
id = elementary_gates["id_1"]

function init_pc_layer(level::Int, n_levels::Int)
    branch_length = 2^(n_levels-level+1)
    gates = [GatePosition(cnot,2*i-1,2*i) for i in 1:div(branch_length,2)]
    sl = SubLayer(branch_length, gates)
    bl = BranchingLayer([sl])
    Layer(bl, 2^(level-1))
end

function init_conv_pc_layer(level::Int, n_levels::Int)
    branch_length = 2^(n_levels-level+1)
    gates1 = [GatePosition(cnot,2*i-1,2*i) for i in 1:div(branch_length,2)]
    sl1 = SubLayer(branch_length, gates1)
    gates2 = vcat(GatePosition(id, 1), [GatePosition(cnot,2*i,2*i+1) for i in 1:(div(branch_length,2)-1)], GatePosition(id, branch_length))
    sl2 = SubLayer(branch_length, gates2)
    bl = BranchingLayer([sl2, sl1])
    Layer(bl, 2^(level-1))
end

@testset "Circuit" begin
    n_levels = 3    
    polar_layers = [init_pc_layer(i, n_levels) for i in 1:n_levels]
    bsps = BitStringProbState([0.1, 0.2, 0.3, 0.4])
    bottom_state = kron([bsps for i in 1:2^(n_levels - 1)]...)

    @testset ".. Constructor" begin
        pc = Circuit(2, bottom_state, polar_layers)
        @test pc.n_bits == 8
        @test pc.n_layers == 3

        wrong_layer = Layer(BranchingLayer([SubLayer(2, [GatePosition(cnot, 1, 2)])]), 1)
        @test_throws ErrorException Circuit(2, bottom_state, vcat(polar_layers[1:2], wrong_layer))  # Wrong number of bits in wrong_layer    
        @test_throws ErrorException Circuit(2, bottom_state, vcat(polar_layers[1:2], polar_layers[1])) # wrong scaling for # of bl
       
        another_layer = Layer(BranchingLayer([SubLayer(1, [GatePosition(id, 1)])]), 8)
        @test_throws ErrorException Circuit(2, bottom_state, vcat(polar_layers, another_layer)) # wrong number of layers
    end

    @testset ".. upward_permutation" begin
        pc = Circuit(2, bottom_state, polar_layers)

        @test_throws ErrorException upward_permutation(pc, 4) # Invalid bottom layer pos
        @test_throws ErrorException upward_permutation(pc, 2, 1, 9) # Invalid bits position

        @test upward_permutation(pc, 2) == [1, 3, 5, 7, 2, 4, 6, 8]
        @test upward_permutation(pc, 3, 1, 8) == [1, 3, 2, 4, 5, 7, 6, 8]
        @test upward_permutation(pc, 2, 5) == [2]
        @test upward_permutation(pc, 3, 3, 6) == [2, 4, 5, 7]
    end

    @testset ".. downward_permutation" begin
        pc = Circuit(2, bottom_state, polar_layers)

        @test_throws ErrorException downward_permutation(pc, 4) # Invalid bottom layer pos
        @test_throws ErrorException downward_permutation(pc, 2, 1, 9) # Invalid bits position

        @test downward_permutation(pc, 1) == [1, 5, 2, 6, 3, 7, 4, 8]
        @test downward_permutation(pc, 2, 1, 8) == [1, 3, 2, 4, 5, 7, 6, 8]
        @test downward_permutation(pc, 1, 5) == [3]
        @test downward_permutation(pc, 2, 3, 6) == [2, 4, 5, 7]
    end

    @testset ".. init_circuit_state" begin
        pc = Circuit(2, bottom_state, polar_layers)
        bs = BitState("eee__011")

        init_circuit_state!(pc, bs)
      
        @test get_state(pc) == bs
        @test get_state(pc, 2) == BitState("e__1e__0")
        @test get_state(pc, 3) == BitState("________")
      
        cc = top_cone(pc)
        @test cc.top_bit_state == BitState("e__0")
        @test cc.transform == kron(VectorTransformation([1, 2, 4, 3]), VectorTransformation([1, 2, 4, 3]))
    end

    @testset ".. link_causal_cone" begin
        pc = Circuit(2, bottom_state, polar_layers)
        bs = BitState("eee_0110")      
        init_circuit_state!(pc, bs)
      
        cc = top_cone(pc)
        @test !isdefined(cc, :childs)
        link_causal_cone!(pc, 3)
        @test isdefined(cc, :childs)
        @test cc.top_bit_state == BitState("e_")
        @test cc.transform == VectorTransformation([1, 2, 4, 3])

        childs = cc.childs
        @test childs[1].top_bit_state == BitState("e_")
        @test childs[1].transform == VectorTransformation([1, 2, 4, 3])
        @test childs[2].top_bit_state == BitState("e_")
        @test childs[2].transform == VectorTransformation([1, 2, 4, 3])

        grandchilds_left = childs[1].childs
        @test grandchilds_left[1].top_bit_state == BitState("_0")
        @test grandchilds_left[1].transform == VectorTransformation([1, 2, 4, 3])
        @test grandchilds_left[2].top_bit_state == BitState("_1")
        @test grandchilds_left[2].transform == VectorTransformation([1, 2, 4, 3])     

        grandchilds_right = childs[2].childs
        @test grandchilds_right[1].top_bit_state == BitState("_1")
        @test grandchilds_right[1].transform == VectorTransformation([1, 2, 4, 3])
        @test grandchilds_right[2].top_bit_state == BitState("_0")
        @test grandchilds_right[2].transform == VectorTransformation([1, 2, 4, 3])

        # testing link_causal_cone at start layer = 2
        pc = Circuit(2, bottom_state, polar_layers)
        bs = BitState("eee_0110")      
        init_circuit_state!(pc, bs)

        link_causal_cone!(pc, 1)
        cc = top_cone(pc)
        @test cc.top_bit_state == BitState("e_")
        @test cc.transform == VectorTransformation([1, 2, 4, 3])

        childs = cc.childs
        @test childs[1].top_bit_state == BitState("e_")
        @test childs[1].transform == VectorTransformation([1, 2, 4, 3])
        @test childs[2].top_bit_state == BitState("e_")
        @test childs[2].transform == VectorTransformation([1, 2, 4, 3])

        @test !isdefined(childs[1], :childs) 
        @test !isdefined(childs[2], :childs)
    end

    @testset ".. change state" begin
        pc = Circuit(2, bottom_state, polar_layers)
        bs = BitState("eeeeeee_")
        init_circuit_state!(pc, bs)

        change_state!(pc, BitState("_10"), 6, 8)
        @test get_state(pc) == BitState("eeeee_10")

        cc = top_cone(pc)
        @test cc.top_bit_state == BitState("e_")
    end

    @testset ".. polar code" begin

        n_levels = 3
        pc_layers = [init_pc_layer(i, n_levels) for i in 1:n_levels]
        zps = ZeroProbState([0.1, 0.1, 0.9, 0.9, 0.1, 0.9, 0.9, 0.9])
       
        pc = Circuit(2, zps, pc_layers)
        
        for i in 1:3  
            @test decode!(pc, i) == BitState("10100101")
            @test decode!(pc, i, [4, 7, 8]) == BitState("10100000")
        end
        @test decode!(pc, 3, [1, 4, 7, 8]) == BitState("00000000")
    end
    
    @testset ".. conv polar code" begin

        n_levels = 3
        conv_pc_layers = [init_conv_pc_layer(i, n_levels) for i in 1:n_levels]
        zps = ZeroProbState([0.1, 0.1, 0.9, 0.9, 0.1, 0.9, 0.9, 0.9])

        conv_pc = Circuit(2, zps, conv_pc_layers)

        for i in 1:3
            @test decode!(conv_pc, i) == BitState("10101111")          
        end
        @test decode!(conv_pc, 1, [4, 7, 8]) == BitState("11100100")
    end

end

