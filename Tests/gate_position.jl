include("../Sources/bmera_codes.jl")
using BMeraCodes
using Base.Test

@testset "Gate position .." begin

    id = elementary_gates["id_1"]
    not = elementary_gates["not"]
    cnot = elementary_gates["cnot"]

    @testset ".. constructor" begin
        @test_throws ErrorException GatePosition(cnot, 2, 2)
        @test_throws ErrorException GatePosition(cnot, 4, 1)
        @test_throws ErrorException GatePosition(cnot, 2, 7)
    end

    @testset ".. getindex" begin
        gp = GatePosition(cnot, 6, 7) 

        # Wrong length
        @test_throws ErrorException gp[BitState("0101")]
        @test_throws ErrorException gp[BitState("0")]

        @test gp[BitState("10")][1] == [GatePosition(id, 6, 6), GatePosition(not, 7, 7)]
        @test gp[BitState("10")][2] == BitState("11")

        @test gp[BitState("e_")][1] == [GatePosition(cnot, 6, 7)]
        @test gp[BitState("e_")][2] == BitState("__")
    end

    @testset ".. span" begin
        gates = [GatePosition(cnot, 1,2), GatePosition(cnot,4,5), GatePosition(id, 3)]
        @test span(gates) == [1,5]
        @test_throws ErrorException span(gates[1:2])
    end
end


