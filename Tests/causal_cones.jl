include("../Sources/bmera_codes.jl")
using BMeraCodes
using Base.Test

@testset "CausalCone" begin
    @testset ".. constructor" begin
        @testset ".. .. ErrorException in contructor" begin 
            # Parameter for the CausalCone
            cnot = VectorTransformation([1, 2, 4, 3])
            bsps = BitStringProbState([0.1, 0.2, 0.3, 0.4])
            bsps2 = BitStringProbState([0.5, 0.5])
            childs = [CausalCone(BitState("__"), 1, cnot, bsps), CausalCone(BitState("__"), 1, cnot, bsps)]
        
            # Contraction not compatible (# open legs output != # open legs childs)
            tbs = BitState("0_")
            @test_throws ErrorException CausalCone(tbs, 1, cnot, childs)
            @test_throws ErrorException CausalCone(tbs, 1, cnot, bsps2)

            # We must decode something. There must be at least one bit__ in top.        
            tbs = BitState("01")
            @test_throws ErrorException CausalCone(tbs, 1, cnot, childs)
            @test_throws ErrorException CausalCone(tbs, 1, cnot, bsps)
        end

        tbs = BitState("ee_0")
        transf = VectorTransformation([1, 2, 4, 3, 5, 6, 8, 7, 13, 14, 16, 15, 9, 10, 12, 11])

        @testset ".. .. contract if child is PS" begin
            bsps = BitStringProbState([0.075, 0.075, 0.05, 0.05, 0.025, 0.025, 0.065, 0.065, 0.1, 0.1, 0.035, 0.035, 0.1, 0.1, 0.05, 0.05])
            cone = CausalCone(tbs, 1, transf, bsps)
           
            @test cone.last_contracted_bit_state == BitState("ee_0")
            @test cone.last_contracted_prob_state == BitStringProbState([0.6, 0.4])
        end

        # Define childs and grandchilds
        cnot = VectorTransformation([1, 2, 4, 3])
        grandchild = CausalCone(BitState("__"), 1, cnot, BitStringProbState([0.1, 0.2, 0.3, 0.4]))
        childs = [grandchild, grandchild]

        cone = CausalCone(tbs, 1, transf, childs)
        @test !isdefined(cone, :last_contracted_bit_state)
        @test !isdefined(cone, :last_contracted_prob_state)
    end

    @testset ".. merge_childs" begin
        # Parameter for the CausalCone
        tbs = BitState("e__0")
        cnot = VectorTransformation([1, 2, 4, 3])
        bsps = BitStringProbState([0.1, 0.2, 0.3, 0.4])
        childs = [CausalCone(BitState("__"), 1, cnot, bsps), CausalCone(BitState("__"), 1, cnot, bsps)]
        last_contracted_ps = childs[1].last_contracted_prob_state
        transf = VectorTransformation([1, 2, 4, 3, 5, 6, 8, 7, 13, 14, 16, 15, 9, 10, 12, 11])

        cone = CausalCone(BitState("e__0"), 3, transf, childs)
        @test merge_childs(cone) ≈ permute(kron(last_contracted_ps, last_contracted_ps), [1,3,2,4])

        # Testing merging with a less trivial tree
        tbs = BitState("e_")
        bsps_left = BitStringProbState([0.5, 0, 0, 0.5])
        bsps_right = BitStringProbState([1/4, 1/4, 1/8, 3/8])
        left_child = CausalCone(BitState("e_"), 1, cnot, bsps_left)
        right_child = CausalCone(BitState("_0"), 1, cnot, bsps_right)
        childs = [left_child, right_child]

        cone = CausalCone(BitState("e_"), 1, cnot, childs)
        @test merge_childs(cone) ≈ kron(left_child.last_contracted_prob_state, right_child.last_contracted_prob_state)
    end
    
    @testset ".. contract" begin
        # Parameter for CausalCone
        cnot = VectorTransformation([1, 2, 4, 3])
        bsps = BitStringProbState([0.1, 0.2, 0.3, 0.4])
        childs = [CausalCone(BitState("__"), 1, cnot, bsps), CausalCone(BitState("__"), 1, cnot, bsps)]        
        tbs = BitState("e__0")
        transf = VectorTransformation([1, 2, 4, 3, 5, 6, 8, 7, 13, 14, 16, 15, 9, 10, 12, 11])
        
        cone = CausalCone(tbs, 1, transf, childs)
        @test !isdefined(cone, :last_contracted_bit_state)
        @test !isdefined(cone, :last_contracted_prob_state) 

        contract!(cone)
        @test isdefined(cone, :last_contracted_bit_state)
        @test isdefined(cone, :last_contracted_prob_state)

        # Testing the result of a contraction
        bsps_left = BitStringProbState([0.5, 0, 0, 0.5])
        bsps_right = BitStringProbState([1/4, 1/4, 1/8, 3/8])
        cnot = VectorTransformation([1, 2, 4, 3])
        left_child = CausalCone(BitState("e_"), 1, cnot, bsps_left)
        right_child = CausalCone(BitState("_0"), 1, cnot, bsps_right)
        childs = [left_child, right_child]

        cone = CausalCone(BitState("e_"), 1, cnot, childs)
        contract!(cone)
        @test cone.last_contracted_prob_state ≈ BitStringProbState([0.4, 0.6])
    end

    @testset ".. set_childs" begin
        # Starting with childs :< probstate
        cnot = VectorTransformation([1, 2, 4, 3])
        bsps = BitStringProbState([0.1, 0.2, 0.3, 0.4])

        cc = CausalCone(BitState("e_"), 1, cnot)
        set_childs!(cc, bsps)
        @test cc.childs == bsps

        # Testing with "real" childs!
        bsps_left = BitStringProbState([0.5, 0, 0, 0.5])
        bsps_right = BitStringProbState([1/4, 1/4, 1/8, 3/8])
        left_child = CausalCone(BitState("e_"), 1, cnot, bsps_left)
        right_child = CausalCone(BitState("_0"), 1, cnot, bsps_right)
        childs = [left_child, right_child]

        cone = CausalCone(BitState("e_"), 1, cnot)
        set_childs!(cone, childs)
        @test isdefined(cone, :childs)

        # Testing if childs does not have childs (contractible childs)
        childs = [CausalCone(BitState("__"), 1, cnot), CausalCone(BitState("__"), 1, cnot)]
        tbs = BitState("ee_0")
        transf = VectorTransformation([1, 2, 4, 3, 5, 6, 8, 7, 13, 14, 16, 15, 9, 10, 12, 11])

        cone = CausalCone(tbs, 1, transf)
        set_childs!(cone, childs)
        @test isdefined(cone, :childs) && !isdefined(cone.childs[1], :childs) && !isdefined(cone.childs[2], :childs)
    end
end

@testset "Causal cone with CPS" begin
    @testset ".. constructor" begin
        cnot = VectorTransformation([1, 2, 4, 3])
        cps = contract_cps(Correlator(0.3, 0.6), MidCorrelatedProbState(0.4, 0.5), MidCorrelatedProbState(0.4, 0.5))
        cone = CausalCone(BitState("__"), 1, cnot, cps)

        @test cone.last_contracted_bit_state == BitState("__")
        @test cone.last_contracted_prob_state == contract_cnot(BitState("__")*cps)

        cone = CausalCone(BitState("e_"), 1, cnot, cps)
        @test cone.last_contracted_bit_state == BitState("e_")
        @test cone.last_contracted_prob_state == BitState("e_")*contract_cnot(cps)
    end

    @testset ".. Merging CPS childs" begin
        corr = [0.5 0.5; 0.5 0.5]
        transf = VectorTransformation([1,2,4,3])
        cpsleft = contract_cps(Correlator(corr), LeftCorrelatedProbState(0.0, 0.5), MidCorrelatedProbState(0.0, 0.5))
        cpsright = contract_cps(Correlator(corr), MidCorrelatedProbState(0.0, 0.5), RightCorrelatedProbState(0.0, 0.5))
        childs = [CausalCone(BitState("e_"), 1, transf, cpsleft), CausalCone(BitState("e_"), 1, transf, cpsright)]  
        cone = CausalCone(BitState("__"), 1, transf, childs)

        #@test merge_cps_childs(cone, Correlator(corr)) ≈ TensorProbState([0.5625 0.1875; 0.1875 0.0625])
    end

    @testset ".. longer example" begin
        corr = [0.5 0.5; 0.5 0.5]
        transf = VectorTransformation([1,2,4,3])
        cpsleft = contract_cps(Correlator(corr), LeftCorrelatedProbState(0.0, 0.5), MidCorrelatedProbState(0.0, 0.5))
        cpsmid = contract_cps(Correlator(corr), MidCorrelatedProbState(0.0, 0.5), MidCorrelatedProbState(0.0, 0.5))
        cpsright = contract_cps(Correlator(corr), MidCorrelatedProbState(0.0, 0.5), RightCorrelatedProbState(0.0, 0.5))
        gchildsleft = [CausalCone(BitState("_0"), 1, transf, cpsleft), CausalCone(BitState("_0"), 1, transf, cpsmid)]
        gchildsright = [CausalCone(BitState("_1"), 1, transf, cpsmid), CausalCone(BitState("_0"), 1, transf, cpsright)]

        childs = [CausalCone(BitState("_0"), 1, transf, gchildsleft), CausalCone(BitState("_0"), 1, transf, gchildsright)]
        cone = CausalCone(BitState("_0"), 1, transf, childs)
        #println(contract!(cone, Correlator(corr)))
    end
end
