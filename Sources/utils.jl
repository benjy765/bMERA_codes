function layer_permutation(pos::Int, bottom_layer::Int, branching::Int)
    branches_length = branching^bottom_layer
    top_branches_length = branches_length * branching

    new_pos = ((pos - 1) .% branching) * branches_length 
    new_pos += div.(pos - 1, branching) + 1
    new_pos += div.( pos-1, top_branches_length) * top_branches_length
end

function position_to_coordinate(pos::Int, branches_length::Int)
    bl = div(pos - 1, branches_length) + 1
    bit = (pos - 1) % branches_length + 1
    (bl, bit)
end

function position_to_coordinate(pos::Vector{Int}, branches_length::Int)
    bl = zeros(Int, length(pos))
    bit = zeros(Int, length(pos))
    for (i, p) in enumerate(pos)
        bl[i], bit[i] = position_to_coordinate(p, branches_length)
    end
    (bl, bit)
end

coordinate_to_position(bl::Int, bit::Int, branches_length::Int) = (bl - 1) * branches_length + bit 

function coordinate_to_position(bl::Vector{Int}, bit::Vector{Int}, branches_length::Int)
    if length(bl) != length(bit)
        error("Both input must have the same length.")
    end

    pos = zeros(Int, length(bl))
    for (i, (a, b)) in enumerate(zip(bl, bit))
        pos[i] = coordinate_to_position(a, b, branches_length)
    end
    pos
end

argsort(a) = sortrows(hcat(a, 1:length(a)), by = x -> x[1])[:,2]

function sort_from_bl(bl::Vector{Int}, bit::Vector{Int}, state::BitState)
    inds = argsort(bl)
    (bl[inds], bit[inds], state[inds])
end

function split_states(state::BitState, pos::Vector{Int}, branches_length::Int)
    bl, bit = position_to_coordinate(pos, branches_length) 
    bl, bit, state = sort_from_bl(bl, bit, state)

    inds = vcat(0,find(diff(bl) .!= 0), length(bl))

    state = [state[inds[i]+1:inds[i+1]] for i in 1:(length(inds)-1)]
    bl = bl[inds[2:end]]
    first_bits = bit[inds[1:end-1]+1]
    last_bits = bit[inds[2:end]]
  
    (state, bl, first_bits, last_bits)
end
