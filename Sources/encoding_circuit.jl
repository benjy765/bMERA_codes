export EncodingCircuit, encode

"""
    EncodingCircuit <: Any
"""
mutable struct EncodingCircuit
    branching::Int
    n_layers::Int
    n_bits::Int
    sublayers::Vector{Vector{SubLayer}}
    perms::Vector{Vector{Int}}

    function EncodingCircuit(circ::Circuit)
        sublayers = [generate_sublayers(layer) for layer in circ.layers]
        perms = [generate_permutations(circ.branching, l, circ.n_layers) for l in 1:circ.n_layers]
        n_bits = circ.branching^circ.n_layers
        new(circ.branching, circ.n_layers, n_bits, sublayers, perms)
    end
end

function generate_sublayers(layer::Layer)
    sl = Vector{SubLayer}(depth(layer))        
    for i in 1:length(sl)
        temp_sl = Vector{SubLayer}([])
        for bl in layer.branching_layers
            if length(bl.sublayers) >= i
                push!(temp_sl, bl.sublayers[i])
            end
        end
        sl[i] = temp_sl[1]
        for sub in temp_sl[2:end]
            sl[i] = sl[i] + sub
        end
    end
    return sl
end

function generate_permutations(branching::Int, layer::Int, n_layers::Int)
    branches_length = branching^(n_layers - layer + 1)
    n_bits = branching^n_layers
    perms = vcat([collect(1:branching:branches_length) .+ x for x in 0:branching-1]...)
    vcat([perms .+ x for x in 0:branches_length:n_bits-1]...)
end

function encode(state::BitState, encoder::EncodingCircuit)
    if length(state) != encoder.n_bits 
        error("Number of input bits must match encoder size.")
    end
    for l in 1:encoder.n_layers
        for sl in encoder.sublayers[l]
            state = propagate(sl, state)[1]
        end
        state = state[encoder.perms[l]]
    end
    state
end
