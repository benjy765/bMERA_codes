export frozen_from_bhatta, frozen_from_random_output, frozen_from_mean_output, frozen_from_erasure
export hamming_distance, prob_first_error, frozen_from_correlation
"""
    frozen_from_bhatta(n_bits::Int, n_frozen::Int, prob::Float64)

Return a Vector{Int} containing all the frozen bits position using bhattacharyya bounds
for the bit flip channel.
"""
function frozen_from_bhatta(n_bits::Int, n_frozen::Int, prob::Float64)
    capacities = zeros(n_bits)
    capacities[1] = 2 * sqrt(prob * (1-prob))
    for i in 1:Int(log2(n_bits))
        for j in 2^(i-1):-1:1
            capacities[2*j] = 2 * capacities[j] - capacities[j]^2
            capacities[2*j-1] = capacities[j]^2
        end
    end
    sortperm(capacities)[1:n_frozen]
end

"""
    prob_first_error(circ::Circuit)

Return a Vector{Float64} where element `i` is the probability that the bit `i` 
is the first to be decoded wrongly with a width of 1.
"""
function prob_first_error(circ::Circuit, input_state::BitState = Bit[])
    if input_state == []
        input_state = zeros(Bit, circ.n_bits)
    end
    # Initial circuit state 
    state = fill(bit_e, circ.n_bits)
    last_open_leg = length(circ)
    state[last_open_leg] = bit__

    init_circuit_state!(circ, state)
    link_causal_cone!(circ)
    prob_zero = zeros(circ.n_bits)
    while last_open_leg > 1
        contract!(top_cone(circ))
        prob_zero[last_open_leg] = top_cone(circ).last_contracted_prob_state.prob[1]
        last_open_leg -= 1
        change_state!(circ, [bit__, input_state[last_open_leg+1]], last_open_leg, last_open_leg + 1)
        lpl = propagate!(circ)
        link_causal_cone!(circ, lpl) 
    end
    contract!(top_cone(circ))   
    prob_zero[1] = top_cone(circ).last_contracted_prob_state.prob[1]
    prob_error = zeros(circ.n_bits)
    for n in 1:circ.n_bits
        prob_error[n] = input_state[n] == bit_0 ? 1 - prob_zero[n] : prob_zero[n] 
    end
    prob_error
end

"""
    frozen_from_random_output(circ::Circuit, n_frozen::Int, prob::Float64, n_reps::Int)

Return a Vector{Int} containing all the frozen bits position using an average over randomly flip outputs.
"""
function frozen_from_random_output(circ::Circuit, n_frozen::Int, prob::Float64, n_reps::Int)
    prob_zero = zeros(circ.n_bits)
    for r in 1:n_reps
        zps = ZeroProbState((rand(length(circ)) .> prob) * (1 - 2 * prob) + prob)
        change_bottom_state!(circ, zps)
        prob_error += prob_first_error(circ)
    end
    prob_error ./= n_reps
    sortperm(prob_zero, rev=true)[1:n_frozen]
end

"""
    frozen_from_mean_ouput(circ::Circuit, n_frozen::Int, prob::Float64)

Return a Vector{Int} containing all the frozen bits position where all 
outputs are given by `(1 - 2 * p * (1 - p), 2 * p * (1 - p))`.
"""
function frozen_from_mean_output(circ::Circuit, n_frozen::Int, prob::Float64)
    expected_prob = 1 - 2 * prob * (1-prob)
    zps = ZeroProbState(fill(expected_prob, circ.n_bits))
    change_bottom_state!(circ, zps)
    sortperm(prob_first_error(circ), rev=true)[1:n_frozen]
end


"""
    frozen_from_erasure(circ::Circuit, n_frozen::Int, prob::Float64, n_reps::Int = 0)
"""
function frozen_from_erasure(circ::Circuit, n_frozen::Int, prob::Float64, n_reps::Int = 0)
    if n_reps == 0
        noise = ZeroProbState(fill(1-prob, circ.n_bits))
        change_bottom_state!(circ, noise)
        sortperm(prob_first_error(circ), rev=true)[1:n_frozen]
    elseif n_reps >= 1
        encoder = EncodingCircuit(circ)
        prob_error = zeros(circ.n_bits)
        for r in 1:n_reps 
            input_state = BitState(rand(circ.n_bits) .> 0.5)
            output_state = encode(input_state, encoder)
            prob_state = ZeroProbState(map(x -> x == bit_0 ? 1 - prob : prob, output_state))
            change_bottom_state!(circ, prob_state)
            prob_error += prob_first_error(circ::Circuit, input_state)
        end
        sortperm(prob_error)[end:-1:n_bits - n_frozen+1]
    else 
        error("Number of repetitions must be non negative.")
    end
end

"""
    hamming_distance(n_bits::Int, frozen1::Vector{Int}, frozen2::Vector{Int})

Return the distance between two vectors of frozen bits position.
"""
function hamming_distance(n_bits::Int, frozen1::Vector{Int}, frozen2::Vector{Int})
    v1 = zeros(Int, n_bits)
    v1[frozen1] = 1
    
    v2 = zeros(Int, n_bits)
    v2[frozen2] = 1

    sum(v1 .!= v2) / n_bits
end

"""
  frozen_from_erasure(circ::Circuit, n_frozen::Int, prob::Float64)

Return a set of frozen bits according to the erasure channel model. 
"""
#=
function frozen_from_erasure(circ::Circuit, n_frozen::Int, prob::Float64)
    zps = ZeroProbState(fill(1 - prob, circ.n_bits))
    change_bottom_state!(circ, zps)
    sortperm(prob_first_error(circ))[1:n_frozen]
end
=#

# """
#    frozen_from_correlation(circ::Circuit, n_frozen::Int)

# Return a set of frozen bits according to the correlated noise model. 
# """
# function frozen_from_correlation(circ::Circuit, n_frozen::Int)
#     sortperm(prob_first_error(circ))[1:n_frozen]
# end
