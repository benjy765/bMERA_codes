__precompile__()

module BMeraCodes
    
    import Base: getindex, setindex!
    import Base: ==, !=, ≈, +
    import Base: length, size, hash, show, isless, zero

    include("States/states.jl")
    include("correlated_noise.jl")
    include("gate.jl")
    include("causal_cones.jl")
    include("gate_position.jl")
    include("sublayers.jl")
    include("branching_layers.jl")
    include("layers.jl")
    include("utils.jl")
    include("circuit.jl")
    include("encoding_circuit.jl")
    include("frozen_bits.jl")
    include("construction.jl")
end

