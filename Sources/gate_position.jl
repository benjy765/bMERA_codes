export GatePosition, span

## Definition
"""
    GatePosition <: Any

Mutable type.

# Fields

- `gate::Gate`
- `first::Int`
- `last::Int`

Gate position define a `gate` with a relative position in the circuit starting to `first`
up to `last`. Obsviously, a valid GatePosition must contains gate with length equals to 
(last - first + 1). It is also possible to create empty gate position 
(useful to preallocate memory). In that case, default values for `first` and `last` are set to 0.

The main advantage of GatePosition over adding a position parameter to the Gate type, is that
by dissocing the position and the Gate, we can use the same Gate a lot of time only by 
refering to it in different GatePosition. This will allows more reuse of computation.

There are three ways to initialize a `GatePostion`:

    GatePosition() # Empty
    GatePosition(gate::Gate, first::Int, last::Int)
    GatePosition(gate::Gate, pos::Int)

# Examples

```julia-repl
# Empty GatePosition
julia> GatePosition()
GatePosition(#undef, 0, 0)

julia> cnot = elementary_gates["cnot"];

julia> gp = GatePosition(cnot, 7, 8)
GatePosition(2 bit(s) cnot gate, 7, 8)

# Wrong initialization since length(cnot) = 2.

julia> gp = GatePosition(cnot, 7, 9)
ERROR: Gate length must fit with first and last bits.
```
"""
mutable struct GatePosition
    gate::Gate
    first::Int
    last::Int

    function GatePosition()
        gp = new()
        gp.first = 0
        gp.last = 0
        gp
    end
    function GatePosition(gate::Gate, first::Int, last::Int)
        if length(gate) != (last - first + 1)
            error("Gate length must fit with first and last bits.")
        end
        new(gate, first, last)
    end
end

"""
    GatePosition(gate::Gate, pos::Int)

Outer constructor for GatePosition when only one bit is specify.
"""
GatePosition(gate::Gate, pos::Int) = GatePosition(gate, pos, pos)

## Comparison and hash. Need to be define for isequal(...) and unique(...)

function ==(gp1::GatePosition, gp2::GatePosition)
    (gp1.gate == gp2.gate) && (gp1.first == gp2.first)
end

isless(gp1::GatePosition, gp2::GatePosition) = (gp1.first < gp2.first)

hash(gp::GatePosition) = hash(gp.gate) + hash(gp.first) + hash(gp.last)

## Get index
"""
   getindex(gp::GatePosition, bs::BitState)

Return the simplify GatePosition for the input bitstate. 

```julia-repl
julia> cnot = elementary_gates["cnot"];
julia> gp = GatePosition(cnot, 6, 7);

julia> gp[BitState("10")]
(GatePosition[GatePosition(1 bit(s) identity gate, 6, 6), GatePosition(1 bit(s) cnot gate, 7, 7)], Bit[bit_1, bit_1])
```
"""
function getindex(gp::GatePosition, bs::BitState)
    if length(bs) != gp.gate.n_bits
        error("Length of BitState must be equal to the number of input of the gate.")
    end
    new_bs, pos, new_gates = gp.gate[bs]
    gate_range = collect(gp.first:gp.last) 
    new_gp = [GatePosition(new_gates[i], gate_range[minimum(pos[i])], gate_range[maximum(pos[i])]) for i in 1:length(pos)]
    (new_gp, new_bs)
end

# Return the span of a set of GatePosition (first and last bit). If the gates are not continuous,
# an error is thrown.

"""
    span(gates::Vector{GatePosition})

Return the span of a set of GatePosition (first and last bit). If the gates are not continuous, an error is thrown.

```julia-repl
julia> id, cnot = elementary_gates["id_1"], elementary_gates["cnot"];
julia> gates = [GatePosition(cnot, 1,2), GatePosition(cnot,4,5), GatePosition(id, 3)];

julia> span(gates)
2-element Array{Int64,1}:
 1
 5
```
"""
function span(gates::Vector{GatePosition})
    sorted_gates = sort(gates)
    first = sorted_gates[1].first
    last = sorted_gates[1].last
    for gate in sorted_gates[2:end]
        if gate.first != last + 1
            error("The gate are not continuous.") 
        end
        last = gate.last   
    end
    [first, last]
end
