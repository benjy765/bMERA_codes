export SubLayer, get_transform, propagate, simplify!

"""
    SubLayer <: Any

Mutable type.

# Fields 

- `n_bits::Int`
- `gates::Vector{GatePosition}`

Contain a Vector of GatePosition that must be ordered. Also, there must be one and only one
gate in `gates` for the `n_bits` bits, but each gate can span more than one bit.

# Examples

```julia-repl
# Sublayer containing two bits cnot.
julia> n_bits = 2;
julia> cnot = elementary_gates["cnot"];
julia> gates = [GatePosition(cnot, 1, 2)];
julia> sublayer = SubLayer(n_bits, gates)
SubLayer(2, GatePosition[GatePosition(2 bit(s) cnot gate, 1, 2), GatePosition(2 bit(s) cnot gate, 1, 2)])
```
"""
mutable struct SubLayer 
    n_bits::Int
    gates::Vector{GatePosition}

    function SubLayer(n_bits::Int, gates::Vector{GatePosition})
        if span(unique(gates)) != [1, n_bits]
            error("The span of the gates is not the same as the number of bits.")
        end
        if length(gates) == n_bits
            new(n_bits, gates)
        else
            sl_gates = fill(GatePosition(), n_bits)
            for gate in gates
                for pos in gate.first:gate.last
                    sl_gates[pos] = gate
                end
            end
            new(n_bits, sl_gates)
        end
    end
end


## Comparison

function ==(sl1::SubLayer, sl2::SubLayer)
    if length(sl1) != length(sl2)
        return false
    else
        for i in 1:length(sl1)
            if sl1[i] != sl2[i]
                return false
            end
        end
        return true
    end
end

## getindex
"""
    getindex(sl::SubLayer, ind::Int)

Returns the gate position corresponding to the position ind in the array.
"""
getindex(sl::SubLayer, ind::Int) = sl.gates[ind]

"""
    getindex(sl::SubLayer, inds...)

Returns an array containing only the unique gates elements corresponding to the positions
ind... in the array. 
"""
getindex(sl::SubLayer, inds...) = unique(sl.gates[inds...])

## Utils
"""
    length(sl::SubLayer)

Returns the number of bits for the sublayer.
"""
length(sl::SubLayer) = sl.n_bits

## Propagation and simplification

"""
    propagate(sl::SubLayer, input_bs::BitState, first_bit::Int, last_bit::Int)

Propagates input bitstate through a sublayer starting from first_bit to last_bit 
and output the resulting bitstate with his span.

    propagate(sl::SubLayer, input_bs::BitState)

Propagate for the whole sublayer.

    propagate(sl::SubLayer, input_bs::BitState, position::Int)

Propagate for one bit at a given position.

# Examples
```julia-repl
julia> not = elementary_gates["not"];
julia> cnot = elementary_gates["cnot"];
julia> sl = SubLayer(5, [GatePosition(cnot, 1, 2), GatePosition(cnot, 3, 4), GatePosition(not, 5)]);

julia> propagate(sl, BitState("e_010"), 1, 2)
(Bit[bit__, bit__], [1, 2])

julia> propagate(sl, BitState("eee_0"), 5)
(Bit[bit_1], [5, 5])

julia> propagate(sl, BitState("eee_0"))
(Bit[bit_e, bit_e, bit__, bit__, bit_1], [1, 5])

# Error
julia> propagate(sl, BitState("01"), 1)
ERROR: Bit State must be the same length as sublayer.
```
"""
function propagate(sl::SubLayer, input_bs::BitState, first_bit::Int, last_bit::Int)
    if length(input_bs) != length(sl)
        error("Bit State must be the same length as sublayer.")
    end
    if last_bit < first_bit || last_bit > length(sl) || first_bit < 1
        error("First or last bit is invalid.")
    end 
    
    gates = sl[first_bit:last_bit]
    output_bs = [gate[input_bs[gate.first:gate.last]][2] for gate in gates]
    (vcat(output_bs...), span(gates))
end

propagate(sl::SubLayer, input_bs::BitState) = propagate(sl, input_bs::BitState, 1, length(sl))

propagate(sl::SubLayer, input_bs::BitState, position::Int) = 
    propagate(sl, input_bs, position, position)

"""
    simplify!(sl::SubLayer, input_bs::BitState, first_bit::Int, last_bit::Int)

Simplify the gates in the sublayer starting for first_bit to last_bit and output 
the simplify array of gate position. 

    simplify!(sl::SubLayer, input_bs::BitState)

Simplify the whole sublayer.

    simplify!(sl::SubLayer, input_bs::BitState, position::Int)

Simplify one bit at the position given by `position.

# Example
```julia-repl
julia> not = elementary_gates["not"];
julia> cnot = elementary_gates["cnot"];
julia> sl = SubLayer(5, [GatePosition(cnot, 1, 2), GatePosition(cnot, 3, 4), GatePosition(not, 5)]);

julia> simplify!(sl, BitState("eeee_"))
julia> sl.gates
5-element Array{GatePosition,1}:
 GatePosition(1 bit(s) identity gate, 1, 1)
 GatePosition(1 bit(s) identity gate, 2, 2)
 GatePosition(1 bit(s) identity gate, 3, 3)
 GatePosition(1 bit(s) identity gate, 4, 4)
 GatePosition(1 bit(s) cnot gate, 5, 5)  
```
""" 
function simplify!(sl::SubLayer, input_bs::BitState, first_bit::Int, last_bit::Int)
    if length(input_bs) != length(sl)
        error("Bit State must be the same length as sublayer.")
    end
    if last_bit < first_bit || last_bit > length(sl) || first_bit < 1
        error("First or last bit is invalid.")
    end 

    gates = sl[first_bit:last_bit]
    for gate in gates
        new_gates = gate[input_bs[gate.first:gate.last]][1]
        for new_gate in new_gates
            for pos in new_gate.first:new_gate.last
                sl.gates[pos] = new_gate
            end
        end
    end
end

simplify!(sl::SubLayer, input_bs::BitState) = simplify!(sl, input_bs::BitState, 1, length(sl))

simplify!(sl::SubLayer, input_bs::BitState, position::Int) = 
    simplify!(sl, input_bs, position, position)

"""
    get_transform(sl::SubLayer, first_bit::Int, last_bit::Int)

Compute the resulting transformation as a VectorTransformation starting from first_bit to last_bit.
"""
function get_transform(sl::SubLayer, first_bit::Int, last_bit::Int)
    if last_bit < first_bit || last_bit > length(sl) || first_bit < 1
        error("First or last bit is invalid.")
    end
    transform = VectorTransformation([1])
    gates = sl[first_bit:last_bit]
    positions = fill([], length(gates))
    for (i, gate) in enumerate(gates)
        transform = kron(transform, gate.gate.transform)
    end
    (transform, span(gates))
end

get_transform(sl::SubLayer, position::Int) = get_transform(sl, position, position)
get_transform(sl::SubLayer) = get_transform(sl, 1, length(sl))

"""
   +(left_sl::SubLayer, right_sl::SubLayer)

Concatenation of two SubLayer. 
"""
function +(left_sl::SubLayer, right_sl::SubLayer)
    offset = left_sl.n_bits
    gates_right = [GatePosition(g.gate,g.first+offset, g.last+offset) for g in right_sl.gates]
    SubLayer(left_sl.n_bits + right_sl.n_bits, vcat(left_sl.gates, gates_right))
end




