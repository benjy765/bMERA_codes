export BranchingLayer, set_state!, change_state!, get_transform
export propagate, simplify!, simplify_and_propagate!

"""
    BranchingLayer <: Any
    
Mutable type.

# Fields 

- `n_bits::Int`
- `sublayers::Vector{SubLayer}`
- `state::BitState`
- `cone::CausalCone`

A vector of sublayers for n_bits with possibly a causal cone and a state. There are two ways to initialize a `BranchingLayer`:

    BranchingLayer(first_open_leg::Int, sublayers::Vector{SubLayer})
    BranchingLayer(sublayers::Vector{SubLayer})

When `first_open_leg` is specify, a `CausalCone` is initialize automatically.

# Examples

```julia-repl
# Initialization of the branching layer.
julia> id, not, cnot = elementary_gates["id_1"], elementary_gates["not"], elementary_gates["cnot"];
julia> n_bits = 8;
julia> sub1 = SubLayer(8, [GatePosition(cnot, 2*i - 1, 2*i) for i in 1:4]);
julia> sub2 = SubLayer(8, vcat(GatePosition(id, 1), [GatePosition(cnot, 2*i, 2*i + 1) for i in 1:3], GatePosition(id, 8)));
julia> sublayers = [sub1, sub2];
julia> bl = BranchingLayer(n_bits, sublayers);

julia> bl.state
8-element Array{Bit,1}:
 bit_e
 bit_e
 bit_e
 bit_e
 bit_e
 bit_e
 bit_e
 bit__

julia> bl.cone
Top bit state : "ee_"
First bit position : 6
Transformation : [1, 2, 4, 3, 8, 7, 5, 6]
No child.
```
"""
mutable struct BranchingLayer
    n_bits::Int
    sublayers::Vector{SubLayer}
    state::BitState
    cone::CausalCone

    function BranchingLayer(first_open_leg::Int, sublayers::Vector{SubLayer})
        n_bits = length(sublayers[1])
        if !(1 <= first_open_leg <= n_bits)
            error("Invalid first open leg value.")
        end
        for sl in sublayers[2:end]
            if length(sl) != n_bits
                error("The size of every sublayers must be the same.")
            end
        end
        
        initial_state = fill(bit_e, n_bits)
        initial_state[first_open_leg:end] = bit__

        bl = new(n_bits, sublayers, initial_state)
        _init_causal_cone(bl, first_open_leg, n_bits)
        bl
    end

    function BranchingLayer(sublayers::Vector{SubLayer})
        n_bits = length(sublayers[1])
        for sl in sublayers[2:end]
            if length(sl) != n_bits
                error("The size of every sublayers must be the same.")
            end
        end
        new(n_bits, sublayers)
    end
end

function _init_causal_cone(bl::BranchingLayer, first_open_leg::Int, last_open_leg::Int)
    open_legs = (first_open_leg, last_open_leg)
    previous_open_legs = (first_open_leg, last_open_leg)

    transform = identity_transform(open_legs[2] - open_legs[1] + 1)
    state = copy(bl.state)

    for sl in bl.sublayers
        propagated_state, open_legs = propagate(sl, state, open_legs...)
        state[open_legs[1]:open_legs[2]] = propagated_state
        
        left_identity_fill = identity_transform(previous_open_legs[1] - open_legs[1])
        right_identity_fill = identity_transform(open_legs[2] - previous_open_legs[2])
        
        transform = kron(left_identity_fill, transform, right_identity_fill)
        transform = get_transform(sl, open_legs[1], open_legs[2])[1] * transform
        
        previous_open_legs = open_legs   
    end
    bl.cone = CausalCone(view(bl.state, open_legs[1]:open_legs[2]), open_legs[1], transform)
end

## Set and change state of branching layer
"""
    set_state!(bl::BranchingLayer, bs::BitState)

Set a state for a branching layer and initialize the causal cone.

# Examples

```julia-repl
julia> cnot = elementary_gates["cnot"];
julia> sub = SubLayer(8, [GatePosition(cnot, 2*i - 1, 2*i) for i in 1:4]);
julia> bl = BranchingLayer([sub]);

julia> bl.state
ERROR: UndefRefError: access to undefined reference

julia> bl.cone
ERROR: UndefRefError: access to undefined reference

julia> set_state!(bl, BitState("eeeeeee_"));

julia> bl.state
8-element Array{Bit,1}:
 bit_e
 bit_e
 bit_e
 bit_e
 bit_e
 bit_e
 bit_e
 bit__

julia> bl.cone
Top bit state : "e_"
First bit position : 7
Transformation : [1, 2, 4, 3]
No child.
```
"""
function set_state!(bl::BranchingLayer, bs::BitState)
    if length(bs) != length(bl)
        error("The length of the Branching Layer and the state must be the same.")
    end
    open_legs = find(bs .== bit__)
    if length(open_legs) == 0
        error("There must be at least one bit to decode.")
    end
    if length(find(diff(open_legs) .!= 1)) > 0 
        error("All open legs must be adjacent.")
    end
    bl.state = bs
    _init_causal_cone(bl, open_legs[1], open_legs[end])
end

"""
    change_state!(bl::BranchingLayer, bs::BitState, first_bit::Int, last_bit::Int)

Change the bitstate at the top of the branching layer starting from first_bit up to last_bit and initialize the causal cone if needed.
"""
function change_state!(bl::BranchingLayer, bs::BitState, first_bit::Int, last_bit::Int)
    if last_bit < first_bit || last_bit > length(bl) || first_bit < 1
        error("First or last bit is invalid.")
    end
    
    if length(bs) != last_bit - first_bit + 1 
        error("The length of the new state is incompatible with first and last bit.")
    end

    cone_previous_state = copy(bl.cone.top_bit_state)
    previous_legs_position = find(bl.state .== bit__)

    tmp_state = copy(bl.state)
    tmp_state[first_bit:last_bit] = bs # Use this state to verify!

    new_legs_position = find(tmp_state .== bit__)

    if length(new_legs_position) == 0
        error("There must be at least one bit to decode.")
    end

    if length(find(diff(new_legs_position) .!= 1)) > 0 
        error("All open legs must be adjacent.")
    end

    bl.state[first_bit:last_bit] = bs

    if bl.cone.top_bit_state != cone_previous_state || previous_legs_position != new_legs_position
        _init_causal_cone(bl, new_legs_position[1], new_legs_position[end])
    end
end

"""
    change_state!(bl::BranchingLayer, bs::BitState)

Change state for the whole branching layer.
"""
change_state!(bl::BranchingLayer, bs::BitState) = change_state!(bl, bs, 1, length(bl))

"""
    change_state!(bl::BranchingLayer, bs::BitState, position::Int)

Change state for one bit position.

# Examples

Consider the same branching layer as in the examples for `BranchingLayer`.
```julia-repl
julia> change_state!(bl, BitState("_1"), 7, 8);

julia> bl.state
8-element Array{Bit,1}:
 bit_e
 bit_e
 bit_e
 bit_e
 bit_e
 bit_e
 bit__
 bit_1

julia> bl.cone
Top bit state : "e_1"
First bit position : 6
Transformation : [1, 2, 4, 3, 8, 7, 5, 6]
No child.
```
"""
change_state!(bl::BranchingLayer, bs::BitState, position::Int) = 
    change_state!(bl, bs, position, position)

## Length

length(bl::BranchingLayer) = bl.n_bits

## Propagate and simplify
"""
    propagate(bl::BranchingLayer, first_bit::Int, last_bit::Int)

Compute the output state at the bottom of the branching layer starting from first_bit up to last_bit. 
"""
function propagate(bl::BranchingLayer, first_bit::Int, last_bit::Int)
    if last_bit < first_bit || last_bit > length(bl) || first_bit < 1
        error("First or last bit is invalid.")
    end
    state = copy(bl.state)
    open_legs = (first_bit, last_bit)
    for sl in bl.sublayers
        propagated_state, open_legs = propagate(sl, state, open_legs...)
        state[open_legs[1]:open_legs[2]] = propagated_state
    end
    (state[open_legs[1]:open_legs[2]], open_legs)
end

"""
    propagate(bl::BranchingLayer, position::Int)

Propagate for one bit.
"""
propagate(bl::BranchingLayer, position::Int) = propagate(bl, position, position)

"""
    propagate(bl::BranchingLayer)

Propagate for the whole branching layer.

# Examples

Consider the same branching layer as in the examples for `BranchingLayer`.
```julia-repl
julia> propagate(bl, 8)
(Bit[bit__, bit__, bit__], [6, 8])
```
"""
propagate(bl::BranchingLayer) = propagate(bl, 1, length(bl))

"""
    simplify!(bl::BranchingLayer, first_bit::Int, last_bit::Int)

Simplify the branching layer starting from first_bit up to last_bit according to the input bitstate resulting in a change in the sublayers.
"""
function simplify!(bl::BranchingLayer, first_bit::Int, last_bit::Int)
    if last_bit < first_bit || last_bit > length(bl) || first_bit < 1
        error("First or last bit is invalid.")
    end
    state = copy(bl.state)
    open_legs = (first_bit, last_bit)
    for sl in bl.sublayers
        simplify!(sl, state, open_legs...)
        propagated_state, open_legs = propagate(sl, state, open_legs...)
        state[open_legs[1]:open_legs[2]] = propagated_state
    end
end

"""
    simplify!(bl::BranchingLayer, position::Int) 

Simplify for one bit.
"""
simplify!(bl::BranchingLayer, position::Int) = simplify!(bl, position, position)

"""
    simplify!(bl::BranchingLayer)

Simplify for the whole branching layer.

# Examples

Consider the same branching layer as in the examples for `BranchingLayer`.
```julia-repl
julia> id, not, cnot = elementary_gates["id_1"], elementary_gates["not"], elementary_gates["cnot"];
julia> n_bits = 4;
julia> sub1 = SubLayer(4, [GatePosition(cnot, 1, 2), GatePosition(cnot, 3,4)]);
julia> sub2 = SubLayer(4, [GatePosition(id, 1), GatePosition(cnot, 2,3), GatePosition(id, 4)]);
julia> sublayers = [sub1, sub2];
julia> bl = BranchingLayer(n_bits, sublayers);

julia> simplify!(bl)

julia> bl.sublayers[1]
SubLayer(4, GatePosition[GatePosition(1 bit(s) identity gate, 1, 1), GatePosition(1 bit(s) identity gate, 2, 2), 
GatePosition(2 bit(s) cnot gate, 3, 4), GatePosition(2 bit(s) cnot gate, 3, 4)])

julia> bl.sublayers[2]
SubLayer(4, GatePosition[GatePosition(1 bit(s) identity gate, 1, 1), GatePosition(2 bit(s) cnot gate, 2, 3), 
GatePosition(2 bit(s) cnot gate, 2, 3), GatePosition(1 bit(s) identity gate, 4, 4)])
```
"""
simplify!(bl::BranchingLayer) = simplify!(bl, 1, length(bl))

"""
    simplify_and_propagate!(bl::BranchingLayer, first_bit::Int, last_bit::Int)

Simplify and propagate through the branching layer starting from first_bit up to last_bit.
"""
function simplify_and_propagate!(bl::BranchingLayer, first_bit::Int, last_bit::Int)
    if last_bit < first_bit || last_bit > length(bl) || first_bit < 1
        error("First or last bit is invalid.")
    end
    state = copy(bl.state)
    open_legs = (first_bit, last_bit)
    for sl in bl.sublayers
        simplify!(sl, state, open_legs...)
        propagated_state, open_legs = propagate(sl, state, open_legs...)
        state[open_legs[1]:open_legs[2]] = propagated_state
    end
    (state[open_legs[1]:open_legs[2]], open_legs) 
end

"""
    simplify_and_propagate!(bl::BranchingLayer, position::Int)

Simplify and propagate for one bit.
"""
simplify_and_propagate!(bl::BranchingLayer, position::Int) = simplify_and_propagate!(bl, position, position)

"""
    simplify_and_propagate!(bl::BranchingLayer)

Simplify and propagate for the whole branching layer. 

# Examples

See the examples for `propagate` and `simplify!`.
"""
simplify_and_propagate!(bl::BranchingLayer) = simplify_and_propagate!(bl, 1, length(bl))

"""
    get_transform(bl::BranchingLayer)

Return the global transformation of `bl`.
"""
function get_transform(bl::BranchingLayer)
    transform = identity_transform(length(bl))
    for (i, sl) in enumerate(bl.sublayers[end:-1:1  ])
        transform *= get_transform(sl)[1]
    end
    transform
end 




