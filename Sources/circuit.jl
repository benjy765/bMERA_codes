export Circuit, decode!, encode!
export top_cone, get_state, change_state!, change_bottom_state!, get_transform

"""
    Circuit <: Any

Mutable type.

# Fields 

- `n_bits::Int`
- `n_layers::Int`
- `branching::Int`
- `bottom_state::ProbState`
- `layers::Vector{Layer}`

A structure that contain a Vector of Layer that represent the whole circuit. `branching`
is needed to compute all permutation between layers. `bottom_state` is the ProbState to decode.
The constructor is 

    Circuit(branching::Int, bottom_state::ProbState, layers::Vector{Layer})

# Example 

```julia-repl
julia> cnot = elementary_gates["cnot"];
julia> # Create the branching layers
julia> bl1 = BranchingLayer([SubLayer(4, [GatePosition(cnot, 1, 2), GatePosition(cnot, 3, 4)])]);
julia> bl2 = BranchingLayer([SubLayer(2, [GatePosition(cnot, 1, 2)])]);
julia> # Init the circuit
julia> Circuit(2, ZeroProbState([0.1, 0.1, 0.9, 0.1]), [Layer(bl1, 1), Layer(bl2, 2)])
Number of bits : 4
Number of layers : 2
Branching : 2
Bottom state : BMeraCodes.ZeroProbState([0.1, 0.1, 0.9, 0.1], 4)
```
"""
mutable struct Circuit 
    # Dimension of the circuit
    n_bits::Int
    n_layers::Int
    branching::Int

    # Circuit state
    bottom_state::ProbState
    layers::Vector{Layer}

    function Circuit(branching::Int, bottom_state::ProbState, layers::Vector{Layer})
        n_bits = length(bottom_state)
        n_layers = length(layers)
        for (i,l) in enumerate(layers)
            if length(l) != n_bits
                error("The number of bits in all layers must be the same as the bottom state.")
            end
            if length(l.branching_layers) != n_bits / branching^(n_layers-i+1)
                error("The number of branching layers on each layer must scale exponentialy.")
            end
        end
        if branching^n_layers != n_bits
            error("The circuit must have the following property : branching^n_layers == n_bits.")
        end
        new(n_bits, n_layers, branching, bottom_state, layers)
    end
end

Circuit(branching::Int, layers::Vector{Layer}) = 
    Circuit(branching, ZeroProbState(ones(branching^length(layers))), layers)

size(circ::Circuit) = (circ.n_bits, circ.n_layers, circ.branching)
length(circ::Circuit) = circ.n_bits

"""
    get_state(circ::Circuit, layer::Int = 1)

Return the state of `circ.layers[layer]` if define.
"""
get_state(circ::Circuit, layer::Int = 1) = get_state(circ.layers[layer])

"""
    top_cone(circ::Circuit)

Return the CausalCone of the first layer of `circ` if define.
"""
top_cone(circ::Circuit) = circ.layers[1].branching_layers[1].cone

"""
    change_state!(circ::Circuit, state::BitState, first_bit::Int, last_bit::Int)

Change the state of the first layer of `circ` between `first_bit` and `last_bit` to `state`.
The overall state must still have at least one open leg and all open_legs must be adjacent.
"""
change_state!(circ::Circuit, state::BitState, first_bit::Int, last_bit::Int) = 
    change_state!(circ.layers[1].branching_layers[1], state, first_bit, last_bit)

change_state!(circ::Circuit, state::BitState) = change_state!(circ, state, 1, length(circ))
change_state!(circ::Circuit, state::BitState, position::Int) =
    change_state!(circ, state, position, position)

"""
    change_bottom_state!(circ::Circuit, bottom_state::ProbState)

Change the ProbState at the bottom of `circ`. The number of bits must be the same. 
"""
function change_bottom_state!(circ::Circuit, bottom_state::ProbState)
    if length(bottom_state) != length(circ)
        error("Number of bits must be the same.")
    end
    circ.bottom_state = bottom_state
end

"""
    update_state!(circ::Circuit, last_open_leg::Int, width::Int, frozen_bits::Vector{Int})

Update the state considering the `frozen_bits`.
"""
function update_state!(circ::Circuit, last_open_leg::Int, width::Int, frozen_bits::Vector{Int})
    last_ind = last_open_leg + width
    
    n_frozen = 0
    while last_open_leg in frozen_bits && last_open_leg > 1
        last_open_leg -= 1
        n_frozen += 1
    end

    if last_open_leg == 1
        n_open_legs = 1
    else
        n_open_legs = last_open_leg < width ? last_open_leg : width
    end
    first_ind = last_open_leg+1 > width ? last_open_leg-width+1 : 1

    updated_state = update_with_frozen(circ, last_ind, last_open_leg, n_open_legs, n_frozen, frozen_bits)
    
    change_state!(circ, updated_state, first_ind, last_ind)
    last_open_leg
end

# Utilitary function for updated_state
function update_with_frozen(circ::Circuit, last_ind::Int, last_open_leg::Int,
                            n_open_legs::Int, n_frozen::Int, frozen_bits::Vector{Int})
    open_legs = fill(bit__, n_open_legs)
    frozen = fill(bit_0, n_frozen)

    updated_state = fill(bit__, last_ind - last_open_leg - n_frozen)
    for (i,b) in enumerate(last_open_leg+n_frozen+1:last_ind)
        if b in frozen_bits
            updated_state[i] = bit_0
        end
    end
    last_contraction = BitState(top_cone(circ).last_contracted_prob_state * updated_state)
    updated_state[updated_state .== bit__] = last_contraction

    vcat(open_legs, frozen, updated_state)
end

## Layer permutation

function downward_permutation(circ::Circuit, top_layer::Int, first_bit::Int, last_bit::Int)
    if !(1 <= top_layer < circ.n_layers)
        error("Invalid top layer position.")
    end

    if last_bit < first_bit || last_bit > length(circ) || first_bit < 1
        error("First or last bit is invalid.")
    end

    branches_length = circ.branching^(circ.n_layers - top_layer)
    top_branches_length = branches_length * circ.branching
    n_branches = Int(circ.n_bits / branches_length)

    perm = (collect(first_bit:last_bit) - 1) .% top_branches_length + 1
    perm = ((perm - 1) .% circ.branching) * branches_length + div.(perm - 1, circ.branching) + 1
    pad = div.( collect(first_bit:last_bit)-1, top_branches_length) * top_branches_length
    perm + pad
end

downward_permutation(circ::Circuit, top_layer::Int) = 
    downward_permutation(circ, top_layer, 1, length(circ))

downward_permutation(circ::Circuit, top_layer::Int, position::Int) =
    downward_permutation(circ, top_layer, position, position)

function upward_permutation(circ::Circuit, bottom_layer::Int, first_bit::Int, last_bit::Int)
    if !(1 < bottom_layer <= circ.n_layers)
        error("Invalid top layer position.")
    end

    if last_bit < first_bit || last_bit > length(circ) || first_bit < 1
        error("First or last bit is invalid.")
    end

    branches_length = circ.branching^(circ.n_layers - bottom_layer + 1)
    top_branches_length = branches_length * circ.branching
    n_branches = Int(circ.n_bits / branches_length)

    perm = (collect(first_bit:last_bit) - 1) .% top_branches_length + 1
    perm = ((perm - 1) .% branches_length) * circ.branching + div.(perm - 1, branches_length) + 1
    pad = div.( collect(first_bit:last_bit)-1, top_branches_length) * top_branches_length
    perm + pad
end

upward_permutation(circ::Circuit, top_layer::Int) = 
    upward_permutation(circ, top_layer, 1, length(circ))

upward_permutation(circ::Circuit, top_layer::Int, position::Int) =
    upward_permutation(circ, top_layer, position, position)

function init_circuit_state!(circ::Circuit, state::BitState)
    new_state = copy(state)
    for (i, layer) in enumerate(circ.layers[1:end-1])
        # Propagate state
        set_state!(layer, new_state)
        new_state = propagate(layer)[1]

        # Permute state
        permutation = upward_permutation(circ, i+1)
        new_state = new_state[permutation]
    end
    set_state!(circ.layers[end], new_state)
end

function link_causal_cone!(circ::Circuit, start_layer::Int)
    if !(1 <= start_layer <= circ.n_layers)
        error("Invalid start layer position.")
    end
    isCorrelatedNoise = typeof(circ.bottom_state) <: CorrelatedNoise
    if isCorrelatedNoise
        corr = circ.bottom_state.corr
    end
    if start_layer == circ.n_layers
        for (i, bl) in enumerate(circ.layers[end].branching_layers)
            if isCorrelatedNoise
                bl.cone.corr = corr
            end
            set_childs!(bl.cone, circ.bottom_state[((i-1)*circ.branching + 1):(i*circ.branching)])
        end
        start_layer -= 1
    end

    for i in start_layer:-1:1
        for (j, bl) in enumerate(circ.layers[i].branching_layers)
            childs = circ.layers[i+1].branching_layers[((j-1)*circ.branching + 1):(j*circ.branching)]
            childs = [child.cone for child in childs]
            if isCorrelatedNoise
                bl.cone.corr = corr
            end
            set_childs!(bl.cone, childs)
        end
    end
end

link_causal_cone!(circ::Circuit) = link_causal_cone!(circ, circ.n_layers)

function _propagate!(circ::Circuit, top_layer::Int, first_bit::Int, last_bit::Int, lpl::Int = 1)
    if top_layer != circ.n_layers
        state, interval = propagate(circ.layers[top_layer], first_bit, last_bit)
        new_positions = downward_permutation(circ, top_layer, interval[1], interval[2])
        n_branches = circ.branching^(circ.n_layers - top_layer)
        state_has_changed = false
        for (s,b,f,l) in zip(split_states(state, new_positions, n_branches)...)
            if circ.layers[top_layer+1].branching_layers[b].state[f:l] != s
                change_state!(circ.layers[top_layer+1].branching_layers[b], s, f, l)
                temp_lpl = _propagate!(circ::Circuit, top_layer+1, f + (b-1) * n_branches, l + (b-1) * n_branches)
                if temp_lpl > lpl
                    lpl = temp_lpl
                end
                state_has_changed = true
            end
        end
        if !state_has_changed
            lpl = top_layer
        end
    else
        lpl = top_layer
    end
    lpl
end
    
propagate!(circ::Circuit, first_bit::Int, last_bit::Int) = _propagate!(circ, 1, first_bit, last_bit)
propagate!(circ::Circuit, pos::Int) = _propagate!(circ, 1, pos, pos)
propagate!(circ::Circuit) = _propagate!(circ, 1, 1, length(circ))

"""
    decode!(circ::Circuit, width::Int = 1, frozen_bits::Vector{Int} = [])

Decode `circ` with width `width` and a set of `frozen_bits`. Return the decoded state.
"""
function decode!(circ::Circuit, width::Int = 1, frozen_bits::Vector{Int} = Vector{Int}([]))
    # Initial circuit state 
    state = fill(bit_e, circ.n_bits)

    last_open_leg = length(circ)
    while last_open_leg in frozen_bits
        state[last_open_leg] = bit_0
        last_open_leg -= 1
    end 
    
    state[last_open_leg-width+1:last_open_leg] = bit__
    init_circuit_state!(circ, state)
    link_causal_cone!(circ) 

    while last_open_leg > width
        contract!(top_cone(circ))
        last_open_leg = update_state!(circ, last_open_leg - width, width, frozen_bits)
        last_propagated_layer = propagate!(circ)
        link_causal_cone!(circ, last_propagated_layer) 
    end

    contract!(top_cone(circ))
    updated_state = [i in frozen_bits ? bit_0 : bit__ for i in 1:last_open_leg]
    if top_cone(circ).last_contracted_prob_state * updated_state == 1
        contracted_state = fill(bit_0, length(updated_state))
    else
        contracted_state = BitState(top_cone(circ).last_contracted_prob_state * updated_state)
        updated_state[updated_state .== bit__] = contracted_state
    end
    circ.layers[1].branching_layers[1].state[1:last_open_leg] = updated_state
    get_state(circ)
end

function show(io::IO, circ::Circuit)
    println("Number of bits : ", circ.n_bits)
    println("Number of layers : ", circ.n_layers)
    println("Branching : ", circ.branching)
    println("Bottom state : ", circ.bottom_state)
    for (index, layer) in enumerate(circ.layers)
        try
            println("Layer ", index, " : ", get_state(layer))
            println("")
        end
    end
end