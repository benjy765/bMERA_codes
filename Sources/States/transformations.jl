export Transformation, VectorTransformation, MatrixTransformation, identity_transform

#### Abstract type

"""
  Transformation <: Any

Abstract type.

Represent how to transform BitState and ProbState from top to bottom.

Needed fields for a composite subtype are 

- `permutation`
- `n_bits::Int`
"""
abstract type Transformation end

### VectorTransformation type 

"""
    VectorTransformation <: Transformation

Mutable type.

# Fields: 

- `permutation::Vector{Int} `

- `n_bits::Int`

A permutation vector of 2^n_bits unique elements between 1 and 2^n_bits.
It represent the permutation of a BitString.

Can be instanciated via a `Vector{Int}` :
```julia-repl
julia> VectorTransformation([1,3,2,4])
VectorTransformation([1, 3, 2, 4], 2)
```
a `Matrix{Int}` :
```julia-repl
julia> VectorTransformation([1 0 0 0 ; 0 1 0 0 ; 0 0 0 1 ; 0 0 1 0])
VectorTransformation([1, 2, 4, 3], 2)
```
or a `MatrixTransformation` :
```julia-repl
julia> mt = MatrixTransformation([1 0 0 0 ; 0 1 0 0 ; 0 0 0 1 ; 0 0 1 0]);
julia> VectorTransformation(mt)
VectorTransformation([1, 2, 4, 3], 2)
```

See [contractions.jl](@ref) for more informations and examples.
"""
mutable struct VectorTransformation <: Transformation
    permutation::Vector{Int}
    n_bits::Int

    # Inner constructor
    function VectorTransformation(permutation::Vector{Int})
        perm_length = length(permutation)
        if !isperm(permutation)
            error("This is not a permutation vector!")
        elseif !is_power_of_two(perm_length)
            error("Number of element must be a power of 2.")
        else
            new(permutation, Int(log2(perm_length)))
        end
    end
end

"""
    MatrixTransformation <: Transformation

Mutable type.

# Fields: 

- `permutation::SparseMatrixCSC{Int,Int}`

- `n_bits::Int`

A sparse matrix with size `2^n_bits` x `2^n_bits`. Each entries are `0` except for one and only
one entry by row and column. It represent the permutation of a BitString.

Can be instanciated via a `SparseMatrixCSC{Int,Int}` :
```julia-repl
julia> smcsc = SparseMatrixCSC{Int,Int}([1 0 0 0 ; 0 1 0 0 ; 0 0 0 1; 0 0 1 0]);
julia> MatrixTransformation(smcsc)
MatrixTransformation(
  [1, 1]  =  1
  [2, 2]  =  1
  [4, 3]  =  1
  [3, 4]  =  1, 2)
```
a `Matrix{Int}` :
```julia-repl
julia> MatrixTransformation([1 0 0 0 ; 0 1 0 0 ; 0 0 0 1 ; 0 0 1 0])
MatrixTransformation(
  [1, 1]  =  1
  [2, 2]  =  1
  [4, 3]  =  1
  [3, 4]  =  1, 2)
```
a `Vector{Int}` :
```julia-repl
MatrixTransformation([1,2,4,3])
MatrixTransformation(
  [1, 1]  =  1
  [2, 2]  =  1
  [4, 3]  =  1
  [3, 4]  =  1, 2)
``` 
or a `VectorTransformation` :
```julia-repl
julia> vt = VectorTransformation([1,2,4,3]);
julia> MatrixTransformation(vt)
MatrixTransformation(
  [1, 1]  =  1
  [2, 2]  =  1
  [4, 3]  =  1
  [3, 4]  =  1, 2)
```

See [contractions.jl](@ref) for more informations and examples.
"""
mutable struct MatrixTransformation <: Transformation
    permutation::SparseMatrixCSC{Int,Int}
    n_bits::Int

    # Inner constructor
    function MatrixTransformation(permutation::SparseMatrixCSC{Int,Int})
        i, j, v = findnz(permutation)
        order = size(permutation)[1]
        check_array = collect(1:order)
        check_i = sort(i) == check_array
        check_j = j == check_array
        check_v = sum(v) == length(v)

        if !(check_i && check_j && check_v)
            error("This is not a permutation matrix!")
        elseif !is_power_of_two(order)
            error("Number of element must be a power of 2.")
        else
            new(permutation, Int(log2(order)))            
        end   
    end
end

### Function and methods

==(transform1::Transformation, transform2::Transformation) = 
    transform1.permutation == transform2.permutation

!=(transform1::Transformation, transform2::Transformation) = !(transform1 == transform2)

"""
    length(transform::Transformation)

Return the number of bits that `transform` acts on.
"""
length(transform::Transformation) = transform.n_bits

"""
    inv(transform::transformation)

Return the inverse transformation of `transform`.
"""
inv(vTransform::VectorTransformation) = VectorTransformation(invperm(vTransform.permutation))
inv(mTransform::MatrixTransformation) = MatrixTransformation(transpose(mTransform.permutation))


"""
    kron(transform1::Transformation, transform2::Transform)

Return the kronecker product between `transform1` and `transform2`. The type 
of the result is the same as transform1.
"""
function kron(vt1::VectorTransformation, vt2::VectorTransformation)
    VectorTransformation(kron(MatrixTransformation(vt1), MatrixTransformation(vt2)))
end

function kron(mt1::MatrixTransformation, mt2::MatrixTransformation)
    MatrixTransformation(kron(mt1.permutation, mt2.permutation))
end

function kron(vt::VectorTransformation, mt::MatrixTransformation)
    VectorTransformation(kron(MatrixTransformation(vt), mt))
end

function kron(mt::MatrixTransformation, vt::VectorTransformation)
    kron(mt, MatrixTransformation(vt))
end

# Elementary transformations

identity_transform(n_bits::Int) = VectorTransformation(collect(1:2^n_bits))
    

### Outer constructors

## VectorTransformation constructors

function VectorTransformation(matrix::MatrixTransformation)
    permVect = invperm(findn(matrix.permutation)[1])
    VectorTransformation(permVect)
end

function VectorTransformation(matrix::Matrix{Int})
    permMatrix = MatrixTransformation(matrix)
    permVect = VectorTransformation(permMatrix)
    VectorTransformation(permVect.permutation)
end

## MatrixTransformation constructors

function MatrixTransformation(vect::VectorTransformation)
    I = vect.permutation
    J = collect(1:length(I))
    permMatrix = sparse(invperm(I), J, 1)
    MatrixTransformation(permMatrix)
end

function MatrixTransformation(matrix::Matrix{Int})
    permMatrix = sparse(matrix)
    MatrixTransformation(permMatrix)
end

function MatrixTransformation(vect::Vector{Int})
    permVect = VectorTransformation(vect)
    MatrixTransformation(permVect)
end



