export ProbState, ZeroProbState, BinProbState, TensorProbState, BitStringProbState, fix_bits 
export CorrelatedProbState, MidCorrelatedProbState, RightCorrelatedProbState, LeftCorrelatedProbState, Correlator
export CorrelatedNoise

### Type definitions

"""
    ProbState <: Any

Abstract type. 

It is a way to represent a distribution of probabilities over
the values of some bits.

Needed fields for a composite subtype are 

- `prob::AbstractArray{Float64}`
- `n_bits::Int`
"""
abstract type ProbState end

"""
    ZeroProbState <: ProbState

Immutable type.

# Fields 

- `prob::Vector{Float64}`
- `n_bits::Int`

A vector of probabilities p such that 0 <= p <= 1 and where each element represents 
the probability of a bit being 0. The lowest index represents the bit on the left.

# Examples
```julia-repl
julia> ZeroProbState([0.1, 0.3, 0.5, 0.7, 0.9])
ZeroProbState([0.1, 0.3, 0.5, 0.7, 0.9], 5)
```
"""
struct ZeroProbState <: ProbState
    prob::Vector{Float64}
    n_bits::Int

    function ZeroProbState(prob::Vector{Float64})
        if !check_prob(prob)
            error("All probabilities must be between 0 and 1.")
        else
            new(prob, length(prob))
        end
    end

    function ZeroProbState(prob::Float64)
        if !check_prob(prob)
            error("All probabilities must be between 0 and 1.")
        else
            new([prob], 1)
        end
    end
end
    
"""
    BinProbState <: ProbState

Immutable type.

# Fields:

- `prob::Matrix{Float64}`

- `n_bits::Int`

A matrix where the elements in the first column represent the probability 
of a bit being bit_0 and the element in the second column represent the probability of being bit_1.
Each rows must sum to one and each prob must be between 0 and 1. The lowest index represents 
the bit on the left.

# Examples
```julia-repl
julia> BinProbState([ 0.1 0.9 ; 0.2 0.8; 0.7 0.3])
BinProbState([0.1 0.9; 0.2 0.8; 0.7 0.3], 3)
```
"""
struct BinProbState <: ProbState
    prob::Matrix{Float64}
    n_bits::Int

    function BinProbState(prob::Matrix{Float64})
        if size(prob)[2] != 2
            error("Second dimension length must be 2.")
        elseif !check_prob(prob)
            error("All probabilities must be between 0 and 1.")
        elseif !(sum(prob,2) ≈ ones(Float64, size(prob)[1], 1))
            error("Probabilities for each bit must sum to 1.")
        else
            new(prob, size(prob)[1])
        end
    end

    function BinProbState(prob::Vector{Float64})
        if length(prob) != 2
            error("Length must be 2.")
        elseif !check_prob(prob)
            error("All probabilities must be between 0 and 1.")
        elseif !(sum(prob) ≈ one(Float64))
            error("Probabilities for each bit must sum to 1.")
        else
            new(reshape(prob,1,2), 1)
        end
    end
end

"""
    TensorProbState <: ProbState

Immutable type.

# Fields:

- `prob::Array{Float64}`

- `n_bits::Int`

A n-dimension array where n is the number of bits and each dimension as size 2.
The first dimension represent the bit on the left. The two index of each dimension
represent the two possible state for a bit (1 -> 0, 2 -> 1). Each element in the array
represents the probability of the corresponding bitstring. All probabilities must be 
between 0 and 1 and their sum must be 1.

# Examples
```julia-repl
julia> prob = zeros(2,2,2);
julia> prob[:,:,1] = [0.10 0.20 ; 0.25 0.05];
julia> prob[:,:,2] = [0.05 0.15 ; 0.10 0.10];
julia> TensorProbState(prob)
TensorProbState([0.1 0.2; 0.25 0.05]
[0.05 0.15; 0.1 0.1], 3)
```
"""
struct TensorProbState <: ProbState
    prob::Array{Float64}
    n_bits::Int
    function TensorProbState(prob::Array{Float64})
        if size(prob) !=  ntuple(x->2, ndims(prob))
            error("All dimensions must have length 2.")
        elseif !check_prob(prob)
            error("All probabilities must be between 0 and 1.")
        elseif !(sum(prob) ≈ 1)
            error("The sum of all probabilities must be 1")
        else
            new(prob, ndims(prob))
        end
    end
end


###  BitStringProbState

"""
    BitStringProbState <: ProbState

Composite type.

# Fields:

- `prob::Vector{Float64}`

- `n_bits::Int`

A vector where the elements represent the probability of a bit string. Entry at index `i`
represent the probability of the bitstring associated to the binary representation of `i - 1`.
All probabilities must be between 0 and 1 and their sum must be 1. Length of the prob must 
be a power of two.

# Examples
```julia-repl
julia> BitStringProbState([0.1, 0.05, 0.20, 0.15, 0.25, 0.10, 0.05, 0.10])
BitStringProbState([0.1, 0.05, 0.2, 0.15, 0.25, 0.1, 0.05, 0.1], 3)
```
"""
struct BitStringProbState <: ProbState
    prob::Vector{Float64}
    n_bits::Int

    function BitStringProbState(prob::Vector{Float64})
        if !is_power_of_two(length(prob))
            error("The length must be a power of two.")
        elseif !check_prob(prob)
            error("All probabilities must be between 0 and 1.")
        elseif !(sum(prob) ≈ 1)
            error("The sum of all probabilities must be 1")
        else
            new(prob, log2(length(prob)))
        end
    end

    function BitStringProbState(prob::Float64)
        if prob != 1
            error("The sum of all probabilities must be 1")
        else
            new([prob], 1)
        end
    end
end

### Functions and methods

==(ps1::ProbState, ps2::ProbState) = (ps1.prob == ps2.prob)
!=(ps1::ProbState, ps2::ProbState) = !(ps1.prob == ps2.prob)
≈(ps1::ProbState, ps2::ProbState) = (ps1.prob ≈ ps2.prob)

"""
    length(ps::ProbState)

Return the number of bits in `ps`.
"""
length(ps::ProbState) = ps.n_bits

"""
    permute(ps::ProbState, permutation::Vector{Int})

Return a new ProbState the same type as `ps` where the bits order as been changed 
according to `permutation`. `permutation` must be the same length as `ps`
and have unique entries between 1 and `length(ps)`.
"""
function permute(ps::ProbState, permutation::Vector{Int})
    if !isperm(permutation)
        error("Invalidate permutation.")  
    else
        ps[permutation] 
    end 
end

function permute(tps::TensorProbState, permutation::Vector{Int})
    if !isperm(permutation)
        error("Invalidate permutation.")   
    else
        TensorProbState(permutedims(tps.prob, permutation))
    end
end

function permute(bsps::BitStringProbState, permutation::Vector{Int})
    if !isperm(permutation)
        error("Invalidate permutation.")   
    else
        tps = TensorProbState(bsps)
        BitStringProbState(permute(tps, permutation))
    end
end

"""
    getindex(ps::ProbState, inds...)

Return the probabilities for the bits specified by `inds...` in the type of `ps`.
If the probabilities are correlated, return the marginal distribution, else 
return the original values at `inds...`. 
"""
getindex(zps::ZeroProbState, inds...) = ZeroProbState(getindex(zps.prob, inds...))
getindex(bps::BinProbState, inds...) = BinProbState(bps.prob[inds..., :])

function getindex(tps::TensorProbState, inds...)
    if sort(collect(inds)) == inds
        TensorProbState(get_sorted_index(tps.prob, inds...))
    else
        TensorProbState(get_unsorted_index(tps.prob, inds...))
    end
end

function getindex(bsps::BitStringProbState, inds...)
    tensor_prob = bit_string_to_tensor(bsps.prob)
    if sort(collect(inds)) == inds
        new_prob = get_sorted_index(tensor_prob, inds...)
    else
        new_prob = get_unsorted_index(tensor_prob, inds...)
    end
    BitStringProbState(tensor_to_bit_string(new_prob))
end

"""
    setindex!(zps::ZeroProbState, prob, inds...)

Set `prob` to bits indexed by `inds...`. All element of prob must be between 0 and 1.
The length of `prob` and `inds...` must the same and lower than the length of `zps`.
"""
function setindex!(zps::ZeroProbState, prob, inds...)
    if !check_prob(prob)
        error("All probabilities must be between 0 and 1.")
    else
        setindex!(zps.prob, prob, inds...)
    end
end

"""
    setindex!(bps::BinProbState, prob::Matrix{Float64}, inds...)

Set `prob` to bits indexed by `inds...`. All element of prob must be between 0 and 1.
Each row of `prob` must sum to 1. The number of rows in `prob` must be the length of  `inds...`
lower than the length of `bps`. The number of columns must be 2.
"""
function setindex!(bps::BinProbState, prob::Matrix{Float64}, inds...)
    if !check_prob(prob)
        error("All probabilities must be between 0 and 1.")
    elseif !(sum(prob,2) ≈ ones(Float64, size(prob)[1], 1))
        error("Probabilities for each bit must sum to 1.")
    else
        bps.prob[inds..., :] = prob
    end
end

"""
    setindex!(bps::BinProbState, prob::Vector{Float64}, ind::Int)

Set `prob` to the bit indexed by `ind`. All element of prob must be between 0 and 1.
`prob` must sum to 1. The length of `prob` must be 2.
"""
function setindex!(bps::BinProbState, prob::Vector{Float64}, ind::Int)
    if !check_prob(prob)
        error("All probabilities must be between 0 and 1.")
    elseif !(sum(prob) ≈ one(Float64))
        error("Probabilities for each bit must sum to 1.")
    else
        bps.prob[ind::Int, :] = prob
    end
end

"""
    kron(tps1::TensorProbState, tps2::TensorProbState)

Compute the kronecker product between the corresponding BitStringProbState and
return the result as a TensorProbState.
"""
function kron(tps1::TensorProbState, tps2::TensorProbState)
    TensorProbState(kron(BitStringProbState(tps1), BitStringProbState(tps2)))
end 

"""
    kron(bsps1::BitStringProbState, bsps2::BitStringProbState)

Compute the kronecker product between the two BitStringProbState array of probabilities.
"""
kron(bsps1::BitStringProbState, bsps2::BitStringProbState) = 
    BitStringProbState(kron(bsps1.prob, bsps2.prob))

"""
    fix_bits(tps::TensorProbState, b::Union{Bit, BitState}, inds...)

Return the probabilities distribution in the form of a TensorProbState with
the bits given by `inds...` fixed to the values given by `b`. Equivalent to the 
probabilities distribution conditional on the bits given by `inds...` begin `b`.
"""
function fix_bits(tps::TensorProbState, b::Bit, ind::Int)
    if (b == bit_e) || (b == bit__)
        error("Bit must be 0 or 1.")
    end
    bit_as_ind = Int(b) + 1    
    new_prob = dims_slices(tps.prob, bit_as_ind, ind)
    TensorProbState(new_prob / sum(new_prob))
end

function fix_bits(tps::TensorProbState, bs::BitState, inds...)
    if (bit_e in bs) || (bit__ in bs)
        error("All bits must be 0 or 1.")
    end
    bits_as_inds = map(x -> Int(x), bs) + 1
    new_prob = dims_slices(tps.prob, bits_as_inds, inds...) 
    TensorProbState(new_prob / sum(new_prob))
end

"""
    fix_bits(bsps::BitStringProbState, b::Union{Bit, BitState}, inds...)

Return the probabilities distribution in the form of a BitStringProbState with
the bits given by `inds...` fixed to the values given by `b`. Equivalent to the 
probabilities distribution conditional on the bits given by `inds...` begin `b`.
"""
function fix_bits(bsps::BitStringProbState, b::Union{Bit, BitState}, inds...)
    tps = TensorProbState(bsps)
    new_tps = fix_bits(tps, b, inds...)
    BitStringProbState(new_tps)
end

### Outer constructors (conversion between ProbState and BitState)

## BitState constructors

"""
    BitState(ps::ProbState)

Find the bit string with maximum probability in `ps` and return the corresponding BitState.
"""
BitState(zps::ZeroProbState) = BitState(zps.prob .< 0.5)

BitState(bps::BinProbState) = BitState(ZeroProbState(bps))

function BitState(tps::TensorProbState)
    indices_of_max = ind2sub(tps.prob, indmax(tps.prob))
    BitState(collect(indices_of_max) - 1)
end

function BitState(bsps::BitStringProbState) 
    prob_max = indmax(bsps.prob) - 1
    BitState(digits(prob_max, 2, bsps.n_bits)[end:-1:1])
end 

"""
    ZeroProbState(bps::BinProbState)

Return a ZeroProbState from the first column of `bps`.
"""
ZeroProbState(bps::BinProbState) = ZeroProbState(bps.prob[:,1])

"""
    BinProbState(zps::ZeroProbState)

Return a BinProbState where the first column is `zps` and the second is `1 - zps`. 
"""
function BinProbState(zps::ZeroProbState)
    bps = zeros(zps.n_bits, 2)
    bps[:,1] = zps.prob
    bps[:,2] = 1 - zps.prob
    BinProbState(bps)
end

"""
    TensorProbState(zps::ZeroProbState)

Convert `zps` to a BitStringProbState and then to a TensorProbState.
"""
TensorProbState(zps::ZeroProbState) = TensorProbState(BitStringProbState(zps))

"""
    TensorProbState(bps::BinProbState)

Convert `bps` to a BitStringProbState and then to a TensorProbState.
"""
TensorProbState(bps::BinProbState) = TensorProbState(BitStringProbState(bps))

"""
    TensorProbState(bsps::BitStringProbState)

Reshape `bsps` to a TensorProbState.
"""
TensorProbState(bsps::BitStringProbState) = TensorProbState(bit_string_to_tensor(bsps.prob))

"""
    BitStringProbState(zps::ZeroProbState)

Convert `zps` to a BinProbState and then to a BitStringProbState.
"""
BitStringProbState(zps::ZeroProbState) = BitStringProbState(BinProbState(zps))

"""
    BitStringProbState(bps::BinProbState)

Return the kronecker product between all rows of `bps` as a BitStringProbState.
"""
function BitStringProbState(bps::BinProbState)
    bsps = bps.prob[1,:]
    for i = 2:bps.n_bits
        bsps = kron(bsps, bps.prob[i,:])
    end
    BitStringProbState(bsps)
end

"""
    BitStringProbState(tps::TensorProbState)

Reshape `tps` to a BitStringProbState.
"""
BitStringProbState(tps::TensorProbState) = BitStringProbState(tensor_to_bit_string(tps.prob))


# ------------------------------ Correlated ProbState --------------------------------------------

"""
    CorrelatedProbState <: ProbState

Abstract type. 

Representation of the ProbState induce by correlation. 
It represents a tensor having physical legs and virtual legs (at most 2). 

Needed fields for a composite subtype are 

- `prob::AbstractArray{Float64}`
- `n_bits::Int`
"""
abstract type CorrelatedProbState <: ProbState end


"""
    MidCorrelatedProbState <: CorrelatedProbState

Composite type.

# Fields:

- `prob::Vector{Float64}`

- `n_bits::Int`

A tensor having `n_bits` physical legs and 2 virtual legs.
"""
struct MidCorrelatedProbState <: CorrelatedProbState
    prob::Array{Float64}
    n_bits::Int            

    function MidCorrelatedProbState(pg::Float64, pb::Float64)
        if !(0.0 <= pg <= 1.0) || !(0.0 <= pb <= 1.0)
            error("Probabilities must be between 0 and 1")
        end
        cps = zeros(2,2,2)
        cps[1,1,:] = [1-pg, pg]
        cps[2,2,:] = [1-pb, pb]
        new(cps, 1)
    end

    function MidCorrelatedProbState(prob::Array{Float64})
        if ndims(prob) < 3 
            error("Number of dimension must be greater than 2.")
        elseif size(prob) !=  ntuple(x->2, ndims(prob))
            error("All dimensions must have length 2.")
        #elseif !check_prob(prob)
        #    error("All probabilities must be between 0 and 1.")
        #elseif !(sum(prob[1,1,fill(:,ndims(prob)-2)...]) ≈ 1) || !(sum(prob[1,2,fill(:,ndims(prob)-2)...]) ≈ 1) || !(sum(prob[2,1,fill(:,ndims(prob)-2)...]) ≈ 1) || !(sum(prob[2,2,fill(:,ndims(prob)-2)...]) ≈ 1)
        #    error("The sum of all probabilities must be 1")
        else
            new(prob, ndims(prob)-2)
        end
    end
end

"""
    RightCorrelatedProbState <: CorrelatedProbState

Composite type.

# Fields:

- `prob::Vector{Float64}`

- `n_bits::Int`

A tensor having `n_bits` physical legs and 1 virtual leg.
This tensor is situated at the bottom right in the circuit.
"""
struct RightCorrelatedProbState <: CorrelatedProbState
    prob::Array{Float64}
    n_bits::Int

    function RightCorrelatedProbState(pg::Float64, pb::Float64)
        if !(0.0 <= pg <= 1.0) || !(0.0 <= pb <= 1.0)
            error("Probabilities must be between 0 and 1")
        end
        cps = zeros(2,2)
        #cps[1,:] = 1/sqrt(2)*[1-pg, pg]
        #cps[2,:] = 1/sqrt(2)*[1-pb, pb]
        cps[1,:] = 1/2*[1-pg, pg]
        cps[2,:] = 1/2*[1-pb, pb]        
        new(cps, 1)
    end

    function RightCorrelatedProbState(pg::Float64, pb::Float64, g::Float64, b::Float64)
        if !(0.0 <= pg <= 1.0) || !(0.0 <= pb <= 1.0)
            error("Probabilities must be between 0 and 1")
        end
        cps = zeros(2,2)
        cps[1,:] = g*[1-pg, pg]
        cps[2,:] = b*[1-pb, pb]
        new(cps, 1)
    end

    function RightCorrelatedProbState(prob::Array{Float64})
        if ndims(prob) < 2 
            error("Number of dimension must be greater than 1.")
        elseif size(prob) !=  ntuple(x->2, ndims(prob))
            error("All dimensions must have length 2.")
        #elseif !check_prob(prob)
        #    error("All probabilities must be between 0 and 1.")
        #elseif  !(sum(prob[1,fill(:,ndims(prob)-1)...]) ≈ 1) || !(sum(prob[2,fill(:,ndims(prob)-1)...]) ≈ 1)
        #    error("The sum of all probabilities must be 1")
        else
            new(prob, ndims(prob)-1)
        end
    end
end

"""
    LeftCorrelatedProbState <: CorrelatedProbState

Composite type.

# Fields:

- `prob::Vector{Float64}`

- `n_bits::Int`

A tensor having `n_bits` physical legs and 1 virtual leg.
This tensor is situated at the bottom left in the circuit.
"""
struct LeftCorrelatedProbState <: CorrelatedProbState
    prob::Array{Float64}
    n_bits::Int

    function LeftCorrelatedProbState(pg::Float64, pb::Float64)
        if !(0.0 <= pg <= 1.0) || !(0.0 <= pb <= 1.0)
            error("Probabilities must be between 0 and 1")
        end
        cps = zeros(2,2)
        #cps[1,:] = 1/sqrt(2)*[1-pg, pg]
        #cps[2,:] = 1/sqrt(2)*[1-pb, pb]
        cps[1,:] = 1/2*[1-pg, pg]
        cps[2,:] = 1/2*[1-pb, pb]
        new(cps, 1)
    end

    function LeftCorrelatedProbState(pg::Float64, pb::Float64, g::Float64, b::Float64)
        if !(0.0 <= pg <= 1.0) || !(0.0 <= pb <= 1.0)
            error("Probabilities must be between 0 and 1")
        end
        cps = zeros(2,2)
        cps[1,:] = g*[1-pg, pg]
        cps[2,:] = b*[1-pb, pb]
        new(cps, 1)
    end

    function LeftCorrelatedProbState(prob::Array{Float64})
        if ndims(prob) < 2 
            error("Number of dimension must be greater than 1.")
        elseif size(prob) !=  ntuple(x->2, ndims(prob))
            error("All dimensions must have length 2.")
        #elseif !check_prob(prob)
        #    error("All probabilities must be between 0 and 1.")
        #elseif !(sum(prob[1,fill(:,ndims(prob)-1)...]) ≈ 1) || !(sum(prob[2,fill(:,ndims(prob)-1)...]) ≈ 1)
        #    error("The sum of all probabilities must be 1")
        else
            new(prob, ndims(prob)-1)
        end
    end
end

"""
Matrix containing all the information about transition probabilities.
"""
struct Correlator
    corr::Matrix{Float64}

    function Correlator(corr::Matrix{Float64})
        if size(corr) != (2,2)
            error("Correlator matrix must have dimension 2 by 2")
        #elseif !(0.0 <= corr[1,1] <= 1.0) || !(0.0 <= corr[1,2] <= 1.0) || !(0.0 <= corr[2,1] <= 1.0) || !(0.0 <= corr[2,2] <= 1.0)
        #    error("All probabilities must be between 0 and 1")
        #elseif sum(corr[:,1]) != 1.0 || sum(corr[:,2]) != 1.0
        #    error("Transition probablity must sum to 1")
        else
            new(corr)
        end
    end

    function Correlator(pgg::Float64, pbb::Float64)
        if !(0.0 <= pgg <= 1.0) || !(0.0 <= pbb <= 1.0)
            error("Probabilities must be between 0 and 1")
        else
            new([pgg 1-pbb; 1-pgg pbb])
        end
    end
end

getindex(cps::MidCorrelatedProbState, inds...) = getindex(cps.prob, inds...)
getindex(cps::LeftCorrelatedProbState, inds...) = getindex(cps.prob, inds...)
getindex(cps::RightCorrelatedProbState, inds...) = getindex(cps.prob, inds...)
getindex(c::Correlator, inds...) = getindex(c.corr, inds...)

"""
Correlated Noise for the bottom state of the circuit
Need to specify correlator, the whole array or the number of bits, left, mid and right.
"""
struct CorrelatedNoise <: ProbState
    n_bits::Int
    corr::Correlator
    chain::Vector{CorrelatedProbState}

    # Here we specify a full array of CPS with a Correlator.
    function CorrelatedNoise(corr::Correlator, chain::Vector{T} where T <: CorrelatedProbState)
        if typeof(chain[1]) != LeftCorrelatedProbState || typeof(chain[end]) != RightCorrelatedProbState
            error("It must be such that first element is left and last is right.")
        else
            for i in 2:length(chain)-1
                if typeof(chain[i]) != MidCorrelatedProbState
                    error("Element in the middle of the array must be mid.")
                end
            end
            nbits = 0
            for element in chain
                nbits+= element.n_bits
            end
            new(nbits, corr, chain)
        end
    end

    # Here we assume each cps of size 1 bit
    function CorrelatedNoise(n_bits::Int, c::Correlator, l::LeftCorrelatedProbState, m::MidCorrelatedProbState, r::RightCorrelatedProbState)
        if l.n_bits != 1 || m.n_bits != 1 || r.n_bits != 1
            error("It must be a 1 bit cps.")
        else
            normalise = 1.0
            #chain = normalise*Vector{CorrelatedProbState}([l, fill(m,n_bits-2)..., r])
            chain = normalise*[l, fill(m,n_bits-2)..., r]
        end
        new(n_bits, normalise*c, chain)
    end
end
