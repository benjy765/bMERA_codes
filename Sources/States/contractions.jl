export contract_cps, contract_cnot

"""
    *(bs::BitState, ps::ProbState)
    *(ps::ProbState, bs::BitState)

Multiplication operator realizing a tensor like contraction where `bit__`
are considered as open legs. This operation commute.

# Examples
```julia-repl
julia> BitState("e_01")*ZeroProbState([0.1, 0.9, 0.3, 0.7])
ZeroProbState([0.9], 1)
julia> BinProbState([0.1 0.9 ; 0.9 0.1 ; 0.3 0.7 ; 0.7 0.3]) * BitState("e_01")
BinProbState([0.9 0.1], 1)
julia> BitStringProbState(fill(0.0625,16)) * BitState("e__1")
BitStringProbState([0.25, 0.25, 0.25, 0.25], 2)
```
"""
function *(bs::BitState, zps::ZeroProbState)
    if length(bs) != zps.n_bits
        error("Number of bits must be the same.")
    end

    if !(bit__ in bs)
        return 1
    end
    
    indices = find(bs .== bit__)
    zps[indices]
end

function *(bs::BitState, bps::BinProbState)
    if length(bs) != bps.n_bits
        error("Number of bits must be the same.")
    end
    
    if !(bit__ in bs)
        return 1
    end
    
    indices = find(bs .== bit__)
    bps[indices]
end

function *(bs::BitState, tps::TensorProbState)
    if length(bs) != tps.n_bits
        error("Number of bits must be the same.")
    end
    
    if !(bit__ in bs)
        return 1
    end

    indices_of_e = find(bs .== bit_e)
    indices_of_0 = find(bs .== bit_0)
    indices_of_1 = find(bs .== bit_1)
    indices_not__ = [indices_of_e ; indices_of_0 ; indices_of_1]
    values_to_keep = [ones(Int, length(indices_of_e) + length(indices_of_0)) ; 2*ones(Int, length(indices_of_1))]

    new_prob = sum(tps.prob, indices_of_e)
    new_prob = dims_slices(new_prob, values_to_keep, indices_not__)
    TensorProbState(new_prob / sum(new_prob))
end

function *(bs::BitState, bsps::BitStringProbState)
    if length(bs) != bsps.n_bits
        error("Number of bits must be the same.")
    end

    if !(bit__ in bs)
        return 1
    end

    tps = TensorProbState(bsps)
    BitStringProbState(bs * tps)
end

*(ps::ProbState, bs::BitState) = bs * ps

"""
    *(transform::Transformation, ps::ProbState)

Multiplication operator realizing a tensor like contraction. This operation doesn't commute.
The element on the right is consider higher in the circuit. If the type of `ps` doesn't
support correlation, a BitStringProbState is return

# Examples
```julia-repl
julia> VectorTransformation([4, 1, 2, 3]) * TensorProbState([0.40 0.15 ; 0.10 0.35])
TensorProbState([0.35 0.4; 0.15 0.1], 2)
julia> MatrixTransformation([0 0 0 1 ; 1 0 0 0 ; 0 1 0 0 ; 0 0 1 0]) * ZeroProbState([0.3, 0.8])
BitStringProbState([0.14, 0.24, 0.06, 0.56], 2)
```
"""
*(transform::Transformation, ps::ProbState) = transform * BitStringProbState(ps) 

function *(vTransform::VectorTransformation, tps::TensorProbState)
    if length(vTransform) != length(tps)
        error("The number of bits must be the same.")
    end
    bs_prob = tensor_to_bit_string(tps.prob)
    bs_prob = bs_prob[vTransform.permutation]
    TensorProbState(bit_string_to_tensor(bs_prob))
end

function *(mTransform::MatrixTransformation, tps::TensorProbState)
    if length(mTransform) != length(tps)
        error("The number of bits must be the same.")
    end
    bs_prob = tensor_to_bit_string(tps.prob)
    bs_prob = mTransform.permutation * bs_prob
    TensorProbState(bit_string_to_tensor(bs_prob))
end

function *(vTransform::VectorTransformation, bsps::BitStringProbState) 
    if length(vTransform) != length(bsps)
        error("The number of bits must be the same.")
    end
    BitStringProbState(bsps.prob[vTransform.permutation])
end

function *(mTransform::MatrixTransformation, bsps::BitStringProbState)
    if length(mTransform) != length(bsps)
        error("The number of bits must be the same.")
    end
    BitStringProbState(mTransform.permutation * bsps.prob)
end

"""
    *(ps::ProbState, transform::Transformation)

Return `inv(transform) * ps`.
"""
*(ps::ProbState, transform::Transformation) = inv(transform) * ps

"""
    *(transform::Transformation, bs::BitState) 

If `bs` is a vector of `bit_0` and `bit_1` return the output state 
"""
*(transform::Transformation, bs::BitState) = VectorTransformation(transform) * bs

function *(transform::VectorTransformation, bs::BitState)
    if bit_e in bs || bit__ in bs
        error("Only bit_0 and bit_1 can be transform.")
    end
    string_value = map(x -> Int(x), bs)' * 2 .^collect(length(bs)-1:-1:0)
    string_value = inv(transform).permutation[string_value + 1]
    BitState(digits(string_value - 1, 2, length(bs))[end:-1:1])
end

"""
    *(bs::BitState, transform::Transformation)

Return `inv(transform) * bs`
"""
*(bs::BitState, transform::Transformation) = inv(transform) * bs

"""
    *(low::Transformation, high::Transformation)

Return the contraction of both Transformation where `low` come after `high` in the circuit.
The return type is the same as `low`.

# Examples
```julia-repl
julia> VectorTransformation([1,2,4,3]) * VectorTransformation([2,3,1,4])
VectorTransformation([2, 3, 4, 1], 2)
julia> MatrixTransformation([1 0 0 0 ; 0 1 0 0 ; 0 0 0 1 ; 0 0 1 0]) * VectorTransformation([2,3,1,4])
MatrixTransformation(
  [4, 1]  =  1
  [1, 2]  =  1
  [2, 3]  =  1
  [3, 4]  =  1, 2)
```
"""
*(low::Transformation, high::Transformation) = low * typeof(low)(high)

function *(low::VectorTransformation, high::VectorTransformation)
    if length(low) != length(high)
        error("The number of bits must be the same.")
    end
    VectorTransformation(high.permutation[low.permutation])
end

function *(low::MatrixTransformation, high::MatrixTransformation)
    if length(low) != length(high)
        error("The number of bits must be the same.")
    end
    MatrixTransformation(low.permutation * high.permutation)
end

# ------------------------------ Useful contraction for correlated noise ----------------------------------------

"""
    contract_cps(corr, cps1, cps2)

Represent the contraction between a cps, a correlator and another cps. 
"""
function contract_cps(corr::Correlator, cps1::MidCorrelatedProbState, cps2::MidCorrelatedProbState)
    n_bits1, n_bits2 = cps1.n_bits, cps2.n_bits
    d_cps1, d_cps2 = fill(:, n_bits1), fill(:, n_bits2)
    result = zeros(2,2, fill(2, n_bits1 + n_bits2)...)
    for i = 1:2, j = 1:2
        a1 = kron_cps(cps1[i,1,d_cps1...], corr[1,1]*cps2[1,j,d_cps2...])
        a2 = kron_cps(cps1[i,1,d_cps1...], corr[1,2]*cps2[2,j,d_cps2...])
        a3 = kron_cps(cps1[i,2,d_cps1...], corr[2,1]*cps2[1,j,d_cps2...])
        a4 = kron_cps(cps1[i,2,d_cps1...], corr[2,2]*cps2[2,j,d_cps2...])

        result[i,j,d_cps1...,d_cps2...] = a1 + a2 + a3 + a4
    end
    MidCorrelatedProbState(result)
end

function contract_cps(corr::Correlator, cps1::LeftCorrelatedProbState, cps2::MidCorrelatedProbState)
    n_bits1, n_bits2 = cps1.n_bits, cps2.n_bits
    d_cps1, d_cps2 = fill(:, n_bits1), fill(:, n_bits2)
    result = zeros(2, fill(2, n_bits1 + n_bits2)...)
    for i in 1:2
        a1 = kron_cps(cps1[1,d_cps1...], corr[1,1]*cps2[1,i,d_cps2...])
        a2 = kron_cps(cps1[1,d_cps1...], corr[1,2]*cps2[2,i,d_cps2...])
        a3 = kron_cps(cps1[2,d_cps1...], corr[2,1]*cps2[1,i,d_cps2...])
        a4 = kron_cps(cps1[2,d_cps1...], corr[2,2]*cps2[2,i,d_cps2...])

        result[i,d_cps1...,d_cps2...] = a1 + a2 + a3 + a4       
    end
    LeftCorrelatedProbState(result)
end

function contract_cps(corr::Correlator, cps1::MidCorrelatedProbState, cps2::RightCorrelatedProbState)
    n_bits1, n_bits2 = cps1.n_bits, cps2.n_bits
    d_cps1, d_cps2 = fill(:, n_bits1), fill(:, n_bits2)
    result = zeros(2, fill(2, n_bits1 + n_bits2)...)
    for i in 1:2
        a1 = kron_cps(cps1[i,1,d_cps1...], corr[1,1]*cps2[1,d_cps2...])
        a2 = kron_cps(cps1[i,1,d_cps1...], corr[1,2]*cps2[2,d_cps2...])
        a3 = kron_cps(cps1[i,2,d_cps1...], corr[2,1]*cps2[1,d_cps2...])
        a4 = kron_cps(cps1[i,2,d_cps1...], corr[2,2]*cps2[2,d_cps2...])

        result[i,d_cps1...,d_cps2...] = a1 + a2 + a3 + a4  
    end
    RightCorrelatedProbState(result)
end

function contract_cps(corr::Correlator, cps1::LeftCorrelatedProbState, cps2::RightCorrelatedProbState)
    n_bits1, n_bits2 = cps1.n_bits, cps2.n_bits
    d_cps1, d_cps2 = fill(:, n_bits1), fill(:, n_bits2)
    result = zeros(fill(2, n_bits1 + n_bits2)...)
    a1 = kron_cps(cps1[1,d_cps1...], corr[1,1]*cps2[1,d_cps2...])
    a2 = kron_cps(cps1[1,d_cps1...], corr[1,2]*cps2[2,d_cps2...])
    a3 = kron_cps(cps1[2,d_cps1...], corr[2,1]*cps2[1,d_cps2...])
    a4 = kron_cps(cps1[2,d_cps1...], corr[2,2]*cps2[2,d_cps2...])

    result = a1 + a2 + a3 + a4
    TensorProbState(result/sum(result))
end

"""
Contract CPS to CNOT
    contract_cnot(cps)

Represent the contraction of a cps with a CNOT. 
The cps must be a tensor of 2 bits.
"""
function contract_cnot(cps::MidCorrelatedProbState)
    if cps.n_bits != 2
        error("Number of bits must be equal to 2 for this contraction")
    else
        result = zeros(2,2,2,2)
        for i = 1:2, j = 1:2
            result[i,j,:,:] = [cps[i,j,1,1] cps[i,j,1,2]; cps[i,j,2,2] cps[i,j,2,1]]
        end
        MidCorrelatedProbState(result)
    end
end

function contract_cnot(cps::LeftCorrelatedProbState)
    if cps.n_bits != 2
        error("Number of bits must be equal to 2 for this contraction")
    else
        result = zeros(2,2,2)
        for i in 1:2
           result[i,:,:] = [cps[i,1,1] cps[i,1,2]; cps[i,2,2] cps[i,2,1]]
        end
        LeftCorrelatedProbState(result)
    end
end

function contract_cnot(cps::RightCorrelatedProbState)
    if cps.n_bits != 2
        error("Number of bits must be equal to 2 for this contraction")
    else
        result = zeros(2,2,2)
        for i in 1:2
            result[i,:,:] = [cps[i,1,1] cps[i,1,2]; cps[i,2,2] cps[i,2,1]]
        end
        RightCorrelatedProbState(result)
    end
end

# When I have no bond indices
function contract_cnot(tps::TensorProbState)
    if tps.n_bits != 2
        error("Number of bits must be equal to 2 for this contraction")
    else
        VectorTransformation([1,2,4,3])*tps
    end
end

"""
Contraction of correlated noise
"""
# From right to left
function contractRtoL(c_noise::CorrelatedNoise)
    # First contraction 
    cps = contract_cps(c_noise.corr, c_noise.chain[end-1], c_noise.chain[end])
    for i in c_noise.n_bits-3:-1:1
        new_cps = contract_cps(c_noise.corr, c_noise.chain[i], cps)
        cps = new_cps
    end
    cps
    #tps = contract_cps(c_noise.corr, c_noise.chain[1], cps)
end

# From left to right
function contractLtoR(c_noise::CorrelatedNoise)
    # First contraction 
    cps = contract_cps(c_noise.corr, c_noise.chain[1], c_noise.chain[2])
    for i in 3:c_noise.n_bits
        new_cps = contract_cps(c_noise.corr, cps, c_noise.chain[i])
        cps = new_cps
    end
    #tps = contract_cps(c_noise.corr, cps, c_noise.chain[3])
    cps
end

function contractMid(c_noise::CorrelatedNoise)
    cps = c_noise.chain[end-1]
    for i in c_noise.n_bits-2:-1:1
        new_cps = contract_cps(c_noise.corr, c_noise.chain[i], cps)
        cps = new_cps
    end
    tps = contract_cps(c_noise.corr, cps, c_noise.chain[end])
    #tps = contract_cps(c_noise.corr, c_noise.chain[1], right)
end

function contractCorrNoise(c_noise::CorrelatedNoise)
    cps = c_noise.chain[1]
    for i in 1:length(c_noise.chain)-1
        new_cps = contract_cps(c_noise.corr, cps, c_noise.chain[i+1])
        cps = new_cps
    end
    cps
end

function getindex(cn::CorrelatedNoise, first::Int, last::Int)
    if first >= last
        error("Fist must be smaller than last")
    elseif last > cn.n_bits
        error("Last must be less or equal then the number of bits")
    else
        cps = cn.chain[first]
        for i in first:last-1
            new_cps = contract_cps(cn.corr, cps, cn.chain[i+1])
            cps = new_cps
        end
        cps
    end
end

getindex(cn::CorrelatedNoise, ind::Int) = cn.chain[ind]

function getindex(cn::CorrelatedNoise, inds::UnitRange{Int})
    inds_arr = collect(inds)
    if length(inds_arr) > 1
        getindex(cn, inds_arr[1], inds_arr[end])
    elseif  length(inds_arr) == 1 
        getindex(cn, inds_arr[1])
    end
end

function getindex(cn::CorrelatedNoise, inds...)
    inds_arr = collect(inds)
    if length(inds_arr) > 1
        getindex(cn, inds_arr[1], inds_arr[end])
    elseif  length(inds_arr) == 1 
        getindex(cn, inds_arr[1])
    end
end


# ------------------- Contraction of cps and BitState -----------------------------

function *(bs::BitState, cps::MidCorrelatedProbState)
    d_cps = fill(:,cps.n_bits)
    open_legs = n_open_legs(bs)
    s = sum(cps.prob)
    if open_legs == 0
        error("No open legs")
    end
    result = zeros(2,2,fill(2,open_legs)...)

    indices_of_e = find(bs .== bit_e)
    indices_of_0 = find(bs .== bit_0)
    indices_of_1 = find(bs .== bit_1)
    indices_not__ = [indices_of_e ; indices_of_0 ; indices_of_1]
    values_to_keep = [ones(Int, length(indices_of_e) + length(indices_of_0)) ; 2*ones(Int, length(indices_of_1))]
    for i in 1:2, j in 1:2
        b = sum(cps[i,j,d_cps...])
        if b != 0
            new_prob = sum(cps[i,j,d_cps...], indices_of_e)
            c = sum(new_prob)
            a = dims_slices(new_prob, values_to_keep, indices_not__)
            result[i,j,fill(:, open_legs)...] = a
            #result[i,j,fill(:, open_legs)...] = a
        else
            result[i,j,fill(:, open_legs)...] = zeros(fill(2, open_legs)...)
        end
    end
    MidCorrelatedProbState(result/sum(result))
end

function *(bs::BitState, cps::LeftCorrelatedProbState)
    d_cps = fill(:,cps.n_bits)
    open_legs = n_open_legs(bs)
    s = sum(cps.prob)
    if open_legs == 0
        error("No open legs")
    end
    result = zeros(2,fill(2,open_legs)...)

    indices_of_e = find(bs .== bit_e)
    indices_of_0 = find(bs .== bit_0)
    indices_of_1 = find(bs .== bit_1)
    indices_not__ = [indices_of_e ; indices_of_0 ; indices_of_1]
    values_to_keep = [ones(Int, length(indices_of_e) + length(indices_of_0)) ; 2*ones(Int, length(indices_of_1))]

    for i in 1:2
        b = sum(cps[i,d_cps...])
        if b != 0
            new_prob = sum(cps[i,d_cps...], indices_of_e)
            c = sum(new_prob)
            a = dims_slices(new_prob, values_to_keep, indices_not__)
            result[i,fill(:, open_legs)...] = a
            #result[i,fill(:, open_legs)...] = a
        else
            result[i,fill(:, open_legs)...] = zeros(fill(2, open_legs)...)
        end
    end
    LeftCorrelatedProbState(result/sum(result))
end

function *(bs::BitState, cps::RightCorrelatedProbState)
    d_cps = fill(:,cps.n_bits)
    open_legs = n_open_legs(bs)
    s = sum(cps.prob)
    if open_legs == 0
        error("No open legs")
    end
    result = zeros(2,fill(2,open_legs)...)

    indices_of_e = find(bs .== bit_e)
    indices_of_0 = find(bs .== bit_0)
    indices_of_1 = find(bs .== bit_1)
    indices_not__ = [indices_of_e ; indices_of_0 ; indices_of_1]
    values_to_keep = [ones(Int, length(indices_of_e) + length(indices_of_0)) ; 2*ones(Int, length(indices_of_1))]

    for i in 1:2
        b = sum(cps[i,d_cps...])
        if b != 0
            new_prob = sum(cps[i,d_cps...], indices_of_e)
            c = sum(new_prob)
            a = dims_slices(new_prob, values_to_keep, indices_not__)
            result[i,fill(:, open_legs)...] = a
            #result[i,fill(:, open_legs)...] = a
        else
            result[i,fill(:, open_legs)...] = zeros(fill(2, open_legs)...)
        end
    end
    RightCorrelatedProbState(result/sum(result))
    #RightCorrelatedProbState(result)
end

# Implementation of contraction with bitstate.
#=
function *(bs::BitState, cps<:CorrelatedProbState)

end
=#


*(n::Float64, l::LeftCorrelatedProbState) = LeftCorrelatedProbState(n*l.prob)
*(n::Float64, m::MidCorrelatedProbState) = MidCorrelatedProbState(n*m.prob)
*(n::Float64, r::RightCorrelatedProbState) = RightCorrelatedProbState(n*r.prob)
function *(n::Float64, v_cps::Array{CorrelatedProbState})
    chain = [n*cps for cps in v_cps]
end
*(n::Float64, c::Correlator) = Correlator(n*c.corr)