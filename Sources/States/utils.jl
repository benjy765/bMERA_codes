"""
    check_prob(prob)

Check if prob is between 0 and 1 and return a Bool (Can be apply on an Array).
"""
function check_prob(prob)
    sum(0.0 .<= prob .<= 1.0) == length(prob)
end


"""
    is_power_of_two(x::Int)

Check if x is a power of 2 and return a Bool.
"""
function is_power_of_two(x::Int)
    binary = digits(x, 2)
    sum(binary) == 1
end

"""
    other_indices(max_ind::Int, inds...)

Return all the indices between 1 and `max_ind` that are not in inds...
"""
other_indices(max_ind::Int, inds...) = [ind for ind in 1:max_ind if !(ind in inds...)]

"""
    dims_slices(array::AbstractArray, fixed_dim_value::Int, dim::Int)

Return `array[:,:, ... , :, fixed_dim_value, :, ...]` where the fixed dimension is given by dim.
"""
function dims_slices(array::AbstractArray, fixed_dim_value::Int, dim::Int)
    first_inds = ones(Int, ndims(array))
    first_inds[dim] = fixed_dim_value
    first_inds = CartesianIndex(first_inds...)

    last_inds = [size(array)...]
    last_inds[dim...] = fixed_dim_value
    last_inds = CartesianIndex(last_inds...)

    inds_to_keep = CartesianRange(first_inds, last_inds)

    new_array = zeros(size(array)[other_indices(ndims(array), dim...)])

    for (i, j) in enumerate(inds_to_keep)
        new_array[i] = array[j]
    end

    new_array
end

"""
    dims_slices(array::AbstractArray, fixed_dims_values::Vector{Int}, dims...)

Return `array[..., :, fixed_dim_values[i], :, ...]` where fixed_dim_values[i] is set at dimension dims[i].
"""
function dims_slices(array::AbstractArray, fixed_dims_values::Vector{Int}, dims...)
    if length(fixed_dims_values) != length(dims...)
        error("The number of fixed values and dims must be the same.")
    end

    first_inds = ones(Int, ndims(array))
    first_inds[dims...] = fixed_dims_values
    first_inds = CartesianIndex(first_inds...)

    last_inds = [size(array)...]
    last_inds[dims...] = fixed_dims_values
    last_inds = CartesianIndex(last_inds...)

    inds_to_keep = CartesianRange(first_inds, last_inds)

    new_array = zeros(size(array)[other_indices(ndims(array), dims...)])

    for (i, j) in enumerate(inds_to_keep)
        new_array[i] = array[j]
    end

    new_array
end

"""
   tensor_to_bit_string(t::Array{Float64})

Convert a prob array from a tensor (2 x 2 x 2 ...) to an bit string (2^n vector).
"""
tensor_to_bit_string(t::Array{Float64}) = reshape(permutedims(t, ndims(t):-1:1), length(t))

"""
    bit_string_to_tensor(bs::Vector{Float64})

Convert a prob array from an bit string (2^n vector) to a tensor (2 x 2 x 2 ...).
"""
function bit_string_to_tensor(bs::Vector{Float64})
    n = Int(log2(length(bs)))
    permutedims(reshape(bs, ntuple(x -> 2, n)), n:-1:1)
end

"""
    get_sorted_index(tensor_prob::Array{Float64}, inds...)

A fast utilitary function of getindex for TensorProbState to apply when indices 
are sorted (Involve no copy).
"""
function get_sorted_index(tensor_prob::Array{Float64}, inds...)
    n_bits_to_keep = length(inds...)
    dims_to_sum = other_indices(ndims(tensor_prob), inds...)
    marginal = sum(tensor_prob, dims_to_sum)
    reshape(marginal, ntuple(x->2, n_bits_to_keep))
end

"""
    get_unsorted_index(tensor_prob::Array{Float64}, inds...)

A getindex function for TensorProbState when indices are not sorted (Involve a copy).
"""
# A slower version when indices are not sorted (Involve a copy.)
function get_unsorted_index(tensor_prob::Array{Float64}, inds...)
    n_bits_to_keep = length(inds...)
    new_order = [inds... ; other_indices(ndims(tensor_prob), inds...)]
    new_prob = permutedims(tensor_prob, new_order)
    marginal = sum(new_prob, (n_bits_to_keep+1):ndims(tensor_prob))
    reshape(marginal, ntuple(x->2, n_bits_to_keep))
end

function kron_cps(c1::Array{Float64}, c2::Array{Float64})
    n1, n2 = ndims(c1), ndims(c2)
    result = kron(reshape(permutedims(c1,n1:-1:1),2^n1), reshape(permutedims(c2,n2:-1:1),2^n2))
    permutedims(reshape(result, ntuple(x->2, (n1+n2))), (n1+n2):-1:1)
end