export Bit, bit_0, bit_1, bit_e, bit__
export BitState, n_open_legs, all_bit_states
export bit_error_rate, frame_error_rate

"""
Enumeration representing bits. Possible values are
    
  * bit_0 represent (1, 0),
  * bit_1 represent (0, 1),
  * bit_e represent (1, 1),
  * bit__ represent an open leg.
    
Can be instantiated via a char (`'0'`, `'1'`, `'e'`, `'_'`), an integer (`0`, `1`, `2`, `3`)
or by their name :

# Examples
```julia-repl
julia> Bit(0)
bit_0::Bit = 0
julia> Bit('e')
bit_e::Bit = 2
julia> bit__
bit__::Bit = 3
```
"""
@enum Bit bit_0=0 bit_1=1 bit_e=2 bit__=3

# Literal 0 and 1

zero(b::Bit) = bit_0
zero(T::Type{Bit}) = bit_0
one(b::Bit) = bit_1
one(T::Type{Bit}) = bit_1

# Constructor from Char. Choice are "0", "1", "e" and "_".

function Bit(c::Char)
    if c == '0'
        return bit_0
    elseif c == '1'
        return bit_1
    elseif c == 'e'
        return bit_e
    elseif c == '_'
        return bit__
    else
        error("Invalid identifier for Bit.")
    end
end

function +(b1::Bit, b2::Bit)
    if b1 == bit_e || b1 == bit__ || b2 == bit_e || b2 == bit__
        error("Can't add bit_e and bit__.")
    end
    return Bit((Int(b1) + Int(b2)) % 2)
end

function *(b1::Bit, b2::Bit)
    if b1 == bit_e || b1 == bit__ || b2 == bit_e || b2 == bit__
        error("Can't add bit_e and bit__.")
    end
    return Bit(Int(b1) * Int(b2))
end

"""
An alias for any array of Bit. Can be instantiated via an array of `Bit` :

```julia-repl
julia> BitState([bit__, bit_1])
2-element Array{Bit,1}:
 bit__
 bit_1
```

For a vector, can be instantiated via a string :

```julia-repl
julia> BitState("e_01")
4-element Array{Bit,1}:
 bit_e
 bit__
 bit_0
 bit_1
```
"""  
const BitState = AbstractArray{Bit}

# Create a BitState from a string of "0", "1", "e" and "_".

function BitState(bit_string::String)
    state = zeros(Bit, length(bit_string))
    for (idx, bit) in enumerate(bit_string)
        state[idx] = Bit(bit)
    end
    BitState(state)
end

# Utils

"""
    permute(bs::Vector{Bit}, permutation::Vector{Int})

If `permutation` is a valid length(bs) permutation then return bs[permutation].
"""
function permute(bs::Vector{Bit}, permutation::Vector{Int})
    if !isperm(permutation)
        error("Invalidate permutation.")  
    else
        bs[permutation] 
    end 
end

"""
    n_open_legs(bs::BS) where {BS <: BitState}

Return the number of bit__ in bs.
"""
n_open_legs(bs::BS) where {BS <: BitState} = sum(bs .== bit__)

""" 
    all_bit_states(n_bits::Int)

Return an array of 4^n_bits elements containing every possible BitState of length n_bits. 
"""
function all_bit_states(n_bits::Int)
    states = fill(zeros(Bit, n_bits), 4^n_bits)
    for i in 1:(4^n_bits)
        states[i] = BitState(digits(i-1,4,n_bits))
    end
    return states
end

function string(bs::BitState)
    st = ""
    for elem in bs
        if elem == bit_0
            st = st * "0"
        elseif elem == bit_1
            st = st * "1"
        elseif elem == bit_e
            st = st * "e"
        elseif elem == bit__
            st = st * "_"
        end
    end
    return st
end

function bit_error_rate(bs1::BS, bs2::BS, n_not_frozen::Int = length(bs1)) where {BS <: BitState}
    if size(bs1) != size(bs2)
        error("Both BitState must have same size.")
    end
    return sum(bs1 .!= bs2) / n_not_frozen
end

function bit_error_rate(bs::BS, n_not_frozen::Int = length(bs)) where {BS <: BitState}
    return sum(bs1) / n_not_frozen
end

function frame_error_rate(bs1::BS, bs2::BS) where {BS <: BitState}
    if size(bs1) != size(bs2)
        error("Both BitState must have same size.")
    end
    return Int(sum(bs1 .!= bs2) > 0)
end

function frame_error_rate(bs::BS) where {BS <: BitState}
    return Int(bit_1 in bs)
end


