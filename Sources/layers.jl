export Layer, get_state, set_state!, propagate, get_transform, depth
"""
    Layer <: Any

Mutable type.

# Fields 

- `n_bits::Int`
- `branches_length::Int`
- `branching_layers::Vector{BranchingLayer}`

A vector of `branching_layer` with each branching_layer having a length `branches_length`. 
A valid `Layer` must have all branching layers with the same length. There are two ways to initialize a layer:
    
    Layer(branching_layers::Vector{BranchingLayer})
    Layer(branching_layer::BranchingLayer, n_branching::Int)

where `n_branching` is the number of `BranchingLayer` within `branching_layers`.

# Examples

```julia-repl
julia> id, cnot = elementary_gates["id_1"], elementary_gates["cnot"];
julia> bl = BranchingLayer([SubLayer(8, [GatePosition(cnot, 2*j - 1, 2*j) for j in 1:4])]);
julia> layer = Layer(bl, 2);

julia> layer.n_bits
16

julia> layer.branches_length
8
```
"""
mutable struct Layer 
    n_bits::Int
    branches_length::Int
    branching_layers::Vector{BranchingLayer}

    function Layer(branching_layers::Vector{BranchingLayer})
        n_bits = [length(bl) for bl in branching_layers]
        if length(unique(n_bits)) != 1
            error("All branching layers must have the same length.")
        end
        new(sum(n_bits), n_bits[1], branching_layers)
    end

    function Layer(branching_layer::BranchingLayer, n_branching::Int)
        n_bits = n_branching * length(branching_layer)
        new(n_bits, length(branching_layer), [BranchingLayer(branching_layer.sublayers) for i in 1:n_branching])
    end
end

get_state(layer::Layer) = vcat([bl.state for bl in layer.branching_layers]...) 
length(layer::Layer) = layer.n_bits

# set_state! for a layer
"""
    set_state!(layer::Layer, bs::BitState)

Set a `BitState` on the top of a `Layer`. The effect of this function is to update the value of `state` for each `BranchingLayer`
and to initialize a `cone`. Also, important to note that the simplification and propagation are not apply automatically.
"""
function set_state!(layer::Layer, bs::BitState)
    if length(bs) != length(layer)
        error("The length of the Layer and the state must be the same.")
    end
    
    n = 1
    for bl in layer.branching_layers
        set_state!(bl, bs[n:n+length(bl)-1])
        n += length(bl)
    end 
end

## Propagate layer
"""
    propagate(layer::Layer, first_bit::Int, last_bit::Int)

Propagate the `BitState` of the layer starting from `first_bit` up to `last_bit` and return the output state 
with the first bit and last bit positions.
"""
function propagate(layer::Layer, first_bit::Int, last_bit::Int)
    first_bl, first_bl_bit = position_to_coordinate(first_bit, layer.branches_length)
    last_bl, last_bl_bit = position_to_coordinate(last_bit, layer.branches_length)

    if first_bl == last_bl
        output = propagate(layer.branching_layers[first_bl], first_bl_bit, last_bl_bit)
        (output[1], (first_bl-1)*layer.branches_length + output[2])
    else
        output_state = fill([], last_bl - first_bl + 1)
        first = first_bl_bit
        last = last_bl_bit
        for (i, j) in enumerate(first_bl:last_bl)
            if j == first_bl
                output = propagate(layer.branching_layers[j], first_bl_bit, layer.branches_length)
                output_state[i] = output[1]
                first = output[2][1] + (j - 1) * layer.branches_length
            elseif j == last_bl
                output = propagate(layer.branching_layers[j], 1, last_bl_bit)
                output_state[i] = output[1]
                last = output[2][2] + (j - 1) * layer.branches_length
            else
                output_state[i] = propagate(layer.branching_layers[j])[1]
            end
        end
        (BitState(vcat(output_state...)), [first, last])
    end
end
"""
    propagate(layer::Layer, position::Int)

Propagate for one bit position.
"""
propagate(layer::Layer, position::Int) = propagate(layer, position, position)
"""
    propagate(layer::Layer)

Propagate for the whole layer.

For more information see [`propagate(bl::BranchingLayer, first_bit::Int, last_bit::Int)`](@ref) simply because the function propagate
in the case of a `layer` acts in the same way as for a `branching_layer`.
"""
propagate(layer::Layer) = propagate(layer, 1, length(layer))

"""
    get_transform(layer::Layer)

Return the global transformation of `layer`.
"""
function get_transform(layer::Layer)
    if length(layer.branching_layers) == 1
        get_transform(layer.branching_layers[1])
    else
        kron([get_transform(bl) for bl in layer.branching_layers]...)
    end
end

function depth(layer::Layer)
    d = 0
    for bl in layer.branching_layers
        d = length(bl.sublayers) > d ? length(bl.sublayers) : d
    end
    d
end
