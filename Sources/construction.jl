export polar_code, conv_polar_code, random_conv_circuit, fix_conv_polar_code

function init_polar_layer(n_layers::Int, layer::Int, gate::Gate)
    branching = length(gate)
    gates = [GatePosition(gate,i,i+branching-1) for i in 1:branching:branching^(n_layers-layer+1)]
    sl = SubLayer(branching^(n_layers-layer+1), gates)
    bl = BranchingLayer([sl])
    Layer(bl, branching^(layer-1))
end

"""
    polar_code(n_layers::Int, gate::Gate)

Create a `Circuit` with a polar structure for the given `gate` where
the `branching` is equal to `length(gate)`.
"""
polar_code(n_layers::Int, gate::Gate) = 
    Circuit(length(gate), [init_polar_layer(n_layers, l, gate) for l in 1:n_layers])

function mid_gates(gate::Gate, branching::Int, all_starting_points)
    gates = Vector{GatePosition}([])
    for k in all_starting_points
        push!(gates, GatePosition(gate, k, k + length(gate) - 1))
        id_pad = [GatePosition(elementary_gates["id_1"], l+k) for l in length(gate):(branching-1)]
        append!(gates, id_pad)
    end
    gates
end 

function init_conv_layer(branching::Int, n_layers::Int, layer::Int, 
                         gates::Vector{Gate}, starting_points::Vector{Int})
    sublayers = Vector{SubLayer}([])
    n_bits = branching^(n_layers-layer+1)
    for i in 1:length(gates)
        all_starting_points = starting_points[i]:branching:(n_bits - branching + 1)
        
        if length(all_starting_points) > 0 # Else no gate can fit
            last_left_id = starting_points[i] - 1
            left = [GatePosition(elementary_gates["id_1"], k) for k in 1:last_left_id]
            
            first_right_id = all_starting_points[end] + length(gates[i])
            right = [GatePosition(elementary_gates["id_1"], k) for k in first_right_id:n_bits]
            
            mid = mid_gates(gates[i], branching, all_starting_points)
            
            push!(sublayers, SubLayer(n_bits, vcat(left, mid, right)))
        end
    end
    bl = BranchingLayer(sublayers)
    Layer(bl, branching^(layer-1))
end

function check_starting_points(branching::Int, gates::Vector{Gate}, starting_points::Vector{Int})
    if length(gates) != length(starting_points)
        error("Each gate must have a starting point.")
    end

    for i in 1:length(gates)
        if !(1 + branching - length(gates[i]) <= starting_points[i] <= branching)
           error("Starting point must be between 1 + branching - length(gate) and branching.") 
        end
    end

    if !(branching in map(x -> length(x), gates[starting_points .== 1]))
        error("At least one of the largest gates must start at bit 1.")
    end
end

"""
    conv_polar_code(n_layers::Int,  gates::Vector{Gate}, starting_points::Vector{Int})

Create a `Circuit` with a general convolutionnal polar structure. The `branching` is given
by the length of the largest element of `gates`. 

The `SubLayer` i of each `BranchingLayer` is given by the repetition of `gates[i]` at position
`starting_points[i]`, `starting_points[i] + branching`, `starting_points[i] + 2 * branching`, 
until it is not possible to fit another gate.
"""
function conv_polar_code(n_layers::Int,  gates::Vector{Gate}, starting_points::Vector{Int},
                         branching::Int = maximum(map(x -> length(x), gates)))
    check_starting_points(branching, gates, starting_points)
    layers = [init_conv_layer(branching, n_layers, l, gates, starting_points) for l in 1:n_layers]
    Circuit(branching, layers)
end
    
"""
    random_conv_circuit(n_layers::Int, branching::Int, n_gates::Int)    
"""     
function random_conv_circuit(n_layers::Int, branching::Int, n_gates::Int) 
    gates = [random_linear_reversible_gate(rand(2:branching)) for i in 1:n_gates]
    starting_points = [rand((branching-length(g)+2):branching) for g in gates]
    main_gate = rand(1:n_gates)
    gates[main_gate] = random_linear_reversible_gate(branching)
    starting_points[main_gate] = 1
    circ = conv_polar_code(n_layers, gates, starting_points)

    (circ, gates, starting_points)
end

function fix_conv_polar_code(n_layers::Int, gates::Vector{Gate}, starting_points::Vector{Int},
                         branching::Int = maximum(map(x -> length(x), gates)))
    
    check_starting_points(branching, gates, starting_points)
    
    layers = Vector{Layer}(n_layers)
    
    layers[1:end-1] = 
        [init_conv_layer(branching, n_layers, l, gates, starting_points) for l in 1:(n_layers-1)]

    # Change last layer to only cnot
    n_bits = branching^n_layers
    gate_position = [GatePosition(elementary_gates["cnot"], 1, 2)]
    sublayer = SubLayer(2, gate_position)
    layers[end] = Layer(BranchingLayer([sublayer]), Int(n_bits/2))

    Circuit(branching, layers)
end
