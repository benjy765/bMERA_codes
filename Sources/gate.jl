export Gate, GateSet, identity_gate, identity_gate!, not_gate, not_gate!
export cnot_gate, cnot_gate!, multi_cnot_gate, multi_cnot_gate!, swap_gate, elementary_gates
export random_linear_reversible_gate
### Gate type
"""
    Gate <: Any

Mutable type.

# Fields

- `n_bits::Int`
- `transform::Transformation`
- `simplification::Function`
- `memory::Dict{BitState, Tuple{BitState, Vector{Vector{Int}}, Vector{Gate}}}`

A `Gate` transform the ProbState via `transform` and use `simplification` to compute 
the way a BitState interact with the gate. `memory` is use to keep track of already done
simplification in order to reuse computation. To create a new gate, we only need to specify the
transform and then manualy give the simplification. This procedure is mandatory to create
`Gate` that can simplify to themself. 

# Example 

```julia-repl
julia> not = Gate(VectorTransformation([2,1]))
1 bit(s) cnot gate
julia> not.simplification = bs -> (Int(bs[1]) < 2 ? [Bit(1 - Int(bs[1]))] : copy(bs), [[1]], [not]);
julia> not[bit_1]
(Bit[bit_0], Array{Int64,1}[[1]], Gate[1 bit(s) cnot gate])
```

See [Elementary gates](@ref) for more details and examples.
"""
mutable struct Gate
    n_bits::Int
    transform::Transformation
    simplification::Function # ::BitState -> (::BitState, ::Vector{Int}, ::Vector{Gate})
    memory::Dict{BitState, Tuple{BitState, Vector{Vector{Int}}, Vector{Gate}}} # Computed simplifications

    # Simplification and memory must be set after to allow self referencing.
    function Gate(transform::Transformation)
        n_bits = length(transform)
        gate = new(n_bits, transform) 
        gate.simplification = bs -> Tuple{BitState, Vector{Vector{Int}}, Vector{Gate}}()
        gate.memory = Dict{BitState, Tuple{BitState, Vector{Vector{Int}}, Vector{Gate}}}()
        gate
    end
end

"""
A dictionnary of `Gate` that can be access by names. Can be use to reduce the number of needed 
`Gate`. All `Gate` in a `GateSet` will simplify to `Gate` in that `GateSet`. So, it is a better 
usage of the `memory` parameter of `Gate`.

See [Elementary gates](@ref) for more details and examples.
"""
const GateSet = Dict{String, Gate}

## Kronecker product for Gate

"""
    kron(left_gate::Gate, right_gate::Gate)

Return a new Gate where `transform` is the kronecker product between 
`left_gate.transform` and `right_gate.transform` and `simplification` is a 
concatenation of `left_gate.transform` and `right_gate.transform`.

# Example 

```julia-repl
julia> id = elementary_gates["id_1"];
julia> not = elementary_gates["not"];
julia> id_not = kron(id, not)
2 bit(s) gate : [2, 1, 4, 3]
julia> id_not[BitState("01")]
(Bit[bit_0, bit_0], Array{Int64,1}[[1], [2]], Gate[1 bit(s) identity gate, 1 bit(s) cnot gate])
```
"""
function kron(left_gate::Gate, right_gate::Gate)
    transf = kron(left_gate.transform, right_gate.transform)
    gate = Gate(transf)
    gate.simplification = bs -> kron_simplification(bs, gate, left_gate, right_gate)        
    gate
end

function kron_simplification(bs::BitState, new_gate::Gate, left_gate::Gate, right_gate::Gate)
    output1 = left_gate[bs[1:length(left_gate)]]
    output2 = right_gate[bs[length(left_gate)+1:new_gate.n_bits]]
   
    states = vcat(output1[1], output2[1])
    positions = vcat(output1[2], left_gate.n_bits + output2[2])
    gates = vcat(output1[3], output2[3])
   
    (states, positions, gates)
end

## Multiplication for Gate

"""
    *(down_gate::Gate, up_gate::Gate)

Return a new Gate where `transform` is given by `down_gate.transform * up_gate.transform` and
`simplification` is obtain by parsing the result of `up_gate.simplification` to 
`down_gate.simplification`. The resulting `simplification` may not be optimaly simplify.

# Example

```julia-repl
julia> not = elementary_gates["not"];
julia> not * not
1 bit(s) identity gate
```
"""
function *(down_gate::Gate, up_gate::Gate)
    if length(down_gate) != length(up_gate)
        error("Gates must have the same dimension")
    end 
    transf = down_gate.transform * up_gate.transform
    gate = Gate(transf)
    # gate simplification is not optimal but we don't care for our use...
    gate.simplification = bs -> (down_gate[up_gate[bs][1]][1], [collect(1:length(gate))], [gate]) 
    gate
end

## Get index. ::Gate[::BitState] -> (::BitState, ::Vector{Int}, ::Vector{Gate})

"""
    getindex(gate::Gate, b::Union{Bit, BitState})

Return a tuple of the simplified BitState, gate positions and gates. Use this function
istead of `gate.simplification` to maximise reuse of computation. The type of `b` must
be BitState if the length of `gate` is more than one.

# Examples

```julia-repl
julia> ccnot = cnot_gate(3);
julia> ccnot[BitState("1_0")]
(Bit[bit_1, bit__, bit__], Array{Int64,1}[[1], [2, 3]], Gate[1 bit(s) identity gate, 2 bit(s) cnot gate])
julia> ccnot[BitState("_10")]
(Bit[bit__, bit_1, bit__], Array{Int64,1}[[2], [1, 3]], Gate[1 bit(s) identity gate, 2 bit(s) cnot gate])
julia> ccnot[BitState("_00")]
(Bit[bit__, bit_0, bit_0], Array{Int64,1}[[1], [2], [3]], Gate[1 bit(s) identity gate, 1 bit(s) identity gate, 1 bit(s) identity gate])
```
"""
function getindex(gate::Gate, bs::BitState)
    if length(bs) != gate.n_bits
        error("Size of input must be the same as the gate.")
    end
    get(gate.memory, bs) do
        gate.memory[bs] = gate.simplification(bs)
    end
end

function getindex(gate::Gate, b::Bit)
    if gate.n_bits != 1
        error("Size of input must be the same as the gate.")
    end
    get(gate.memory, [b]) do
        gate.memory[[b]] = gate.simplification([b])
    end
end

"""
    length(gate::Gate)

Return the number of bits that `gate` act on.
"""
length(gate::Gate) = gate.n_bits

"""
    ==(g1::Gate, g2::Gate)

Check if `g1.transform == g2.transform`.
"""
==(g1::Gate, g2::Gate) = g1.transform == g2.transform

function show(io::IO, gate::Gate)
    identity = VectorTransformation(collect(1:(2^gate.n_bits)))
    cnot = VectorTransformation(collect(1:(2^gate.n_bits)))
    cnot.permutation[[end-1, end]] = cnot.permutation[[end, end-1]]

    transform = VectorTransformation(gate.transform)

    print(io, string(gate.n_bits) * " bit(s) ")
    if transform == identity
        print(io, "identity gate")
    elseif transform == cnot
        print(io, "cnot gate")
    else    
        print(io, "gate : ", transform.permutation)
    end
end

### Elementary gates

## identity gate

"""
    identity_gate(n_bits::Int)

Return a `Gate` object that act like the identity on `n_bits` bits. 

# Example

```julia-repl
julia> id = identity_gate(2)
2 bit(s) identity gate
julia> id[BitState("0_")]
(Bit[bit_0, bit__], Array{Int64,1}[[1, 2]], Gate[2 bit(s) identity gate])
```
"""
function identity_gate(n_bits::Int)
    if n_bits < 1
        error("Number of bits must be positive.")
    end
    transform = VectorTransformation(collect(1:(2^n_bits)))
    gate = Gate(transform)
    gate.simplification = bs -> (copy(bs), [collect(1:n_bits)], [gate])
    gate
end

"""
    identity_gate!(gate_set::GateSet, n_bits::Int)

Return a reference to the n_bits identity gate in `gate_set`. If the gate didn't exist,
it is created and added in `gate_set` with the name `id_{n_bits}`.

# Example

```julia-repl
julia> gs = GateSet()
Dict{String,Gate} with 0 entries
julia> identity_gate!(gs,2);
julia> gs
Dict{String,Gate} with 1 entry:
  "id_2" => 2 bit(s) identity gate
```
"""
function identity_gate!(gate_set::GateSet, n_bits::Int)
    gate_name = "id_" * string(n_bits)
    get(gate_set, gate_name) do
        gate_set[gate_name] = identity_gate(n_bits)
    end
end

## Not gate

"""
    not_gate()

Return a `Gate` object that act like the not gate. 

# Example

```julia-repl
julia> not = not_gate()
1 bit(s) cnot gate
julia> not[bit_1]
(Bit[bit_0], Array{Int64,1}[[1]], Gate[1 bit(s) cnot gate])
```
"""
function not_gate()
    gate = Gate(VectorTransformation([2, 1]))
    gate.simplification = bs -> _not_simplification(bs, gate)
    gate
end

function _not_simplification(bs::BitState, gate::Gate)
    if length(bs) != 1
        error("Number of bit must be 1.")
    end
    output_bs = Int(bs[1]) < 2 ? [Bit(1 - Int(bs[1]))] : copy(bs)
    (output_bs, [[1]], [gate])
end

"""
    not_gate!(gate_set::GateSet)

Return a reference to the not gate in `gate_set. If the gate didn't exist,
it is created and added in `gate_set` with the names `not` and `cnot_1`. Note that 
both names refer to the same gate.

# Example

```julia-repl
julia> gs = GateSet()
Dict{String,Gate} with 0 entries
julia> not_gate!(gs);
julia> gs
Dict{String,Gate} with 2 entries:
  "cnot_1" => 1 bit(s) cnot gate
  "not"    => 1 bit(s) cnot gate
```
"""
function not_gate!(gate_set::GateSet)
    get(gate_set, "not") do
        gate_set["not"] = not_gate()
        gate_set["cnot_1"] = gate_set["not"]
    end
end

## CNOT (Last bit is target. All other bits are control.)

"""
    cnot_gate(n_bits::Int = 2)

Return a `Gate` object that act like the cnot gate on `n_bits` bits. The last bit 
is the target and all the others are control. If `n_bits = 1`, it became the not gate. 

# Examples

```julia-repl
julia> cnot = cnot_gate()
2 bit(s) cnot gate
julia> cnot[BitState("10")]
(Bit[bit_1, bit_1], Array{Int64,1}[[1], [2]], Gate[1 bit(s) identity gate, 1 bit(s) cnot gate])
julia> cnot[BitState("e_")]
(Bit[bit__, bit__], Array{Int64,1}[[1, 2]], Gate[2 bit(s) cnot gate])
```
"""
function cnot_gate(n_bits::Int = 2)
    if n_bits < 1
        error("Number of bits must be positive.")
    elseif n_bits == 1
        not_gate()
    else
        transform = collect(1:(2^n_bits))
        transform[[end-1, end]] = transform[[end, end-1]]
        gate = Gate(VectorTransformation(transform))
        gate.simplification = bs -> _cnot_simplification(bs, gate)
        gate
    end
end

# Simplification function for cnot gate
function _cnot_simplification(bs::BitState, gate::Gate)
    if length(bs) != gate.n_bits 
        error("Length of bit state is not the same as the gate.")
    end
    if length(bs) == 1 # Not gate
        output_bs = Int(bs[1]) < 2 ? [Bit(1 - Int(bs[1]))] : copy(bs)
        (output_bs, [[1]], [not_gate()])
    else
        # cnot to identity
        if bit_0 in bs[1:(end-1)] || bs[end] == bit_e
            (bs, [[i] for i in 1:gate.n_bits], fill(identity_gate(1), gate.n_bits))
        else
            # All bit_1 in control can be remove from the cnot. 
            if bit_1 in bs[1:(end-1)]
                inds_of_ones = find(bs[1:(end-1)] .== bit_1)
                inds_not_ones = other_indices(gate.n_bits, inds_of_ones)
                    
                # Simplify the gate without the bit_1.            
                simp = _cnot_simplification(bs[inds_not_ones], cnot_gate(length(inds_not_ones)))
                
                # Combine the bit_1 and the other simplifications.
                output_bs = zeros(Bit, gate.n_bits)
                output_bs[inds_of_ones] = bit_1 
                output_bs[inds_not_ones] = simp[1]
                
                output_inds = [inds_not_ones[i] for i in simp[2]]
                output_inds = vcat([[i] for i in inds_of_ones], output_inds)
                
                output_gates = vcat(fill(identity_gate(1), length(inds_of_ones)), simp[3])
                
                (output_bs, output_inds, output_gates)
            else # If no bit_1, add the open legs.
                if bs[end] == bit__
                    (fill(bit__, gate.n_bits), [collect(1:gate.n_bits)], [gate])
                elseif bit__ in bs[1:(end-1)]
                    output_bs = copy(bs)
                    output_bs[end] = bit__
                    (output_bs, [collect(1:gate.n_bits)], [gate])
                else
                    error("This state can't be simplify.")
                end
            end
        end
    end
end

"""
    cnot_gate!(gate_set::GateSet, n_bits::Int = 2)

Return a reference to the cnot gate in `gate_set. If the gate didn't exist,
it is created and added in `gate_set` with the name `cnot_{n_bits}`. 

# Example

```julia-repl
julia> gs = GateSet()
Dict{String,Gate} with 0 entries
julia> not_gate!(gs);
julia> gs
Dict{String,Gate} with 2 entries:
  "cnot_1" => 1 bit(s) cnot gate
  "not"    => 1 bit(s) cnot gate
```
"""
function cnot_gate!(gate_set::GateSet, n_bits::Int = 2)
    gate_name = "cnot_" * string(n_bits)
    get(gate_set, gate_name) do
        if n_bits < 1
            error("Number of bits must be positive.")
        elseif n_bits == 1
            not_gate!(gate_set)
        else
            transform = collect(1:(2^n_bits))
            transform[[end-1, end]] = transform[[end, end-1]]
            gate = Gate(VectorTransformation(transform))
            gate.simplification = bs -> _cnot_simplification!(bs, gate, gate_set)
            gate_set[gate_name] = gate
        end
    end 
end

function _cnot_simplification!(bs::BitState, gate::Gate, gate_set::GateSet)
    if length(bs) != gate.n_bits 
        error("Length of bit state is not the same as the gate.")
    end
    if length(bs) == 1 # Not gate
        output_bs = Int(bs[1]) < 2 ? [Bit(1 - Int(bs[1]))] : copy(bs)
        (output_bs, [[1]], [not_gate!(gate_set)])
    else
        if bit_0 in bs[1:(end-1)] || bs[end] == bit_e
            (bs, [[i] for i in 1:gate.n_bits], fill(identity_gate!(gate_set, 1), gate.n_bits))
        else
            # All bit_1 in control can be remove from the cnot. 
            if bit_1 in bs[1:(end-1)]
                inds_of_ones = find(bs[1:(end-1)] .== bit_1)
                inds_not_ones = other_indices(gate.n_bits, inds_of_ones)
                      
                # Simplify the gate without the bit_1.                      
                n_keep = length(inds_not_ones)
                simp = _cnot_simplification!(bs[inds_not_ones], cnot_gate!(gate_set, n_keep), gate_set)
                
                # Combine the bit_1 and the other simplifications.
                output_bs = zeros(Bit, gate.n_bits)
                output_bs[inds_of_ones] = bit_1 
                output_bs[inds_not_ones] = simp[1]
                
                output_inds = [inds_not_ones[i] for i in simp[2]]
                output_inds = vcat([[i] for i in inds_of_ones], output_inds)
                
                output_gates = vcat(fill(identity_gate!(gate_set, 1), length(inds_of_ones)), simp[3])
                
                (output_bs, output_inds, output_gates)
            else # If no bit_1, add the open legs.
                if bs[end] == bit__
                    (fill(bit__, gate.n_bits), [collect(1:gate.n_bits)], [gate])
                elseif bit__ in bs[1:(end-1)]
                    output_bs = copy(bs)
                    output_bs[end] = bit__
                    (output_bs, [collect(1:gate.n_bits)], [gate])
                else
                    error("This state can't be simplify.")
                end
            end
        end
    end
end

## Multi cnot gate

"""
    multi_cnot_gate(n_controls::Int, n_targets::Int)
"""
function multi_cnot_gate(n_controls::Int, n_targets::Int)
    if n_controls < 0 || n_targets < 0
        error("Number of bits must be non negative.")
    end
    if n_controls == 0 || n_targets == 0
        identity_gate(n_controls+n_targets)
    end
    n_bits = n_controls + n_targets
    transform = collect(1:2^n_bits)
    transform[(2^n_bits - 2^n_targets + 1):end] = transform[end:-1:(2^n_bits - 2^n_targets + 1)]
    gate = Gate(VectorTransformation(transform))
    gate.simplification = bs -> _multi_cnot_simplification(bs, gate, n_controls, n_targets)
    gate
end

function _multi_cnot_simplification(bs::BitState, gate::Gate, n_controls::Int, n_targets::Int)
    if length(bs) != gate.n_bits 
        error("Length of bit state is not the same as the gate.")
    end

    if bit__ in bs
        (fill(bit__, gate.n_bits), [collect(1:gate.n_bits)], [gate])
    elseif bit_0 in bs[1:n_controls] || bs[n_controls+1:end] == fill(bit_e, n_targets)
        (bs, [[i] for i in 1:gate.n_bits], fill(identity_gate(1), gate.n_bits))
    elseif bs[1] == bit_1
        if n_controls == 1
            outputs = [_not_simplification([bs[i+1]], not_gate()) for i in 1:n_targets]
            output_state = vcat(bit_1, [outputs[i][1] for i in 1:n_targets]...)
            output_positions = [[i] for i in 1:n_targets+1]
            output_gates = vcat(identity_gate(1), [outputs[i][3] for i in 1:n_targets]...)
            (output_state, output_positions, output_gates)
        else
            new_gate = multi_cnot_gate(n_controls-1, n_targets)
            output = _multi_cnot_simplification(bs[2:end], new_gate, n_controls-1, n_targets)
            (vcat(bit_1, output[1]), vcat([[0]], output[2]).+1, vcat(identity_gate(1), output[3]))
        end
    elseif bs[end] == bit_e
        if n_targets == 1
            (bs, collect(1:gate.n_bits), identity_gate(gate.n_bits))
        else
            output = _multi_cnot_simplification(bs[1:end-1], multi_cnot_gate(n_controls, n_targets-1), n_controls, n_targets-1)
            (vcat(output[1], bit_e), vcat(output[2], [[gate.n_bits]]), vcat(output[3],identity_gate(1)))
        end
    # elseif bit__ in bs[1:n_controls]
        # (vcat(bs[1:n_controls], fill(bit__, n_targets)), [collect(1:gate.n_bits)], [gate])
    # elseif bit__ in bs[n_controls+1:end]
        # (vcat(fill(bit__, n_controls), bs[n_controls+1:end]), [collect(1:gate.n_bits)], [gate])
    else
        error("This state can't be simplify.")
    end
end


"""
    multi_cnot_gate!(n_controls::Int, n_targets::Int)
"""
function multi_cnot_gate!(gate_set::GateSet, n_controls::Int, n_targets::Int)
    if n_controls < 0 || n_targets < 0
        error("Number of bits must be non negative.")
    end
    if n_controls == 0 || n_targets == 0
        identity_gate!(gate_set, n_controls+n_targets)
    end
    n_bits = n_controls + n_targets
    transform = collect(1:2^n_bits)
    transform[(2^n_bits - 2^n_targets + 1):end] = transform[end:-1:(2^n_bits - 2^n_targets + 1)]
    gate = Gate(VectorTransformation(transform))
    gate.simplification = bs -> _multi_cnot_simplification!(bs, gate, n_controls, n_targets, gate_set)
    gate
end

function _multi_cnot_simplification!(bs::BitState, gate::Gate, n_controls::Int, n_targets::Int, gate_set::GateSet)
    if length(bs) != gate.n_bits 
        error("Length of bit state is not the same as the gate.")
    end
    if bit__ in bs
        (fill(bit__, gate.n_bits), [collect(1:gate.n_bits)], [gate])
    elseif bit_0 in bs[1:n_controls] || bs[n_controls+1:end] == fill(bit_e, n_targets)
        (bs, [[i] for i in 1:gate.n_bits], fill(identity_gate!(gate_set, 1), gate.n_bits))
    elseif bs[1] == bit_1
        if n_controls == 1
            outputs = [_not_simplification([bs[i+1]], not_gate!(gate_set)) for i in 1:n_targets]
            output_state = vcat(bit_1, [outputs[i][1] for i in 1:n_targets]...)
            output_positions = [[i] for i in 1:n_targets+1]
            output_gates = vcat(identity_gate!(gate_set, 1), [outputs[i][3] for i in 1:n_targets]...)
            (output_state, output_positions, output_gates)
        else
            new_gate = multi_cnot_gate!(gate_set, n_controls-1, n_targets)
            output = _multi_cnot_simplification!(bs[2:end], new_gate, n_controls-1, n_targets, gate_set)
            (vcat(bit_1, output[1]), vcat([[0]], output[2]).+1, vcat(identity_gate!(gate_set, 1), output[3]))
        end
    elseif bs[end] == bit_e
        if n_targets == 1
            (bs, collect(1:gate.n_bits), identity_gate!(gate_set, gate.n_bits))
        else
            output = _multi_cnot_simplification!(bs[1:end-1], multi_cnot_gate!(gate_set, n_controls, n_targets-1), n_controls, n_targets-1, gate_set)
            (vcat(output[1], bit_e), vcat(output[2], [[gate.n_bits]]), vcat(output[3],identity_gate!(gate_set, 1)))
        end
    # elseif bit__ in bs[1:n_controls]
    #     (vcat(bs[1:n_controls], fill(bit__, n_targets)), [collect(1:gate.n_bits)], [gate])
    # elseif bit__ in bs[n_controls+1:end]
    #     (vcat(fill(bit__, n_controls), bs[n_controls+1:end]), [collect(1:gate.n_bits)], [gate])
    elseif bit__ in bs
        (fill(bit__, gate.n_bits), [collect(1:gate.n_bits)], [gate])
    else
        error("This state can't be simplify.")
    end
end

## SWAP gate

"""
    swap_gate()   

Return a `Gate` object that act like the swap gate on 2 bits. 

# Examples

```julia-repl
julia> swap = swap_gate()
2 bit(s) gate : [1, 3, 2, 4]
julia> swap[BitState("10")]
(Bit[bit_0, bit_1], Array{Int64,1}[[1, 2]], Gate[2 bit(s) arbitrary gate : [1, 3, 2, 4]])
```
"""
function swap_gate()
    gate = Gate(VectorTransformation([1, 3, 2, 4]))
    gate.simplification = bs -> _swap_simplification(bs, gate)
    gate
end

function _swap_simplification(bs::BitState, gate::Gate)
    if length(bs) != 2
        error("Number of bit must be 2.")
    end
    id1 = identity_gate(1)
    if bs == BitState("__")
        (BitState("__"), [[1, 2]], [gate])
    else
        bs[1] != bs[2] ? ([bs[2], bs[1]], [[1, 2]], [gate]) : (copy(bs), [[1], [2]], [id1, id1])
    end
end

## Random gates

"""
    random_linear_reversible_gate(n_bits::Int) 

Generate a random linear reversible gate of `n_bits.
"""
function random_linear_reversible_gate(n_bits::Int)
    rand_mat = Int.(rand(n_bits,n_bits) .> 0.5)
    while (det(rand_mat) % 2) == 0
        rand_mat = Int.(rand(n_bits,n_bits) .> 0.5)
    end
    transform = VectorTransformation(linear_gate_to_permutation(rand_mat))
    gate = Gate(transform)
    gate.simplification = bs -> random_gate_simplification(bs, gate)
    gate
end

function random_gate_simplification(bs::BitState, gate::Gate)
    if length(bs) != gate.n_bits 
        error("Length of bit state is not the same as the gate.")
    end
    if bit__ in bs
        (fill(bit__, length(bs)), [[1:length(bs)...]], [gate])
    elseif prod(bs .== bit_e)
        (bs, [[1:length(bs)...]], [identity_gate(length(bs))])
    elseif !(bit_e in bs)
        (gate.transform * bs, [[1:length(bs)...]], [gate])
    else
        error("This state can't be simplify.")
    end
end

function linear_gate_to_permutation(mat::Matrix{Int})
    if size(mat)[1] != size(mat)[2]
        error("Matrix must be square.")
    end
    n_bits = size(mat)[1]
    bit_inputs = [digits(i,2,n_bits)[end:-1:1] for i in 0:(2^n_bits-1)]
    bit_outputs = [(mat * bit_input) .% 2 for bit_input in bit_inputs]
    basis = [2^k for k in (n_bits-1):-1:0]
    permutation = [dot(basis, bit_output) for bit_output in bit_outputs] .+ 1
    invperm(permutation)
end

## Elementary gate set

elementary_gates = GateSet()

identity_gate!(elementary_gates, 1)
not_gate!(elementary_gates)
cnot_gate!(elementary_gates)
elementary_gates["cnot"] = elementary_gates["cnot_2"]


