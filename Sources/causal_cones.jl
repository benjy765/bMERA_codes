export CausalCone, ConeChilds, depth, merge_childs, merge_cps_childs, contract!, set_childs!

## Type definition

"""
    CausalCone <: Any

Mutable type.

# Fields

- `top_bit_state::BitState`
- `first_bit_position::Int`
- `transform::Transformation`
- `childs::Union{Vector{CausalCone}, ProbState}`
- `last_contracted_bit_state::BitState`
- `last_contracted_prob_state::ProbState`

Causal cone must have some open legs to proceed to the decoding. We must also
ensure that the number of linked legs of `childs` equals the length of `top_bit_state` 
so that contraction is possible. There are tree ways to init a `CausalCone` : 
    
    CausalCone(top_bit_state::BitState, first_bit_position::Int, transform::Transformation, childs::Vector{CausalCone})
    CausalCone(top_bit_state::BitState, first_bit_position::Int, transform::Transformation, childs::ProbState)
    CausalCone(top_bit_state::BitState, first_bit_position::Int, transform::Transformation)

The CausalCone will be contracted at creation if `childs` is of type Probstate.
This will define a value for `last_contracted_bit_state` and `last_contracted_prob_state`.

# Examples 

```julia-repl
julia> transform = VectorTransformation([1,2,4,3]);
julia> left_prob = ZeroProbState([0.1, 0.1]);
julia> right_prob = ZeroProbState([0.9, 0.1]);
julia> left_cone = CausalCone(BitState("e_"), 1, transform, left_prob)
Top bit state : "e_"
First bit position : 1
Transformation : [1, 2, 4, 3]
ZeroProbState :
[0.1, 0.1]

julia> left_cone.last_contracted_bit_state
2-element Array{Bit,1}:
 bit_e
 bit__

julia> left_cone.last_contracted_prob_state
BitStringProbState([0.82, 0.18], 1)

julia> right_cone = CausalCone(BitState("e_"), 1, transform, right_prob);
julia> cone = CausalCone(BitState("_0"), 1, transform, [left_cone, right_cone])
Top bit state : "_0"
First bit position : 1
Transformation : [1, 2, 4, 3]
Childs :
   Top bit state : "e_"
   First bit position : 1
   Transformation : [1, 2, 4, 3]
   ZeroProbState :
   [0.1, 0.1]

   Top bit state : "e_"
   First bit position : 1
   Transformation : [1, 2, 4, 3]
   ZeroProbState :
   [0.9, 0.1]

julia> cone.last_contracted_bit_state
ERROR: UndefRefError: access to undefined reference

julia> no_child_cone = CausalCone(BitState("e_"), 1, transform)
Top bit state : "e_"
First bit position : 1
Transformation : [1, 2, 4, 3]
No child.

julia> no_child_cone.last_contracted_bit_state
ERROR: UndefRefError: access to undefined reference
```
"""
mutable struct CausalCone
    top_bit_state::BitState
    first_bit_position::Int
    transform::Transformation
    childs::Union{Vector{CausalCone}, ProbState}
    last_contracted_bit_state::BitState
    last_contracted_prob_state::ProbState
    corr::Correlator

    function CausalCone(top_bit_state::BitState, first_bit_position::Int, transform::Transformation, childs::Vector{CausalCone})
        length_tbs = length(top_bit_state)
        check = sum([n_open_legs(c.top_bit_state) for c in childs]) == length(top_bit_state)

        if !check
            error("The number of open legs in childs is not compatible with the length of tbs. Contraction won't be possible.")
        end
        if !(bit__ in top_bit_state)
            error("There's nothing to decode !")
        end
        new(top_bit_state, first_bit_position, transform, childs)
    end

    function CausalCone(top_bit_state::BitState, first_bit_position::Int, transform::Transformation, childs::ProbState)
        if length(top_bit_state) != childs.n_bits
            error("The number of input and output must be the same.")
        end
        if !(bit__ in top_bit_state)
            error("There's nothing to decode !")
        end

        if typeof(childs) <: CorrelatedProbState
            # Contraction for CPS assuming a 2 bits cps.
            if VectorTransformation(transform) != VectorTransformation([1,2,4,3])   # Contraction is only implement for a CNOT
                error("Transformation must be a CNOT.")
            end 

            if childs.n_bits != 2
                error("It must be a 2 bits CPS.")
            end
            last_contracted_prob_state = top_bit_state * contract_cnot(childs)
            last_contracted_bit_state = top_bit_state   
            new(top_bit_state, first_bit_position, transform, childs, last_contracted_bit_state, last_contracted_prob_state)                     
        else    
            # We can compute last_contracted_prob_state and update last_contracted_bit_state
            last_contracted_prob_state = childs * transform * top_bit_state
            last_contracted_bit_state = top_bit_state
            new(top_bit_state, first_bit_position, transform, childs, last_contracted_bit_state, last_contracted_prob_state)
        end
    end

    function CausalCone(top_bit_state::BitState, first_bit_position::Int, transform::Transformation)
        if !(bit__ in top_bit_state)
            error("There's nothing to decode !")
        end
        new(top_bit_state, first_bit_position, transform)
    end
end

"""
    length(cc::CausalCone)

Return the length of top_bit_state.    
"""
length(cc::CausalCone) = length(cc.top_bit_state)

"""
    depth(cc::CausalCone)

Return the depth of a causal cone, corresponding to the number of descendant of the causal cone.
For example, a causal cone with childs and grandchilds is a causal cone of depth of 3.
"""
function depth(cc::CausalCone)
    if !isdefined(cc, :childs)
        return 1
    elseif typeof(cc.childs) <: ProbState
        return 2 
    else
        return 1 + maximum([depth(c) for c in cc.childs])
    end
end

# Useful alias
const ConeChilds = Union{Vector{CausalCone}, ProbState}

"""
    merge_childs(cc::CausalCone)

Compute the kronecker between all childs and permute the result according to the number
of childs and `cc.first_bit_position` to fit `cc` bottom legs positions.
"""
function merge_childs(cc::CausalCone)
    childs = cc.childs
    memory_value = childs[1].last_contracted_prob_state
    for i in 1:length(childs)-1
        memory_value = kron(memory_value, childs[i+1].last_contracted_prob_state)
    end
    permute(memory_value, _childs_permutation(cc))
end

function merge_cps_childs(cc::CausalCone)
    childs = cc.childs
    memory_value = contract_cps(cc.corr, childs[1].last_contracted_prob_state, childs[2].last_contracted_prob_state)    
end

"""
    contract!(cc::CausalCone)

Perform the contraction on a causal cone. It corresponds to an updated value of
last_contracted_bit_state and last_contracted_prob_state.
"""
function contract!(cc::CausalCone)
    if !(isdefined(cc, :last_contracted_bit_state) && cc.last_contracted_bit_state == cc.top_bit_state)
        if typeof(cc.childs) <: CorrelatedProbState
            if VectorTransformation(cc.transform) != VectorTransformation([1,2,4,3])   # Contraction is only implement for a CNOT
                error("Transformation must be a CNOT.")
            end 
            if cc.childs.n_bits != 2
                error("It must be a 2 bits CPS.")
            end
            cc.last_contracted_prob_state = cc.top_bit_state * contract_cnot(cc.childs)
            cc.last_contracted_bit_state = cc.top_bit_state 

        elseif typeof(cc.childs) <: ProbState
            cc.last_contracted_prob_state = cc.childs * cc.transform * cc.top_bit_state
            cc.last_contracted_bit_state = cc.top_bit_state
        else         
            for child in cc.childs
                contract!(child)
            end

            if typeof(cc.childs[1].last_contracted_prob_state) <: CorrelatedProbState
                result = merge_cps_childs(cc)
                if VectorTransformation(cc.transform) != VectorTransformation([1,2,4,3])   # Contraction is only implement for a CNOT
                    error("Transformation must be a CNOT.")
                end 
                cc.last_contracted_prob_state = cc.top_bit_state * contract_cnot(result)
                cc.last_contracted_bit_state = cc.top_bit_state              
            else
                result = merge_childs(cc)
                cc.last_contracted_prob_state = result * cc.transform * cc.top_bit_state            
                cc.last_contracted_bit_state = cc.top_bit_state
            end
        end
    end
end


"""
    set_childs!(cc::CausalCone, childs::Vector{CausalCone})

Set the attribute childs for a causal cone with the childs corresponding to a vector of causal cone.
The length of the causal cone must be equal to the number of open legs of the childs.
"""
function set_childs!(cc::CausalCone, childs::Vector{CausalCone})
    n_open_legs = 0
    for child in childs 
        n_open_legs += sum(child.top_bit_state .== bit__)
    end

    if n_open_legs != length(cc)
        error("The number of open legs in the child must be the length of the causal cone.")
    end
    cc.childs = childs
end

"""
    set_childs!(cc::CausalCone, childs::ProbState)

Set the childs when they are a probstate. The length of the probstate must be equal
to the length of the causal cone. Will contract the cone.
"""
function set_childs!(cc::CausalCone, childs::ProbState)
    if length(childs) != length(cc)
        error("The length of the new child must be the same as the causal cone.")
    end
    cc.childs = childs
    contract!(cc)
end

"""
    _check_childs(cc::CausalCone)

Return a true value if the family tree of the causal cone ends with probstate. 
Otherwise, false is return, meaning that at least one of the child doesn't end with a probstate.
"""
function _check_childs(cc::CausalCone)
    if isdefined(cc, :childs)
        if typeof(cc.childs) <: ProbState
            return true
        else
            check = true
            for child in cc.childs
                check *= _check_childs(child)
            end
            return check
        end
    else
        return false
    end
end

"""
    _childs_permutation(cc::CausalCone)

Return the sum of _childs_position and _childs_offset resulting into the permutation between the top cone and his childs.
"""
_childs_permutation(cc::CausalCone) = _childs_position(cc) + _childs_offset(cc)

"""
    _childs_position(cc::CausalCone)

Utilitary function for _childs_permutation
"""
function _childs_position(cc::CausalCone)
    n_childs = length(cc.childs)
    max_rep = div(length(cc), n_childs)
    first_childs = [[i for j in 1:n_childs] for i in 1:max_rep]
    last_childs = [max_rep+1 for i in 1:(length(cc) - max_rep*n_childs)]
    vcat(first_childs..., last_childs)
end

"""
    _childs_offset(cc::CausalCone)

Utilitary function for _childs_permutation
"""
function _childs_offset(cc::CausalCone)
    n_childs = length(cc.childs)
    childs_offset = vcat(0,cumsum([sum(c.top_bit_state .== bit__) for c in cc.childs[1:end-1]]))
    childs_position = collect(0:(length(cc)-1)) + cc.first_bit_position - 1
    childs_offset[childs_position .% n_childs + 1]
end

# Show

function show(io::IO, cc::CausalCone, space = "")
    println(space * "Top bit state : \"" * string(cc.top_bit_state) * "\"")
    println(space * "First bit position : " * string(cc.first_bit_position))
    println(space * "Transformation : " * string(cc.transform.permutation))
    # println(space * "Last contracted : " * string(cc.last_contracted_prob_state))
    if isdefined(cc, :childs)
        if typeof(cc.childs) <: ProbState
            println(space * string(typeof(cc.childs)) * " : ")
            println(space, cc.childs.prob)
        else
            println(space * "Childs : ")
            space = space * "   "
            for child in cc.childs
                show(io, child, space)
                println("")
            end
        end
    else
        println("No child.")
    end
end