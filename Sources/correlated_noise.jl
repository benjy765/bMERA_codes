export burst, p_error, flip_cps, stationary_vector, corr_noise, shuffle_noise, fer, ber, bad_length, mean_burst_length
export bad_length, mean_bad_length, num_transition, mean_num_transition

function burst(n_bits::Int, pgb::Float64, pbg::Float64, p_start::Float64, h = 1/2)
    output = zeros(Int, n_bits)
    state = rand() < p_start ? "G" : "B"
    for i in n_bits:-1:1    # Correlation from right to left !
        if state == "B"
            # This is the "bad" state
            output[i] = rand() > h ? 0 : 1

            # pick a random number to know what to do next
            if rand() <= pbg
                state = "G"
            end
        else
            # State = "G"
            output[i] = 0
            # pick a random number to know what to do next
            if rand() <= pgb
                state = "B"
            end
        end
    end
    output
end

function bad_length(n_bits::Int, pgb::Float64, pbg::Float64, p_start::Float64, h = 1/2)
    burst_length = []
    state = rand() < p_start ? "G" : "B"
    counter = 0
    for i in n_bits:-1:1    # Correlation from right to left !
        if state == "B"
            counter += 1
            if rand() <= pbg
                state = "G"
                push!(burst_length, counter)
                counter = 0
            end
        else
            if rand() <= pgb
                state = "B"
            end
        end
    end
    if length(burst_length) != 0
        mean_length = mean(burst_length)
    else
        0.0
    end
end

function mean_bad_length(n_rep::Int, n_bits::Int, pgb::Float64, pbg::Float64, p_start::Float64, h = 1/2)
    mean([bad_length(n_bits, pgb, pbg, p_start, h) for i in 1:n_rep])
end

function num_transition(n_bits::Int, pgb::Float64, pbg::Float64, p_start::Float64, h = 1/2)
    n_trans = 0
    state = rand() < p_start ? "G" : "B"
    for i in n_bits:-1:1    # Correlation from right to left !
        if state == "B"
            if rand() <= pbg
                state = "G"
                n_trans += 1
            end
        else
            if rand() <= pgb
                state = "B"
                n_trans += 1
            end
        end
    end
    n_trans
end

function mean_num_transition(n_rep::Int, n_bits::Int, pgb::Float64, pbg::Float64, p_start::Float64, h = 1/2)
    mean([num_transition(n_bits, pgb, pbg, p_start, h) for i in 1:n_rep])
end

# Estimated logical prob of error
function p_error(n_rep::Int, n_bits::Int, p_gb::Float64, p_bg::Float64, p_start::Float64, h = 1/2)
    r = 0
    for x in 1:n_rep
        out = burst(n_bits, p_gb, p_bg, p_start, h)
        r += sum(out)/n_bits
    end
    r/n_rep
end

# Assuming that good is identity and bad is BSC(h)
function flip_cps(cn::CorrelatedNoise, h = 1/2)
    new_cn = deepcopy(cn)
    pgb, pbg = new_cn.corr.corr[2,1], new_cn.corr.corr[1,2]
    p_start = stationary_vector(cn)[1]
    n_bits = new_cn.n_bits
    b = burst(n_bits, pgb, pbg, p_start, h)
    if b[1] == 1
        new_cn[1].prob[1,:] = new_cn[1].prob[1,:][[2,1]]
        new_cn[1].prob[2,:] = new_cn[1].prob[2,:][[2,1]]
    end
    for i in 2:new_cn.n_bits-1
        if b[i] == 1
            new_cn[i].prob[1,1,:] = new_cn[i].prob[1,1,:][[2,1]]
            new_cn[i].prob[2,2,:] = new_cn[i].prob[2,2,:][[2,1]]
        end
    end
    if b[n_bits] == 1
        new_cn[n_bits].prob[1,:] = new_cn[n_bits].prob[1,:][[2,1]]
        new_cn[n_bits].prob[2,:] = new_cn[n_bits].prob[2,:][[2,1]]
    end
    new_cn
end

# Finding the stationary distribution of a Markov Chain
function stationary_vector(c::Correlator)
    r = c.corr
    d, v = eig(r)
    ind = find(x->x==1.0, d)
    v = v[:,ind]
    v/sum(v)
end

function stationary_vector(cn::CorrelatedNoise)
    c = cn.corr
    stationary_vector(c)
end

function stationary_vector(p_gb::Float64, p_bg::Float64)
    c = Correlator(1-p_gb, 1-p_bg)
    stationary_vector(c)
end

# Correlated noise for a ZeroProbState
function corr_noise(n_bits::Int, pgb::Float64, pbg::Float64, p_start::Float64, h::Float64, pe::Float64)
    b = burst(n_bits, pgb, pbg, p_start, h)
    ZeroProbState([bit == 0 ? 1 - pe : pe for bit in b])
end

# Shuffle Correlated noise for a ZeroProbState
function shuffle_noise(n_bits::Int, pgb::Float64, pbg::Float64, p_start::Float64, h::Float64, pe::Float64, perm::Array{Int64})
    b = burst(n_bits, pgb, pbg, p_start, h)
    b = b[perm]
    ZeroProbState([bit == 0 ? 1 - pe : pe for bit in b])
end

# BER and FER
fer(decoded_state::BitState) = bit_1 in decoded_state
ber(decoded_state::BitState, not_frozen::Int) = sum(x -> Int(x), decoded_state) / not_frozen