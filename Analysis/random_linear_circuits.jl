@everywhere include("/home/trem2434/bMERA_codes/Sources/bmera_codes.jl")
@everywhere using BMeraCodes

# Parameters 

@everywhere begin
branching, n_layers, n_to_keep, run_tag = Int.(readdlm("input.txt")) 
n_bits = branching^n_layers
max_n_gates = 5
prob = 0.25
n_iters = 100

save_folder = "/home/trem2434/bMERA_codes/Analysis/Results/run_"*string(run_tag)
try
    mkdir(save_folder) 
end
save_folder *= "/branching_"*string(branching)
try
    mkdir(save_folder)
end

function iteration(i::Int)    
    circ, gates, starting_points = random_conv_circuit(n_layers, branching, rand(1:max_n_gates))   
    change_bottom_state!(circ, ZeroProbState(fill(1-prob,n_bits)))
    try
        pfe = mean(sort(prob_first_error(circ))[1:n_to_keep])
	try
	    mkdir(save_folder * "/iter_"*string(i))
	end
	for (k, gate) in enumerate(gates)
	    writedlm(save_folder * "/iter_"*string(i)*"/gate_"*string(k)".dat", gate.transform.permutation)
	end
	writedlm(save_folder * "/iter_"*string(i)*"/starting_points.dat", starting_points)
        return pfe
    catch
        return -1
    end
end 
end
writedlm(save_folder*"/pfe.dat", pmap(iteration, 1:n_iters))


