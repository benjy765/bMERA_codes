@everywhere include("../Sources/bmera_codes.jl")
@everywhere using BMeraCodes

@everywhere begin
    prob = 0.25
    cnot = elementary_gates["cnot"]


    function iteration(n_layers::Int, n_cnot::Int, struct12)
        n_bits = 2^n_layers
        n_to_keep = 2^(n_layers-1)
        if struct12 
            starting_points = [2 - (k % 2) for k in 1:n_cnot]
        else
            starting_points = [(k % 2) + 1 for k in 1:n_cnot]
        end
        circ = conv_polar_code(n_layers,  fill(cnot, n_cnot), starting_points)
        change_bottom_state!(circ, ZeroProbState(fill(1-prob,n_bits)))
        return mean(sort(prob_first_error(circ))[1:n_to_keep])
    end
end

results = zeros(7,10)
println(1)
results[1] = pmap(l -> iteration(l, 1, true), 3:12)
println(2)
results[2] = pmap(l -> iteration(l, 2, true), 3:12)
println(3)
results[3] = pmap(l -> iteration(l, 2, false), 3:12)
println(4)
results[4] = pmap(l -> iteration(l, 3, true), 3:12)
println(5)
results[5] = pmap(l -> iteration(l, 3, false), 3:12)
println(6)
results[6] = pmap(l -> iteration(l, 4, true), 3:12)
println(7)
results[7] = pmap(l -> iteration(l, 4, false), 3:12)
writedlm("results.dat",results)